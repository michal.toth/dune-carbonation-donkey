// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=8 sw=2 sts=2:
#ifndef DUNE_PDELAB_BACKEND_ISTL_OVLPISTLSOLVERBACKEND_MAXNORM_HH
#define DUNE_PDELAB_BACKEND_ISTL_OVLPISTLSOLVERBACKEND_MAXNORM_HH

#include <dune/pdelab/backend/istl/ovlpistlsolverbackend.hh>

namespace Dune {
  namespace PDELab {

    //===============================================================
    // Linear solver with maximum norm.
    // Follows the implementation of the linear solver with Euclid
    // norm, making minimal adjustments.
    //===============================================================

    template<typename GFS>
    class OVLPScalarProductImplementationMaxNorm 
      : public OVLPScalarProductImplementation<GFS>
    {
    public:
      OVLPScalarProductImplementationMaxNorm(const GFS& gfs_)
        : OVLPScalarProductImplementation<GFS>(gfs_), gfs(gfs_)
      {}

      using OVLPScalarProductImplementation<GFS>::dot;

      using OVLPScalarProductImplementation<GFS>::norm;

      template<typename X>
      typename Dune::template FieldTraits<typename X::ElementType >::real_type infinity_norm (const X& x) const
      {
        // get local maximum, overlap included but it does not matter
        typename X::ElementType max = x.infinity_norm();
        // do global communication
        return this->gfs.gridView().comm().max(max);
      }

      using OVLPScalarProductImplementation<GFS>::parallelHelper;

    private:
      const GFS& gfs;
    };

    //! Class OVLPScalarProduct, but with infinity_norm instead of two_norm
    template<typename GFS, typename X>
    class OVLPScalarProductMaxNorm
      : public ScalarProduct<X>
    {
    public:
      SolverCategory::Category category() const override
      {
        return SolverCategory::overlapping;
      }

      OVLPScalarProductMaxNorm(const OVLPScalarProductImplementationMaxNorm<GFS>& implementation_)
        : implementation(implementation_)
      {}

      virtual typename X::Container::field_type dot(const X& x, const X& y) const override
      {
        return implementation.dot(x,y);
      }

      virtual typename X::Container::field_type norm (const X& x) const override
      {
        return implementation.infinity_norm(x);
      }

    private:
      const OVLPScalarProductImplementationMaxNorm<GFS>& implementation;
    };

    //! Class ISTLBackend_OVLP_Base, but with infinity_norm,
    //  only difference is in PSP=OVLPScalarProductMaxNorm
    template<class GFS, class C,
             template<class,class,class,int> class Preconditioner,
             template<class> class Solver>
    class ISTLBackend_OVLP_Base_MaxNorm
      : public OVLPScalarProductImplementationMaxNorm<GFS>, public LinearResultStorage
    {
    public:
      /*! \brief make a linear solver object

        \param[in] gfs_ a grid function space
        \param[in] c_ a constraints object
        \param[in] maxiter_ maximum number of iterations to do
        \param[in] steps_ number of SSOR steps to apply as inner iteration
        \param[in] verbose_ print messages if true
      */
      ISTLBackend_OVLP_Base_MaxNorm (const GFS& gfs_, const C& c_, unsigned maxiter_=5000,
                                            int steps_=5, int verbose_=1)
        : OVLPScalarProductImplementationMaxNorm<GFS>(gfs_), gfs(gfs_), c(c_), maxiter(maxiter_), steps(steps_), verbose(verbose_)
      {}

      /*! \brief solve the given linear system

        \param[in] A the given matrix
        \param[out] z the solution vector to be computed
        \param[in] r right hand side
        \param[in] reduction to be achieved
      */
      template<class M, class V, class W>
      void apply(M& A, V& z, W& r, typename Dune::template FieldTraits<typename V::ElementType >::real_type reduction)
      {
        using Backend::Native;
        using Backend::native;
        typedef OverlappingOperator<C,M,V,W> POP;
        POP pop(c,A);
        typedef OVLPScalarProductMaxNorm<GFS,V> PSP;
        PSP psp(*this);
        typedef Preconditioner<
          Native<M>,
          Native<V>,
          Native<W>,
          1
          > SeqPrec;
        SeqPrec seqprec(native(A),steps,1.0);
        typedef OverlappingWrappedPreconditioner<C,GFS,SeqPrec> WPREC;
        WPREC wprec(gfs,seqprec,c,this->parallelHelper());
        int verb=0;
        if (gfs.gridView().comm().rank()==0) verb=verbose;
        Solver<V> solver(pop,psp,wprec,reduction,maxiter,verb);
        Dune::InverseOperatorResult stat;
        solver.apply(z,r,stat);
        res.converged  = stat.converged;
        res.iterations = stat.iterations;
        res.elapsed    = stat.elapsed;
        res.reduction  = stat.reduction;
        res.conv_rate  = stat.conv_rate;
      }
    private:
      const GFS& gfs;
      const C& c;
      unsigned maxiter;
      int steps;
      int verbose;
    };

    /**
     * @brief Overlapping parallel BiCGStab solver with SSOR preconditioner
     * @tparam GFS The Type of the GridFunctionSpace.
     * @tparam CC The Type of the Constraints Container.
     */
    template<class GFS, class CC>
    class ISTLBackend_OVLP_BCGS_SSORk_MaxNorm
      : public ISTLBackend_OVLP_Base_MaxNorm<GFS,CC,Dune::SeqSSOR, Dune::BiCGSTABSolver>
    {
    public:
      /*! \brief make a linear solver object

        \param[in] gfs a grid function space
        \param[in] cc a constraints container object
        \param[in] maxiter maximum number of iterations to do
        \param[in] steps number of SSOR steps to apply as inner iteration
        \param[in] verbose print messages if true
      */
      ISTLBackend_OVLP_BCGS_SSORk_MaxNorm (const GFS& gfs, const CC& cc, unsigned maxiter=5000,
                                            int steps=5, int verbose=1)
        : ISTLBackend_OVLP_Base_MaxNorm<GFS,CC,Dune::SeqSSOR, Dune::BiCGSTABSolver>(gfs, cc, maxiter, steps, verbose)
      {}
    };
  } // end namespace PDELab
} // end namespace Dune
#endif // DUNE_PDELAB_BACKEND_ISTL_OVLPISTLSOLVERBACKEND_MAXNORM_HH