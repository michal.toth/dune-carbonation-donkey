    constexpr std::size_t lfsusize = 4; // this file is inside if(lfsusize==4)
                                       // overwriting enables using it as a template argument

    const auto& a0 = eg.geometry().corner(0);
    const auto& c3 = eg.geometry().corner(3);
    const DF dx = c3[0]-a0[0];
    const DF dy = c3[1]-a0[1];
    constexpr std::array<int,lfsusize> pairx{1,0,3,2};
    constexpr std::array<int,lfsusize> pairy{2,3,0,1};

    std::array<RF,lfsusize> Kwr, Kac, Karc; // better have vector of coefficients than re-compute pow()
    for (size_t i=0; i<lfsusize; ++i)
    {
      Kwr[i] = param.Kwr(Sa[i],phi[i]);
      Kac[i] = param.Kac(Sa[i],phi[i],co2[i]);
      Karc[i] = Kac[i]*param.getrho_a(pa[i],co2[i]);
    }

    std::array<RF,lfsusize> dKwr_dSa;
    std::array<RF,lfsusize> dKwr_dphi;
    std::array<RF,lfsusize> dKarc_dpw;
    std::array<RF,lfsusize> dKarc_dSa;
    std::array<RF,lfsusize> dKarc_dco2;
    std::array<RF,lfsusize> dKarc_dphi;
    for (std::size_t i=0; i<lfsusize; ++i)
    {
      dKwr_dSa[i]   = param.dKwr_dSa(Sa[i],phi[i]);
      dKwr_dphi[i]  = param.dKwr_dphi(Sa[i],phi[i]);
      dKarc_dpw[i]  = param.dKarc_dpw(Sa[i],pa[i],phi[i],co2[i]);
      dKarc_dSa[i]  = param.dKarc_dSa(Sa[i],pa[i],phi[i],co2[i]);
      dKarc_dco2[i] = param.dKarc_dco2(Sa[i],pa[i],phi[i],co2[i]);
      dKarc_dphi[i] = param.dKarc_dphi(Sa[i],pa[i],phi[i],co2[i]);
    }
    std::array<RF,lfsusize> co2_RTMa;
    for (std::size_t i=0; i<lfsusize; ++i)
    {
      co2_RTMa[i] = co2[i]/p_h.getMa(co2[i])*param.getR()*param.getT();
    }

    for (int i=0; i<int(lfsusize); ++i)
    {
      // !!! this way is each edge visited twice = once from each side
      // therefore only one part, lfsv_X,i, is accumulated

      RF co2avg_y = (co2[i]+co2[pairy[i]])/2.;
      RF pa_avg_y = (pa[i]+pa[pairy[i]])/2.;
      RF rho_ay = param.getrho_a(pa_avg_y,co2avg_y);
      RF nablawx = (pw[pairx[i]]-pw[i])/dx;
      RF nablawy = (pw[pairy[i]]-pw[i])/dy-(i-pairy[i])/2.*param.getgrav()*param.getrho_w();
      RF Kwrx = h2avg(Kwr[i],Kwr[pairx[i]],"Kwr in jacobian_volume");
      RF Kwry = h2avg(Kwr[i],Kwr[pairy[i]],"Kwr in jacobian_volume");
      RF qwx = -dy/2.*Kwrx*nablawx;
      RF qwy = -dx/2.*Kwry*nablawy;
      RF nablaax = (pa[pairx[i]]-pa[i])/dx;
      RF nablaay = (pa[pairy[i]]-pa[i])/dy-(i-pairy[i])/2.*param.getgrav()*rho_ay;
      RF Karcx = h2avg(Karc[i],Karc[pairx[i]],"Karc in jacobian_volume");
      RF Karcy = h2avg(Karc[i],Karc[pairy[i]],"Karc in jacobian_volume");
      RF qax = -dy/2.*Karcx*nablaax;
      RF qay = -dx/2.*Karcy*nablaay;

      // water diffusion
      // r.accumulate(lfsv_pw,i, qwx +qwy);
      // derive water pressure gradient by pw (itself)
      mat.accumulate(lfsv_pw,i,lfsu_pw,      i , dy/2.*Kwrx/dx +dx/2.*Kwry/dy);
      mat.accumulate(lfsv_pw,i,lfsu_pw,pairx[i],-dy/2.*Kwrx/dx               );
      mat.accumulate(lfsv_pw,i,lfsu_pw,pairy[i],               -dx/2.*Kwry/dy);
      // derive permeability by Sa
      mat.accumulate(lfsv_pw,i,lfsu_Sa,      i ,-dy/2. *h2avg_deriv(Kwr[i],Kwr[pairx[i]]) *dKwr_dSa[      i ] *nablawx);
      mat.accumulate(lfsv_pw,i,lfsu_Sa,      i ,-dx/2. *h2avg_deriv(Kwr[i],Kwr[pairy[i]]) *dKwr_dSa[      i ] *nablawy);
      mat.accumulate(lfsv_pw,i,lfsu_Sa,pairx[i],-dy/2. *h2avg_deriv(Kwr[pairx[i]],Kwr[i]) *dKwr_dSa[pairx[i]] *nablawx);
      mat.accumulate(lfsv_pw,i,lfsu_Sa,pairy[i],-dx/2. *h2avg_deriv(Kwr[pairy[i]],Kwr[i]) *dKwr_dSa[pairy[i]] *nablawy);
      // derive permeability by phi
      mat.accumulate(lfsv_pw,i,lfsu_phi,      i ,-dy/2. *h2avg_deriv(Kwr[i],Kwr[pairx[i]]) *dKwr_dphi[      i ] *nablawx);
      mat.accumulate(lfsv_pw,i,lfsu_phi,      i ,-dx/2. *h2avg_deriv(Kwr[i],Kwr[pairy[i]]) *dKwr_dphi[      i ] *nablawy);
      mat.accumulate(lfsv_pw,i,lfsu_phi,pairx[i],-dy/2. *h2avg_deriv(Kwr[pairx[i]],Kwr[i]) *dKwr_dphi[pairx[i]] *nablawx);
      mat.accumulate(lfsv_pw,i,lfsu_phi,pairy[i],-dx/2. *h2avg_deriv(Kwr[pairy[i]],Kwr[i]) *dKwr_dphi[pairy[i]] *nablawy);

      // air diffusion
      // r.accumulate(lfsv_Sa,i, qax +qay);
      // derive air pressure pa by pw
      mat.accumulate(lfsv_Sa,i,lfsu_pw,      i ,  dy/2.*Karcx/dx +dx/2.*Karcy/dy );
      mat.accumulate(lfsv_Sa,i,lfsu_pw,pairx[i], -dy/2.*Karcx/dx                 );
      mat.accumulate(lfsv_Sa,i,lfsu_pw,pairy[i],                 -dx/2.*Karcy/dy );
      // derive air pressure pa by Sa
      mat.accumulate(lfsv_Sa,i,lfsu_Sa,      i ,( dy/2.*Karcx/dx +dx/2.*Karcy/dy) *dpc[      i ] );
      mat.accumulate(lfsv_Sa,i,lfsu_Sa,pairx[i],(-dy/2.*Karcx/dx                ) *dpc[pairx[i]] );
      mat.accumulate(lfsv_Sa,i,lfsu_Sa,pairy[i],(                -dx/2.*Karcy/dy) *dpc[pairy[i]] );
      // derive density term by pw
      RF drho_a_dpa  = -(i-pairy[i])/2.*param.getgrav()*param.drho_a_dpa(pa_avg_y,co2avg_y);
      RF drho_a_dco2 = -(i-pairy[i])/2.*param.getgrav()*param.drho_a_dco2(pa_avg_y,co2avg_y);
      mat.accumulate(lfsv_Sa,i,lfsu_pw,      i ,-dx/2.*Karcy *drho_a_dpa/2. );
      mat.accumulate(lfsv_Sa,i,lfsu_pw,pairy[i],-dx/2.*Karcy *drho_a_dpa/2. );
      // derive density term by Sa
      mat.accumulate(lfsv_Sa,i,lfsu_Sa,      i ,-dx/2.*Karcy *drho_a_dpa/2. *dpc[      i ] );
      mat.accumulate(lfsv_Sa,i,lfsu_Sa,pairy[i],-dx/2.*Karcy *drho_a_dpa/2. *dpc[pairy[i]] );
      // derive density term by co2
      mat.accumulate(lfsv_Sa,i,lfsu_co2,      i ,-dx/2.*Karcy *drho_a_dco2/2. );
      mat.accumulate(lfsv_Sa,i,lfsu_co2,pairy[i],-dx/2.*Karcy *drho_a_dco2/2. );
      // derive permeability by pw (that density term...)
      mat.accumulate(lfsv_Sa,i,lfsu_pw,      i ,-dy/2. *h2avg_deriv(Karc[i],Karc[pairx[i]]) *dKarc_dpw[      i ] *nablaax);
      mat.accumulate(lfsv_Sa,i,lfsu_pw,      i ,-dx/2. *h2avg_deriv(Karc[i],Karc[pairy[i]]) *dKarc_dpw[      i ] *nablaay);
      mat.accumulate(lfsv_Sa,i,lfsu_pw,pairx[i],-dy/2. *h2avg_deriv(Karc[pairx[i]],Karc[i]) *dKarc_dpw[pairx[i]] *nablaax);
      mat.accumulate(lfsv_Sa,i,lfsu_pw,pairy[i],-dx/2. *h2avg_deriv(Karc[pairy[i]],Karc[i]) *dKarc_dpw[pairy[i]] *nablaay);
      // derive permeability by Sa (includes density term)
      mat.accumulate(lfsv_Sa,i,lfsu_Sa,      i ,-dy/2. *h2avg_deriv(Karc[i],Karc[pairx[i]]) *dKarc_dSa[      i ] *nablaax);
      mat.accumulate(lfsv_Sa,i,lfsu_Sa,      i ,-dx/2. *h2avg_deriv(Karc[i],Karc[pairy[i]]) *dKarc_dSa[      i ] *nablaay);
      mat.accumulate(lfsv_Sa,i,lfsu_Sa,pairx[i],-dy/2. *h2avg_deriv(Karc[pairx[i]],Karc[i]) *dKarc_dSa[pairx[i]] *nablaax);
      mat.accumulate(lfsv_Sa,i,lfsu_Sa,pairy[i],-dx/2. *h2avg_deriv(Karc[pairy[i]],Karc[i]) *dKarc_dSa[pairy[i]] *nablaay);
      // derive permeability by co2 (density term)
      mat.accumulate(lfsv_Sa,i,lfsu_co2,      i ,-dy/2. *h2avg_deriv(Karc[i],Karc[pairx[i]]) *dKarc_dco2[      i ] *nablaax);
      mat.accumulate(lfsv_Sa,i,lfsu_co2,      i ,-dx/2. *h2avg_deriv(Karc[i],Karc[pairy[i]]) *dKarc_dco2[      i ] *nablaay);
      mat.accumulate(lfsv_Sa,i,lfsu_co2,pairx[i],-dy/2. *h2avg_deriv(Karc[pairx[i]],Karc[i]) *dKarc_dco2[pairx[i]] *nablaax);
      mat.accumulate(lfsv_Sa,i,lfsu_co2,pairy[i],-dx/2. *h2avg_deriv(Karc[pairy[i]],Karc[i]) *dKarc_dco2[pairy[i]] *nablaay);
      // derive permeability by phi
      mat.accumulate(lfsv_Sa,i,lfsu_phi,      i ,-dy/2. *h2avg_deriv(Karc[i],Karc[pairx[i]]) *dKarc_dphi[      i ] *nablaax);
      mat.accumulate(lfsv_Sa,i,lfsu_phi,      i ,-dx/2. *h2avg_deriv(Karc[i],Karc[pairy[i]]) *dKarc_dphi[      i ] *nablaay);
      mat.accumulate(lfsv_Sa,i,lfsu_phi,pairx[i],-dy/2. *h2avg_deriv(Karc[pairx[i]],Karc[i]) *dKarc_dphi[pairx[i]] *nablaax);
      mat.accumulate(lfsv_Sa,i,lfsu_phi,pairy[i],-dx/2. *h2avg_deriv(Karc[pairy[i]],Karc[i]) *dKarc_dphi[pairy[i]] *nablaay);

      // Ca and CO_2 convection
      // qwy /= param.getrho_w();
      // qwx /= param.getrho_w();
      // qay *= param.getR()*param.getT();
      // qax *= param.getR()*param.getT();

      // either I access the same edge twice and divide it by 2,
      // or accumulate only one edge like in water-air part
      // TODO !!! finish rectangular part
      std::size_t branchCax = qwx>0 ? i : pairx[i];
      std::size_t branchCay = qwy>0 ? i : pairy[i];
      // derive Ca convection by Ca
      mat.accumulate(lfsv_Ca,i,lfsu_Ca,branchCax, qwx/param.getrho_w());
      mat.accumulate(lfsv_Ca,i,lfsu_Ca,branchCay, qwy/param.getrho_w());
      // derive Ca convection by pw -flow gradient
      mat.accumulate(lfsv_Ca,i,lfsu_pw,      i , dy/2.*Ca[branchCax]*Kwrx/param.getrho_w()/dx +dx/2.*Ca[branchCay]*Kwry/param.getrho_w()/dy);
      mat.accumulate(lfsv_Ca,i,lfsu_pw,pairx[i],-dy/2.*Ca[branchCax]*Kwrx/param.getrho_w()/dx                            );
      mat.accumulate(lfsv_Ca,i,lfsu_pw,pairy[i],                                              -dx/2.*Ca[branchCay]*Kwry/param.getrho_w()/dy);
      // derive Ca convection by Sa -flow permeability
      mat.accumulate(lfsv_Ca,i,lfsu_Sa,      i ,-dy/2.*Ca[branchCax] *h2avg_deriv(Kwr[i],Kwr[pairx[i]]) *dKwr_dSa[      i ]/param.getrho_w() *nablawx);
      mat.accumulate(lfsv_Ca,i,lfsu_Sa,      i ,-dx/2.*Ca[branchCay] *h2avg_deriv(Kwr[i],Kwr[pairy[i]]) *dKwr_dSa[      i ]/param.getrho_w() *nablawy);
      mat.accumulate(lfsv_Ca,i,lfsu_Sa,pairx[i],-dy/2.*Ca[branchCax] *h2avg_deriv(Kwr[pairx[i]],Kwr[i]) *dKwr_dSa[pairx[i]]/param.getrho_w() *nablawx);
      mat.accumulate(lfsv_Ca,i,lfsu_Sa,pairy[i],-dx/2.*Ca[branchCay] *h2avg_deriv(Kwr[pairy[i]],Kwr[i]) *dKwr_dSa[pairy[i]]/param.getrho_w() *nablawy);
      // derive Ca convection by phi -flow permeability
      mat.accumulate(lfsv_Ca,i,lfsu_phi,      i ,-dy/2.*Ca[branchCax] *h2avg_deriv(Kwr[i],Kwr[pairx[i]]) *dKwr_dphi[      i ]/param.getrho_w() *nablawx);
      mat.accumulate(lfsv_Ca,i,lfsu_phi,      i ,-dx/2.*Ca[branchCay] *h2avg_deriv(Kwr[i],Kwr[pairy[i]]) *dKwr_dphi[      i ]/param.getrho_w() *nablawy);
      mat.accumulate(lfsv_Ca,i,lfsu_phi,pairx[i],-dy/2.*Ca[branchCax] *h2avg_deriv(Kwr[pairx[i]],Kwr[i]) *dKwr_dphi[pairx[i]]/param.getrho_w() *nablawx);
      mat.accumulate(lfsv_Ca,i,lfsu_phi,pairy[i],-dx/2.*Ca[branchCay] *h2avg_deriv(Kwr[pairy[i]],Kwr[i]) *dKwr_dphi[pairy[i]]/param.getrho_w() *nablawy);

      std::size_t branchco2x = qax>0 ? i : pairx[i];
      std::size_t branchco2y = qay>0 ? i : pairy[i];
      // derive co2 convection by co2
      mat.accumulate(lfsv_co2,i,lfsu_co2,branchco2x, qax*param.getR()*param.getT()*(1./p_h.getMa(co2[branchco2x])+(p_h.getMa(co2[branchco2x])*p_h.getMa(co2[branchco2x]))*(-1.)*p_h.getMa_deriv(co2[branchco2x])*co2[branchco2x]) );
      mat.accumulate(lfsv_co2,i,lfsu_co2,branchco2y, qay*param.getR()*param.getT()*(1./p_h.getMa(co2[branchco2y])+(p_h.getMa(co2[branchco2y])*p_h.getMa(co2[branchco2y]))*(-1.)*p_h.getMa_deriv(co2[branchco2y])*co2[branchco2y]) );
      // derive co2 convection by pw -flow gradient
      mat.accumulate(lfsv_co2,i,lfsu_pw,      i , dy/2.*co2_RTMa[branchco2x]*Karcx/dx );
      mat.accumulate(lfsv_co2,i,lfsu_pw,      i , dx/2.*co2_RTMa[branchco2y]*Karcy/dy );
      mat.accumulate(lfsv_co2,i,lfsu_pw,pairx[i],-dy/2.*co2_RTMa[branchco2x]*Karcx/dx );
      mat.accumulate(lfsv_co2,i,lfsu_pw,pairy[i],-dx/2.*co2_RTMa[branchco2y]*Karcy/dy );
      // derive co2 convection by Sa -flow gradient
      mat.accumulate(lfsv_co2,i,lfsu_Sa,      i , dy/2.*co2_RTMa[branchco2x]*Karcx/dx *dpc[      i ] );
      mat.accumulate(lfsv_co2,i,lfsu_Sa,      i , dx/2.*co2_RTMa[branchco2y]*Karcy/dy *dpc[      i ] );
      mat.accumulate(lfsv_co2,i,lfsu_Sa,pairx[i],-dy/2.*co2_RTMa[branchco2x]*Karcx/dx *dpc[pairx[i]] );
      mat.accumulate(lfsv_co2,i,lfsu_Sa,pairy[i],-dx/2.*co2_RTMa[branchco2y]*Karcy/dy *dpc[pairy[i]] );
      // derive co2 convection by pw (-> flow -> density inside gravity term)
      mat.accumulate(lfsv_co2,i,lfsu_pw,      i ,-dx/2.*co2_RTMa[branchco2y]*Karcy*drho_a_dpa/2. );
      mat.accumulate(lfsv_co2,i,lfsu_pw,pairy[i],-dx/2.*co2_RTMa[branchco2y]*Karcy*drho_a_dpa/2. );
      // derive co2 convection by Sa (-> flow -> density inside gravity term)
      mat.accumulate(lfsv_co2,i,lfsu_Sa,      i ,-dx/2.*co2_RTMa[branchco2y]*Karcy*drho_a_dpa/2. *dpc[      i ] );
      mat.accumulate(lfsv_co2,i,lfsu_Sa,pairy[i],-dx/2.*co2_RTMa[branchco2y]*Karcy*drho_a_dpa/2. *dpc[pairy[i]] );
      // derive co2 convection by co2 (-> flow -> density inside gravity term)
      mat.accumulate(lfsv_co2,i,lfsu_co2,      i ,-dx/2.*co2_RTMa[branchco2y]*Karcy*drho_a_dco2/2. );
      mat.accumulate(lfsv_co2,i,lfsu_co2,pairy[i],-dx/2.*co2_RTMa[branchco2y]*Karcy*drho_a_dco2/2. );
      // derive co2 convection by pw -permeability (density term inside)
      mat.accumulate(lfsv_co2,i,lfsu_pw,      i ,-dy/2.*co2_RTMa[branchco2x] *h2avg_deriv(Karc[i],Karc[pairx[i]]) *dKarc_dpw[      i ] *nablaax);
      mat.accumulate(lfsv_co2,i,lfsu_pw,      i ,-dx/2.*co2_RTMa[branchco2y] *h2avg_deriv(Karc[i],Karc[pairy[i]]) *dKarc_dpw[      i ] *nablaay);
      mat.accumulate(lfsv_co2,i,lfsu_pw,pairx[i],-dy/2.*co2_RTMa[branchco2x] *h2avg_deriv(Karc[pairx[i]],Karc[i]) *dKarc_dpw[pairx[i]] *nablaax);
      mat.accumulate(lfsv_co2,i,lfsu_pw,pairy[i],-dx/2.*co2_RTMa[branchco2y] *h2avg_deriv(Karc[pairy[i]],Karc[i]) *dKarc_dpw[pairy[i]] *nablaay);
      // derive co2 convection by Sa -permeability (includes density term)
      mat.accumulate(lfsv_co2,i,lfsu_Sa,      i ,-dy/2.*co2_RTMa[branchco2x] *h2avg_deriv(Karc[i],Karc[pairx[i]]) *dKarc_dSa[      i ] *nablaax);
      mat.accumulate(lfsv_co2,i,lfsu_Sa,      i ,-dx/2.*co2_RTMa[branchco2y] *h2avg_deriv(Karc[i],Karc[pairy[i]]) *dKarc_dSa[      i ] *nablaay);
      mat.accumulate(lfsv_co2,i,lfsu_Sa,pairx[i],-dy/2.*co2_RTMa[branchco2x] *h2avg_deriv(Karc[pairx[i]],Karc[i]) *dKarc_dSa[pairx[i]] *nablaax);
      mat.accumulate(lfsv_co2,i,lfsu_Sa,pairy[i],-dx/2.*co2_RTMa[branchco2y] *h2avg_deriv(Karc[pairy[i]],Karc[i]) *dKarc_dSa[pairy[i]] *nablaay);
      // derive co2 convection by co2 -permeability (density term only)
      mat.accumulate(lfsv_co2,i,lfsu_co2,      i ,-dy/2.*co2_RTMa[branchco2x] *h2avg_deriv(Karc[i],Karc[pairx[i]]) *dKarc_dco2[      i ] *nablaax);
      mat.accumulate(lfsv_co2,i,lfsu_co2,      i ,-dx/2.*co2_RTMa[branchco2y] *h2avg_deriv(Karc[i],Karc[pairy[i]]) *dKarc_dco2[      i ] *nablaay);
      mat.accumulate(lfsv_co2,i,lfsu_co2,pairx[i],-dy/2.*co2_RTMa[branchco2x] *h2avg_deriv(Karc[pairx[i]],Karc[i]) *dKarc_dco2[pairx[i]] *nablaax);
      mat.accumulate(lfsv_co2,i,lfsu_co2,pairy[i],-dx/2.*co2_RTMa[branchco2y] *h2avg_deriv(Karc[pairy[i]],Karc[i]) *dKarc_dco2[pairy[i]] *nablaay);
      // derive co2 convection by phi -permeability
      mat.accumulate(lfsv_co2,i,lfsu_phi,      i ,-dy/2.*co2_RTMa[branchco2x] *h2avg_deriv(Karc[i],Karc[pairx[i]]) *dKarc_dphi[      i ] *nablaax);
      mat.accumulate(lfsv_co2,i,lfsu_phi,      i ,-dx/2.*co2_RTMa[branchco2y] *h2avg_deriv(Karc[i],Karc[pairy[i]]) *dKarc_dphi[      i ] *nablaay);
      mat.accumulate(lfsv_co2,i,lfsu_phi,pairx[i],-dy/2.*co2_RTMa[branchco2x] *h2avg_deriv(Karc[pairx[i]],Karc[i]) *dKarc_dphi[pairx[i]] *nablaax);
      mat.accumulate(lfsv_co2,i,lfsu_phi,pairy[i],-dx/2.*co2_RTMa[branchco2y] *h2avg_deriv(Karc[pairy[i]],Karc[i]) *dKarc_dphi[pairy[i]] *nablaay);
    }

    // Ca and CO_2 diffusion term -------------------------------------------------------------------------------------------------------------
    // r.accumulate(lfsv_Ca,i,
    //       dx/2.*(Ca[i]-Ca[pairy[i]])/dy*h2avg(Dw[i],Dw[pairy[i]])
    //      +dy/2.*(Ca[i]-Ca[pairx[i]])/dx*h2avg(Dw[i],Dw[pairx[i]]) );
    // r.accumulate(lfsv_co2,i,
    //       dx/2.*( co2[i]-co2[pairy[i]] )/dy
    //            *  h2avg( (pa[i]+param.getpa0())*Da[i],(pa[pairy[i]]+param.getpa0())*Da[pairy[i]] )
    //      +dy/2.*( co2[i]-co2[pairx[i]] )/dx
    //            *  h2avg( (pa[i]+param.getpa0())*Da[i],(pa[pairx[i]]+param.getpa0())*Da[pairx[i]] ) );
    std::array<RF,lfsusize> Dw, pap, Da, pa_Da_Ma, Ma;
    for (std::size_t i=0; i<lfsusize; ++i)
    {
      Dw[i] = p_h.getdifCa(phi[i],1.-Sa[i]);
      pap[i] = pa[i]+param.getpa0();
      Da[i] = p_h.getdifco2(phi[i],Sa[i]);
      Ma[i] = p_h.getMa(co2[i])/p_h.getMco2();
      pa_Da_Ma[i] = pap[i]*Da[i]*Ma[i];
    }

    for (std::size_t i=0; i<lfsusize; ++i)
    {
      // derive Ca gradient by Ca
      mat.accumulate(lfsv_Ca,i,lfsu_Ca,      i,  dy/2./dx *h2avg(Dw[i],Dw[pairx[i]]) );
      mat.accumulate(lfsv_Ca,i,lfsu_Ca,      i,  dx/2./dy *h2avg(Dw[i],Dw[pairy[i]]) );
      mat.accumulate(lfsv_Ca,i,lfsu_Ca,pairx[i],-dy/2./dx *h2avg(Dw[i],Dw[pairx[i]]) );
      mat.accumulate(lfsv_Ca,i,lfsu_Ca,pairy[i],-dx/2./dy *h2avg(Dw[i],Dw[pairy[i]]) );
      // derive Dw by Sa
      mat.accumulate(lfsv_Ca,i,lfsu_Sa,      i,( dy/2.*(Ca[i]-Ca[pairx[i]])/dx *h2avg_deriv(Dw[i],Dw[pairx[i]])
                                              +dx/2.*(Ca[i]-Ca[pairy[i]])/dy *h2avg_deriv(Dw[i],Dw[pairy[i]]))*p_h.ddifCa_dSw(phi[      i ],1.-Sa[      i ])*(-1.) );
      mat.accumulate(lfsv_Ca,i,lfsu_Sa,pairx[i], dy/2.*(Ca[i]-Ca[pairx[i]])/dx *h2avg_deriv(Dw[pairx[i]],Dw[i]) *p_h.ddifCa_dSw(phi[pairx[i]],1.-Sa[pairx[i]])*(-1.) );
      mat.accumulate(lfsv_Ca,i,lfsu_Sa,pairy[i], dx/2.*(Ca[i]-Ca[pairy[i]])/dy *h2avg_deriv(Dw[pairy[i]],Dw[i]) *p_h.ddifCa_dSw(phi[pairy[i]],1.-Sa[pairy[i]])*(-1.) );
      // derive Dw by phi
      mat.accumulate(lfsv_Ca,i,lfsu_phi,      i,( dy/2.*(Ca[i]-Ca[pairx[i]])/dx *h2avg_deriv(Dw[i],Dw[pairx[i]])
                                              +dx/2.*(Ca[i]-Ca[pairy[i]])/dy *h2avg_deriv(Dw[i],Dw[pairy[i]]))*p_h.ddifCa_dphi(phi[      i ],1.-Sa[      i ]) );
      mat.accumulate(lfsv_Ca,i,lfsu_phi,pairx[i], dy/2.*(Ca[i]-Ca[pairx[i]])/dx *h2avg_deriv(Dw[pairx[i]],Dw[i]) *p_h.ddifCa_dphi(phi[pairx[i]],1.-Sa[pairx[i]]) );
      mat.accumulate(lfsv_Ca,i,lfsu_phi,pairy[i], dx/2.*(Ca[i]-Ca[pairy[i]])/dy *h2avg_deriv(Dw[pairy[i]],Dw[i]) *p_h.ddifCa_dphi(phi[pairy[i]],1.-Sa[pairy[i]]) );

      // derive co2 gradient by co2
      mat.accumulate(lfsv_co2,i,lfsu_co2,      i,  dy/2./dx *h2avg(pa_Da_Ma[i] ,pa_Da_Ma[pairx[i]]) );
      mat.accumulate(lfsv_co2,i,lfsu_co2,      i,  dx/2./dy *h2avg(pa_Da_Ma[i] ,pa_Da_Ma[pairy[i]]) );
      mat.accumulate(lfsv_co2,i,lfsu_co2,pairx[i],-dy/2./dx *h2avg(pa_Da_Ma[i] ,pa_Da_Ma[pairx[i]]) );
      mat.accumulate(lfsv_co2,i,lfsu_co2,pairy[i],-dx/2./dy *h2avg(pa_Da_Ma[i] ,pa_Da_Ma[pairy[i]]) );

      RF factor_iy = dx/2.*(co2[i]-co2[pairy[i]])/dy *h2avg_deriv(pa_Da_Ma[i],pa_Da_Ma[pairy[i]]);
      RF factor_ix = dy/2.*(co2[i]-co2[pairx[i]])/dx *h2avg_deriv(pa_Da_Ma[i],pa_Da_Ma[pairx[i]]);
      RF factor_yi = dx/2.*(co2[i]-co2[pairy[i]])/dy *h2avg_deriv(pa_Da_Ma[pairy[i]],pa_Da_Ma[i]);
      RF factor_xi = dy/2.*(co2[i]-co2[pairx[i]])/dx *h2avg_deriv(pa_Da_Ma[pairx[i]],pa_Da_Ma[i]);
      // derive pa by pw
      mat.accumulate(lfsv_co2,i,lfsu_pw,      i,( factor_ix
                                                 +factor_iy)*Da[      i ]*Ma[      i ] );
      mat.accumulate(lfsv_co2,i,lfsu_pw,pairx[i], factor_xi *Da[pairx[i]]*Ma[pairx[i]] );
      mat.accumulate(lfsv_co2,i,lfsu_pw,pairy[i], factor_yi *Da[pairy[i]]*Ma[pairy[i]] );
      // derive pa by Sa
      mat.accumulate(lfsv_co2,i,lfsu_Sa,      i,( factor_ix
                                                 +factor_iy)*Da[      i ]*Ma[      i ]*dpc[      i ] );
      mat.accumulate(lfsv_co2,i,lfsu_Sa,pairx[i], factor_xi *Da[pairx[i]]*Ma[pairx[i]]*dpc[pairx[i]] );
      mat.accumulate(lfsv_co2,i,lfsu_Sa,pairy[i], factor_yi *Da[pairy[i]]*Ma[pairy[i]]*dpc[pairy[i]] );
      // derive Da by Sa
      mat.accumulate(lfsv_co2,i,lfsu_Sa,      i,( factor_ix
                                                 +factor_iy)*pap[      i ]*Ma[      i ]*p_h.ddifco2_dSa(phi[      i ],Sa[      i ]) );
      mat.accumulate(lfsv_co2,i,lfsu_Sa,pairx[i], factor_xi *pap[pairx[i]]*Ma[pairx[i]]*p_h.ddifco2_dSa(phi[pairx[i]],Sa[pairx[i]]) );
      mat.accumulate(lfsv_co2,i,lfsu_Sa,pairy[i], factor_yi *pap[pairy[i]]*Ma[pairy[i]]*p_h.ddifco2_dSa(phi[pairy[i]],Sa[pairy[i]]) );
      // derive Da by phi
      mat.accumulate(lfsv_co2,i,lfsu_phi,      i,( factor_ix
                                                  +factor_iy)*pap[      i ]*Ma[      i ]*p_h.ddifco2_dphi(phi[      i ],Sa[      i ]) );
      mat.accumulate(lfsv_co2,i,lfsu_phi,pairx[i], factor_xi *pap[pairx[i]]*Ma[pairx[i]]*p_h.ddifco2_dphi(phi[pairx[i]],Sa[pairx[i]]) );
      mat.accumulate(lfsv_co2,i,lfsu_phi,pairy[i], factor_yi *pap[pairy[i]]*Ma[pairy[i]]*p_h.ddifco2_dphi(phi[pairy[i]],Sa[pairy[i]]) );
      // derive Ma by co2
      mat.accumulate(lfsv_co2,i,lfsu_co2,      i,( factor_ix
                                                  +factor_iy)*pap[      i ]*Da[      i ]*p_h.getMa_deriv(co2[      i ])/p_h.getMco2() );
      mat.accumulate(lfsv_co2,i,lfsu_co2,pairx[i], factor_xi *pap[pairx[i]]*Da[pairx[i]]*p_h.getMa_deriv(co2[pairx[i]])/p_h.getMco2() );
      mat.accumulate(lfsv_co2,i,lfsu_co2,pairy[i], factor_yi *pap[pairy[i]]*Da[pairy[i]]*p_h.getMa_deriv(co2[pairy[i]])/p_h.getMco2() );
    }

  // dispersion terms
  // r.accumulate(lfsv_Ca,i,
  //       dy/2.*(Dw11*(Ca[i]-Ca[pairx[i]])/dx+Dw12*Cadery*(i-pairx[i])  ) // (i-pairx[i]) works as a unit normal vector
  //      +dx/2.*(Dw22*(Ca[i]-Ca[pairy[i]])/dy+Dw12*Caderx*(i-pairy[i])/2) );
  // r.accumulate(lfsv_co2,i,
  //       dy/2.*(Da11*(co2[i]-co2[pairx[i]])/dx+Da12*co2dery*(i-pairx[i])  )
  //      +dx/2.*(Da22*(co2[i]-co2[pairy[i]])/dy+Da12*co2derx*(i-pairy[i])/2) );    const RF& a_T = p_h.getalfat();
  const RF& a_L = p_h.getalfal();
  const RF& a_T = p_h.getalfat();
  RF size = static_cast<RF>(lfsusize);
  RF avgKwr = mojefunkcie::hNavg(Kwr,"Kwr for dispersion in jacobian_volume");
  RF vwx = -avgKwr* (pw[3]-pw[2]+pw[1]-pw[0])/2./dx/param.getrho_w();
  RF vwy = -avgKwr*((pw[3]-pw[1]+pw[2]-pw[0])/2./dy/param.getrho_w()-param.getgrav());
  RF vwsqrt = std::max(1e-5, sqrt(vwx*vwx+vwy*vwy));
  RF sat{0};
  for (const auto& v : Sa)
    sat+=v;
  sat/=static_cast<RF>(lfsusize);
  RF Dw11 = (a_L*vwx*vwx+a_T*vwy*vwy)/vwsqrt/((1.-sat)*(1.-sat));
  RF Dw22 = (a_T*vwx*vwx+a_L*vwy*vwy)/vwsqrt/((1.-sat)*(1.-sat));
  RF Dw12 = (a_L-a_T)*vwx*vwy/vwsqrt/((1.-sat)*(1.-sat));
  const RF Caderx = (Ca[3]-Ca[2]+Ca[1]-Ca[0])/dx/2.;
  const RF Cadery = (Ca[3]-Ca[1]+Ca[2]-Ca[0])/dy/2.;

  // prepare helpful constants
  // Ca dispersion by pw: gradient in velocity,
  RF dvwx_dpw = -avgKwr/2./dx/param.getrho_w(); // correct up to a sign
  RF dvwy_dpw = -avgKwr/2./dy/param.getrho_w();
  for (int j=0; j<int(lfsusize); ++j)
  {
    RF dvwx_dpwj = dvwx_dpw*(j-pairx[j]);
    RF dvwy_dpwj = dvwy_dpw*((j-pairy[j])/2);
    RF dvwsqrt_dpwj = vwsqrt==1e-5 ? 0. : 0.5/vwsqrt*(2.*vwx*dvwx_dpwj+2.*vwy*dvwy_dpwj);
    RF dDw11_dpwj = (a_L*2*vwx*dvwx_dpwj+a_T*2*vwy*dvwy_dpwj)/vwsqrt/((1.-sat)*(1.-sat))
                 +Dw11/vwsqrt*(-1.)*dvwsqrt_dpwj;
    RF dDw12_dpwj = (a_L-a_T)/((1.-sat)*(1.-sat))*(dvwx_dpwj*vwy+vwx*dvwy_dpwj)/vwsqrt
                 +Dw12/vwsqrt*(-1.)*dvwsqrt_dpwj ;
    RF dDw22_dpwj = (a_T*2*vwx*dvwx_dpwj+a_L*2*vwy*dvwy_dpwj)/vwsqrt/((1.-sat)*(1.-sat))
                 +Dw22/vwsqrt*(-1.)*dvwsqrt_dpwj;
    for (int i=0; i<int(lfsusize); ++i)
      mat.accumulate(lfsv_Ca,i,lfsu_pw,j,
             dy/2.*(dDw11_dpwj*(Ca[i]-Ca[pairx[i]])/dx+dDw12_dpwj*Cadery*(i-pairx[i])  ) // (i-pairx[i]) works as a unit normal vector
            +dx/2.*(dDw22_dpwj*(Ca[i]-Ca[pairy[i]])/dy+dDw12_dpwj*Caderx*(i-pairy[i])/2) );
  }
  // Ca dispersion by Sa: average permeability in velocity
  //                      and 1/(1-Sa)^2 weights for unsaturated dispersion
  for (int j=0; j<int(lfsusize); ++j)
  {
    // can be simplified, extra terms can be extracted to a single factor
    RF dsat_dSaj = 1./size;
    RF daKwr_dSaj = avgKwr/(size*Kwr[j]*Kwr[j])*dKwr_dSa[j];
    // RF dvwx_dSaj = vwx*daKwr_dSaj;
    // RF dvwy_dSaj = vwy*daKwr_dSaj;
    // RF dvwsqrt_dSaj = vwsqrt==1e-5 ? 0. : 0.5/vwsqrt*(2.*vwx*dvwx_dSaj+2.*vwy*dvwy_dSaj);
    // RF dvwsqrt_dSaj = vwsqrt==1e-5 ? 0. : vwsqrt*daKwr_dSaj;
    // RF dDw11_dSaj = (a_L*2*vwx*dvwx_dSaj+a_T*2*vwy*dvwy_dSaj)/vwsqrt/((1.-sat)*(1.-sat))
    //               +(a_L*vwx*vwx+a_T*vwy*vwy)/vwsqrt/vwsqrt*(-1.)*dvwsqrt_dSaj/((1.-sat)*(1.-sat))
    //               +(a_L*vwx*vwx+a_T*vwy*vwy)/vwsqrt/((1.-sat)*(1.-sat)*(1.-sat))*(-2.)*(-1.)*dsat_dSaj;
    // RF dDw12_dSaj = (a_L-a_T)/((1.-sat)*(1.-sat))
    //                *( dvwx_dSaj*vwy/vwsqrt+vwx*dvwy_dSaj/vwsqrt
    //                   +vwx*vwy/(vwsqrt*vwsqrt)*(-1.)*dvwsqrt_dSaj )
    //                +(a_L-a_T)/((1.-sat)*(1.-sat)*(1.-sat))*(-2.)*(-1.)*dsat_dSaj
    //                *vwx*vwy/vwsqrt;
    // RF dDw22_dSaj = (a_T*2*vwx*dvwx_dSaj+a_L*2*vwy*dvwy_dSaj)/vwsqrt/((1.-sat)*(1.-sat))
    //              +(a_T*vwx*vwx+a_L*vwy*vwy)/(vwsqrt*vwsqrt)*(-1.)*dvwsqrt_dSaj/((1.-sat)*(1.-sat))
    //              +(a_T*vwx*vwx+a_L*vwy*vwy)/vwsqrt/((1.-sat)*(1.-sat)*(1.-sat))*(-2.)*(-1.)*dsat_dSaj;
    RF dSa = (vwsqrt==1e-5 ? 2. : 1.)*daKwr_dSaj+2./(1.-sat)*dsat_dSaj;
    RF dDw11_dSaj = Dw11*dSa;
    RF dDw12_dSaj = Dw12*dSa;
    RF dDw22_dSaj = Dw22*dSa;

    for (int i=0; i<int(lfsusize); ++i)
      mat.accumulate(lfsv_Ca,i,lfsu_Sa,j,
             dy/2.*(dDw11_dSaj*(Ca[i]-Ca[pairx[i]])/dx+dDw12_dSaj*Cadery*(i-pairx[i])  ) // (i-pairx[i]) works as a unit normal vector
            +dx/2.*(dDw22_dSaj*(Ca[i]-Ca[pairy[i]])/dy+dDw12_dSaj*Caderx*(i-pairy[i])/2) );
  }
  // calcium dispersion by Ca: Ca gradients
  for (int i=0; i<int(lfsusize); ++i)
  {
    int sign = (i-pairx[i])*(i-pairy[i])/2;
    mat.accumulate(lfsv_Ca,i,lfsu_Ca,i,
             dy/2.*(  Dw11/dx + Dw12/dy/2.*  sign)
            +dx/2.*(  Dw22/dy + Dw12/dx/2.*  sign) );
    mat.accumulate(lfsv_Ca,i,lfsu_Ca,pairx[i],
             dy/2.*( -Dw11/dx + Dw12/dy/2.*  sign )    // pairx[i] has different sign at Dw11, but same at Dw12
            +dx/2.*(            Dw12/dx/2.*(-sign)) ); // does not appear at Dw22, different sign at Dw21=Dw12
    mat.accumulate(lfsv_Ca,i,lfsu_Ca,pairy[i],
             dy/2.*(            Dw12/dy/2.*(-sign))
            +dx/2.*( -Dw22/dy + Dw12/dx/2.*  sign ) );
    mat.accumulate(lfsv_Ca,i,lfsu_Ca,3-i, // 3-i == pairx[pairy[i]] == pairy[pairx[i]]
             dy/2.*(            Dw12/dy/2.*(-sign))
            +dx/2.*(            Dw12/dx/2.*(-sign)) );
  }
  // Ca dispersion by phi: average permeability in velocity
  // similar to Sa in average permeability
  for (int j=0; j<int(lfsusize); ++j)
  {
    RF dphi = avgKwr/(size*Kwr[j]*Kwr[j])*dKwr_dphi[j]*(vwsqrt==1e-5 ? 2. : 1.);
    RF dDw11_dphij = Dw11*dphi;
    RF dDw12_dphij = Dw12*dphi;
    RF dDw22_dphij = Dw22*dphi;

    for (int i=0; i<int(lfsusize); ++i)
      mat.accumulate(lfsv_Ca,i,lfsu_phi,j,
             dy/2.*(dDw11_dphij*(Ca[i]-Ca[pairx[i]])/dx+dDw12_dphij*Cadery*(i-pairx[i])  ) // (i-pairx[i]) works as a unit normal vector
            +dx/2.*(dDw22_dphij*(Ca[i]-Ca[pairy[i]])/dy+dDw12_dphij*Caderx*(i-pairy[i])/2) );
  }

/*  // co2 dispersion, prepare some constants
  RF avgKac = mojefunkcie::hNavg(Kac,"Kac for dispersion in jacobian_volume");
  std::array<RF,lfsusize> rhoa;
  for (size_t j=0; j<lfsusize; ++j)
    rhoa[j] = param.getrho_a(pa[j],co2[j]);
  RF arhoa = mojefunkcie::aritavg(rhoa);
  RF vax = -avgKac* (pa[3]-pa[2]+pa[1]-pa[0])/2./dx;
  RF vay = -avgKac*((pa[3]-pa[1]+pa[2]-pa[0])/2./dy-param.getgrav()*arhoa);
  RF vasqrt = std::max(1e-5, sqrt(vax*vax+vay*vay));
  RF Da11_ = (a_L*vax*vax+a_T*vay*vay)/vasqrt*sat*sat;
  RF Da22_ = (a_T*vax*vax+a_L*vay*vay)/vasqrt*sat*sat;
  RF Da12_ = (a_L-a_T)*vax*vay/vasqrt*sat*sat;
  RF avgpap = mojefunkcie::aritavg(pa)+param.getpa0();
  RF avgMa = mojefunkcie::aritavg(Ma)/p_h.getMco2();
  RF pM = avgpap*avgMa;
  RF Da11 = Da11_*pM;
  RF Da22 = Da22_*pM;
  RF Da12 = Da12_*pM;

  // deriving harmonic average squares demoninator and multiplies by its derivative
  // Havg = N/\sum{1/x_i}; dHavgdx_i = N/(\sum{1/x_i})^2*(-1)*(1/x_i)^2*(-1)
  std::array<RF,lfsusize> davgKac_dj;
  for (size_t i=0; i<lfsusize; ++i)
    davgKac_dj[i] = avgKac*avgKac/(size*Kac[i]*Kac[i]);
  // co2 dispersion by pw: pressure air gradient in velocity,
  // co2 dispersion by pw: average density in velocity -gravity term,
  const RF co2derx = (co2[3]-co2[2]+co2[1]-co2[0])/dx/2.;
  const RF co2dery = (co2[3]-co2[1]+co2[2]-co2[0])/dy/2.;
  for (int j=0; j<int(lfsusize); ++j)
  {
    RF dvax_dpwj = -avgKac/2./dx*(j-pairx[j]); // dpa_dpwj = 1.
    RF dvay_dpwj = -avgKac/2./dy*((j-pairy[j])/2)
                   -avgKac*(-param.getgrav())*param.drho_a_dpa(pa[j],co2[j])/lfsusize;
    RF dvasqrt_dpwj = vasqrt==1e-5 ? 0. : (vax*dvax_dpwj+vay*dvay_dpwj)/vasqrt;
    RF dDa11_dpwj = (a_L*2.*vax*dvax_dpwj+a_T*2.*vay*dvay_dpwj)/vasqrt*sat*sat*pM
                    +Da11*(-1.)/vasqrt*dvasqrt_dpwj;
    RF dDa22_dpwj = (a_T*2.*vax*dvax_dpwj+a_L*2.*vay*dvay_dpwj)/vasqrt*sat*sat*pM
                    +Da22*(-1.)/vasqrt*dvasqrt_dpwj;
    RF dDa12_dpwj = (a_L-a_T)*(vax*dvay_dpwj+dvax_dpwj*vay)/vasqrt*sat*sat*pM
                    +Da12*(-1.)/vasqrt*dvasqrt_dpwj;
    dDa11_dpwj *= 0.;
    dDa22_dpwj *= 0.;
    dDa12_dpwj *= 0.;
    for (int i=0; i<int(lfsusize); ++i)
      mat.accumulate(lfsv_co2,i,lfsu_pw,j,
            dy/2.*(dDa11_dpwj*(co2[i]-co2[pairx[i]])/dx+dDa12_dpwj*co2dery*(i-pairx[i])  )
           +dx/2.*(dDa22_dpwj*(co2[i]-co2[pairy[i]])/dy+dDa12_dpwj*co2derx*(i-pairy[i])/2) );
  }
  // co2 dispersion by pw: pa in pa_Da_Ma
  for (int j=0; j<int(lfsusize); ++j)
  {
    RF dpM_dpwj = avgMa/size;
    for (int i=0; i<int(lfsusize); ++i)
      mat.accumulate(lfsv_co2,i,lfsu_pw,j,
            dy/2.*(Da11_*dpM_dpwj*(co2[i]-co2[pairx[i]])/dx+Da12_*dpM_dpwj*co2dery*(i-pairx[i])  )
           +dx/2.*(Da22_*dpM_dpwj*(co2[i]-co2[pairy[i]])/dy+Da12_*dpM_dpwj*co2derx*(i-pairy[i])/2) );
  }
  // co2 dispersion by Sa: pressure air gradient in velocity,
  // co2 dispersion by Sa: average density in velocity -gravity term
  // co2 dispersion by Sa: average permeability,
  // co2 dispersion by Sa: 1/Sa^2 weight for unsaturated dispersion
  for (int j=0; j<int(lfsusize); ++j)
  {
    RF dsat_dSaj = 1./size;
    RF dKac_dSaj = param.dKac_dSa(Sa[j],phi[j],co2[j]);
    RF davgKac_dSaj = davgKac_dj[j]*dKac_dSaj;
    RF dvax_dSaj = -avgKac*(j-pairx[j])/2./dx*dpc[j]
                   -davgKac_dSaj*(pa[3]-pa[2]+pa[1]-pa[0])/2./dx;
    RF dvay_dSaj = -avgKac*(((j-pairy[j])/2)/2./dy*dpc[j]-param.getgrav()*param.drho_a_dpa(pa[j],co2[j])*dpc[j]/lfsusize)
                   -davgKac_dSaj*((pa[3]-pa[1]+pa[2]-pa[0])/2./dy-param.getgrav()*arhoa);
    RF dvasqrt_dSaj = vasqrt==1e-5 ? 0. : 1./vasqrt*(vax*dvax_dSaj+vay*dvay_dSaj);
    RF dDa11_dSaj = (a_L*2.*vax*dvax_dSaj+a_T*2.*vay*dvay_dSaj)/vasqrt*sat*sat*pM
                   +Da11/vasqrt*(-1.)*dvasqrt_dSaj
                   +Da11/sat*2.*dsat_dSaj;
    RF dDa22_dSaj = (a_T*2.*vax*dvax_dSaj+a_L*2.*vay*dvay_dSaj)/vasqrt*sat*sat*pM
                   +Da22/vasqrt*(-1.)*dvasqrt_dSaj
                   +Da22/sat*2.*dsat_dSaj;
    RF dDa12_dSaj = (a_L-a_T)*(dvax_dSaj*vay+vax*dvay_dSaj)/vasqrt*sat*sat*pM
                   +Da12/vasqrt*(-1.)*dvasqrt_dSaj
                   +Da12/sat*2.*dsat_dSaj;

    for (int i=0; i<int(lfsusize); ++i)
      mat.accumulate(lfsv_co2,i,lfsu_Sa,j,
          dy/2.*(dDa11_dSaj*(co2[i]-co2[pairx[i]])/dx+dDa12_dSaj*co2dery*(i-pairx[i])  )
         +dx/2.*(dDa22_dSaj*(co2[i]-co2[pairy[i]])/dy+dDa12_dSaj*co2derx*(i-pairy[i])/2) );
  }
  // co2 dispersion by Sa: pa in pa_Da_Ma
  for (int j=0; j<int(lfsusize); ++j)
  {
    RF dpM_dSaj = avgMa/size*dpc[j];
    for (int i=0; i<int(lfsusize); ++i)
      mat.accumulate(lfsv_co2,i,lfsu_Sa,j,
            dy/2.*(Da11_*dpM_dSaj*(co2[i]-co2[pairx[i]])/dx+Da12_*dpM_dSaj*co2dery*(i-pairx[i])  )
           +dx/2.*(Da22_*dpM_dSaj*(co2[i]-co2[pairy[i]])/dy+Da12_*dpM_dSaj*co2derx*(i-pairy[i])/2) );
  }

  // co2 dispersion by co2: co2 gradient
  for (int i=0; i<int(lfsusize); ++i)
  {
    int sign = (i-pairx[i])*(i-pairy[i])/2;
    mat.accumulate(lfsv_co2,i,lfsu_co2,i,
             dy/2.*(  Da11/dx + Da12/dy/2.*  sign)
            +dx/2.*(  Da22/dy + Da12/dx/2.*  sign) );
    mat.accumulate(lfsv_co2,i,lfsu_co2,pairx[i],
             dy/2.*( -Da11/dx + Da12/dy/2.*  sign )
            +dx/2.*(            Da12/dx/2.*(-sign)) );
    mat.accumulate(lfsv_co2,i,lfsu_co2,pairy[i],
             dy/2.*(            Da12/dy/2.*(-sign))
            +dx/2.*( -Da22/dy + Da12/dx/2.*  sign ) );
    mat.accumulate(lfsv_co2,i,lfsu_co2,3-i,
             dy/2.*(            Da12/dy/2.*(-sign))
            +dx/2.*(            Da12/dx/2.*(-sign)) );
  }
  // co2 dispersion by co2: average permeability,
  //                        density in gravity term in velocity
  for (int j=0; j<int(lfsusize); ++j)
  {
    RF dKac_dco2j = param.dKac_dco2(Sa[j],phi[j],co2[j]);
    RF davgKac_dco2j = davgKac_dj[j]*dKac_dco2j;
    RF dvax_dco2j = -davgKac_dco2j*(pa[3]-pa[2]+pa[1]-pa[0])/2./dx;
    RF dvay_dco2j = -avgKac*(-param.getgrav()*param.drho_a_dco2(pa[j],co2[j])/lfsusize)
                    -davgKac_dco2j*((pa[3]-pa[1]+pa[2]-pa[0])/2./dy-param.getgrav()*arhoa);
    RF dvasqrt_dco2j = vasqrt==1e-5 ? 0. : 1./vasqrt*(vax*dvax_dco2j+vay*dvay_dco2j);
    RF dDa11_dco2j = (a_L*2.*vax*dvax_dco2j+a_T*2.*vay*dvay_dco2j)/vasqrt*sat*sat*pM
                   +Da11/vasqrt*(-1.)*dvasqrt_dco2j;
    RF dDa22_dco2j = (a_T*2.*vax*dvax_dco2j+a_L*2.*vay*dvay_dco2j)/vasqrt*sat*sat*pM
                   +Da22/vasqrt*(-1.)*dvasqrt_dco2j;
    RF dDa12_dco2j = (a_L-a_T)*(dvax_dco2j*vay+vax*dvay_dco2j)/vasqrt*sat*sat*pM
                   +Da12/vasqrt*(-1.)*dvasqrt_dco2j;

    for (int i=0; i<int(lfsusize); ++i)
      mat.accumulate(lfsv_co2,i,lfsu_co2,j,
          dy/2.*(dDa11_dco2j*(co2[i]-co2[pairx[i]])/dx+dDa12_dco2j*co2dery*(i-pairx[i])  )
         +dx/2.*(dDa22_dco2j*(co2[i]-co2[pairy[i]])/dy+dDa12_dco2j*co2derx*(i-pairy[i])/2) );
  }
  // co2 dispersion by co2: Ma in pa_Da_Ma
  for (int j=0; j<int(lfsusize); ++j)
  {
    RF dpM_dco2j = avgpap/size*p_h.dMa_dco2(co2[j]);
    for (int i=0; i<int(lfsusize); ++i)
      mat.accumulate(lfsv_co2,i,lfsu_co2,j,
            dy/2.*(Da11_*dpM_dco2j*(co2[i]-co2[pairx[i]])/dx+Da12_*dpM_dco2j*co2dery*(i-pairx[i])  )
           +dx/2.*(Da22_*dpM_dco2j*(co2[i]-co2[pairy[i]])/dy+Da12_*dpM_dco2j*co2derx*(i-pairy[i])/2) );
  }
  // co2 dispersion by phi: average permeability
  for (int j=0; j<int(lfsusize); ++j)
  {
    RF dKac_dphi = param.dKac_dphi(Sa[j],phi[j],co2[j]);
    RF dphi = avgKac/(size*Kac[j]*Kac[j])*dKac_dphi*(vasqrt==1e-5 ? 2. : 1.);
    RF dDa11_dphij = Da11*dphi;
    RF dDa12_dphij = Da12*dphi;
    RF dDa22_dphij = Da22*dphi;
    for (int i=0; i<int(lfsusize); ++i)
      mat.accumulate(lfsv_co2,i,lfsu_phi,j,
             dy/2.*(dDa11_dphij*(co2[i]-co2[pairx[i]])/dx+dDa12_dphij*co2dery*(i-pairx[i])  ) // (i-pairx[i]) works as a unit normal vector
            +dx/2.*(dDa22_dphij*(co2[i]-co2[pairy[i]])/dy+dDa12_dphij*co2derx*(i-pairy[i])/2) );
  }
// */