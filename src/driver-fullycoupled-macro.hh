#define RASPEN false

template<typename ES, typename FEM, typename GV>
void driver (const ES& es, const FEM& fem, Dune::ParameterTree& ptree, const GV& gv)
{
  // dimension and important types
  // const int dim = ES::dimension;
  typedef double RF;                   // type for computations

  // get parameters
  const int ver = ptree.get<int>("parallel.verbose");
  int print_proc = ptree.get<int>("parallel.rank");
  const int max_newton = ptree.get<int>("parallel.max_newton");
  const int itNum = ptree.get<int>("operatorsplitting.iter",5);
  constexpr double dtup = 1.5; // 1.25 default till 1.10.2020
  constexpr double dtdown = 0.75; // 0.75 default
  #if RASPEN
  const int lineSearch = ptree.get<int>("parallel.lineSearch");
  const double red_outer = ptree.get<double>("parallel.red_outer");
  const double min_lin_red = ptree.get<double>("parallel.min_lin_red",1e-3);
  const double abs_limit = ptree.get<double>("parallel.abs_limit");
  const unsigned int restart = ptree.get<int>("parallel.restart");
  const double red_inner1 = ptree.get<double>("parallel.red_inner1");
  const double red_inner2 = ptree.get<double>("parallel.red_inner2");
  #endif
  // const double vtkgap = ptree.get<double>("vtk.vtkgap");

  // Make grid function space
  typedef Dune::PDELab::OverlappingConformingDirichletConstraints COND; // parallel
  // typedef Dune::PDELab::ConformingDirichletConstraints COND; // sequential, or nonoverlapping parallel with ghosts
  // typedef Dune::PDELab::NoConstraints CON;
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<ES,FEM,COND,VBE> GFS;
  GFS gfs(es,fem);
  gfs.name("Vhd");
  int verbose=0;
  if (print_proc == -1)
    print_proc = gfs.gridView().comm().size()-1;
  if (gfs.gridView().comm().rank()==print_proc) verbose=ver;
  std::cout << "verbosity on rank " << gfs.gridView().comm().rank() << " is " << verbose << std::endl;

  using VBES = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed>;
  // last argument is either "fixed" or "none" to give istl information about structure
  // tag fixed stands for fixed size of blocks in the matrix
  // using OrderingTag = Dune::PDELab::LexicographicOrderingTag;
  using OrderingTag = Dune::PDELab::EntityBlockedOrderingTag;
  // lists coefficients in order, LexicographicOrderingTag has each vector space compact,
  // EntityBlockedOrderingTag puts DoFs from one entity together (order<=2, unless number of elements per entity may vary)
  using GFSS = Dune::PDELab::PowerGridFunctionSpace<GFS,6,VBES,OrderingTag>;
  // GFSS spaces may differ in constraints
  GFSS gfss(gfs);
  // gfss names for VTK output
  using namespace Dune::TypeTree::Indices;
  gfss.child(_0).name("pw");
  gfss.child(_1).name("Sa");
  gfss.child(_2).name("Ca");
  gfss.child(_3).name("CO_2");
  gfss.child(_4).name("phi");
  gfss.child(_5).name("CaStored");

  // Coefficient vector
  using ZS = Dune::PDELab::Backend::Vector<GFSS, RF>;
  ZS zs(gfss);

  // Parameter class
  RF time = 0.0;
  using ParamFlow = Parametre<RF>;
  ParamFlow problemd( ptree.get("material.aalfa",      (RF) 1.89e-2   )
                    , ptree.get("material.thetas",     (RF) 3.8e-1    )
                    , ptree.get("material.n",          (RF) 2.81      )
                    , ptree.get("material.Ks",         (RF) 2.4e-4    )
                    , ptree.get("grid.structured.LY",  (RF) 1.e1      )
                    , ptree.get("grid.structured.LX",  (RF) 5.        )
                    , ptree.get("material.grav",       (RF)-9.81      )
                    , ptree.get("material.viscosity_w",(RF) 1.002e-3  )
                    , ptree.get("material.viscosity_a",(RF) 1.8369e-5 )
                    , ptree.get("material.rho_w",      (RF) 9.98205e2 )
                    , ptree.get("material.Swr",        (RF) 0.        )
                    , ptree.get("material.Sar",        (RF) 0.        )
                    , ptree.get("problem.etastab",     (RF) 1.e2      )
                    , ptree.get("material.air.R",      (RF) 8.3144598 )
                    , ptree.get("material.air.T",      (RF) 2.9315e2  )
                    , ptree.get("material.air.Ma",     (RF) 2.89626e-2)
                    , ptree.get("material.air.Mc",     (RF) 4.401e-2  )
                    , ptree.get("material.air.p0",     (RF) 1.01325e5 )
                    , ptree.get("regularization.Ka_correction", (RF) 50.)
                    , ptree.get("regularization.lscheme_co2", (RF) 10.)
                      );
  problemd.setTime(time);
  // parameter class
  Parametre_chem<RF, ParamFlow> param_chem( problemd
                                          , ptree.get("heat.alfa_l",               (RF) 1.      )
                                          , ptree.get("chem.ini_Ca",               (RF) 2.2e1   )
                                          , ptree.get("chem.ini_co2",              (RF) 0.      )
                                          , ptree.get("chem.difCa",                (RF) 2.3e-13 )
                                          , ptree.get("chem.difCae",               (RF) 9.95    )
                                          , ptree.get("chem.difco2",               (RF) 8.e-7   )
                                          , ptree.get("chem.airco2",               (RF) 4.e-4   )
                                          , ptree.get("chem.carbonation.carba",    (RF) 1.5     )
                                          , ptree.get("chem.carbonation.carbb",    (RF) 1.5     )
                                          , ptree.get("chem.carbonation.carbtheta",(RF) 1.23e-1 )
                                          , ptree.get("chem.carbonation.VCaCO_3",  (RF) 3.693e-5)
                                          , ptree.get("material.viscosity_c",      (RF) 1.48e-5 )
                                            );
  // if negative, sets treshold between setting_to_zero and failed_timestep
  RF tresholdCa  = ptree.get("regularization.tresholdCa" ,(RF) -1e-6);
  RF tresholdco2 = ptree.get("regularization.tresholdco2",(RF) -1e-4);
  // initial condition
  auto glambdas = [&](const auto& e, const auto& x)
  {
    Dune::FieldVector<RF,6> rf;
    rf[0] = problemd.g(e,x);
    rf[1] = problemd.ga(e,x);
    rf[2] = param_chem.g_Ca(e,x);
    rf[3] = param_chem.g_co2(e,x);
    rf[4] = param_chem.g_porosity(e,x);
    rf[5] = param_chem.g_CaStored(e,x);
    return rf;
  };
  auto gs = Dune::PDELab::
    makeInstationaryGridFunctionFromCallable(es,glambdas,problemd);
  Dune::PDELab::interpolate(gs,gfss,zs);

  // boundary conditions - bool, true==dirichlet
  auto b0lambda = [&](const auto& i, const auto& x){return problemd.b(i,x);};
  auto bf0 = Dune::PDELab::makeBoundaryConditionFromCallable(es,b0lambda);
  auto b1lambda = [&](const auto& i, const auto& x){return problemd.ba(i,x);};
  auto bf1 = Dune::PDELab::makeBoundaryConditionFromCallable(es,b1lambda);
  auto b2lambda = [&](const auto& i, const auto& x){return param_chem.bCa(i,x);};
  auto bf2 = Dune::PDELab::makeBoundaryConditionFromCallable(es,b2lambda);
  auto b3lambda = [&](const auto& i, const auto& x){return param_chem.bco2(i,x);};
  auto bf3 = Dune::PDELab::makeBoundaryConditionFromCallable(es,b3lambda);
  auto b4lambda = [&](const auto& i, const auto& x){return false;};
  auto bf4 = Dune::PDELab::makeBoundaryConditionFromCallable(es,b4lambda);
  auto b5lambda = [&](const auto& i, const auto& x){return false;};
  auto bf5 = Dune::PDELab::makeBoundaryConditionFromCallable(es,b5lambda);

  using BS = Dune::PDELab
    ::CompositeConstraintsParameters<decltype(bf0),decltype(bf1),decltype(bf2),decltype(bf3),decltype(bf4),decltype(bf5)>;
  BS bs(bf0,bf1,bf2,bf3,bf4,bf5);

  // Assemble constraints
  typedef typename GFSS::template ConstraintsContainer<RF>::Type CS;
  CS cs;
  Dune::PDELab::constraints(bs,gfss,cs); // assemble constraints
  if (verbose>=1)
    std::cout << "constrained dofs=" << cs.size() << " of "
              << gfss.globalSize() << std::endl;

  // initialize simulation time, the coefficient vector
  ZS zsnew(zs);  // result from apply()
  ZS zsold(zs); // keeps value of the current time step, to enable better predictor in operator splitting

  // Make instationary grid operator
  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
  MBE mbe((int)20);
  typedef LOP_spatial_fullycoupled<Parametre_chem<RF,ParamFlow>,GFSS,ZS> LOPS;
  LOPS lops(param_chem,gfss,zsold);
  typedef LOP_time_fullycoupled<Parametre_chem<RF,ParamFlow>> TLOPS;
  TLOPS tlops(param_chem);
  #if RASPEN
  typedef Dune::PDELab::Raspen::GridOperator<GFSS,GFSS,LOPS,MBE,
                                     RF,RF,RF,CS,CS> GOS0;
  typedef Dune::PDELab::Raspen::GridOperator<GFSS,GFSS,TLOPS,MBE,
                                     RF,RF,RF,CS,CS> GOS1;
  typedef Dune::PDELab::Raspen::OneStepGridOperator<GOS0,GOS1> IGOS;
  #else
  typedef Dune::PDELab::GridOperator<GFSS,GFSS,LOPS,MBE,
                                     RF,RF,RF,CS,CS> GOS0;
  typedef Dune::PDELab::GridOperator<GFSS,GFSS,TLOPS,MBE,
                                     RF,RF,RF,CS,CS> GOS1;
  typedef Dune::PDELab::OneStepGridOperator<GOS0,GOS1> IGOS;
  #endif
  GOS0 gos0(gfss,cs,gfss,cs,lops,mbe);
  GOS1 gos1(gfss,cs,gfss,cs,tlops,mbe);
  IGOS igos(gos0,gos1);
  // -----RASPEN-PART---------------------------------------------------------------------------------------------------------------------------
  #if RASPEN
  ZS v(gfss);                 // store internal values
  ZS part_unity(gfss,1.0);    // Initialize vector to 1

  // Set to zero on subdomain boundaries
  Dune::PDELab::set_constrained_dofs(cs,0.0,part_unity);

  Dune::PDELab::AddDataHandle<GFSS,ZS> partitionunityhandle(gfss,part_unity);
  gfss.gridView().communicate(partitionunityhandle,Dune::All_All_Interface,           Dune::ForwardCommunication);
  // gfss.gridView().communicate(partitionunityhandle,Dune::InteriorBorder_All_Interface,Dune::ForwardCommunication);

  Dune::PDELab::set_constrained_dofs(cs,0.0,part_unity);
  // Zero on subdomain boundary (Need that a 2nd time due to add comm before!)

  // Divide 1/x
  // for (auto iter = part_unity.begin(); iter != part_unity.end(); iter++) { if (*iter > 0) *iter = 1.0 / *iter; }
  //same using range for:
  for (auto& p_u : part_unity)
  {
    if (p_u>0.) p_u=1.0/p_u;
  }

  // Fill the coefficient vector
  Dune::PDELab::interpolate(gs,gfss,v);
  // my case Dune::PDELab::interpolate(gs,gfss,zs);

  // make vector consistent NEW IN PARALLEL
  Dune::PDELab::ISTL::ParallelHelper<GFSS> helper(gfss);
  helper.maskForeignDOFs(v);
  Dune::PDELab::AddDataHandle<GFSS,ZS> addvdh(gfss,v);
  if (gfss.gridView().comm().size()>1)
  {
    gfss.gridView().communicate(addvdh,Dune::InteriorBorder_All_Interface,Dune::ForwardCommunication);
  }

  for (const auto& col : cs)
  {
    v[col.first] = 0.0;
  }
  v -= zs;    //store internal values

  typedef Dune::PDELab::ISTLBackend_SEQ_UMFPack LSS; // RASPEN uses sequential solver for single rank
  // typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU LSS;
  LSS lss(100,0); // SEQ_SuperLU, SEQ_UMFPack
  typedef Dune::PDELab::Raspen::RestrainedNewton<IGOS,LSS,ZS> PDESOLVER; // renamed RASPEN
  // typedef Dune::PDELab::Raspen::TwoLevelRASPEN<IGOS,LSS,ZS> PDESOLVER; // renamed RASPEN
  PDESOLVER pdesolver(igos,zs,v,part_unity,lss); // RASPEN
  pdesolver.setReassembleThreshold(0.0);
  pdesolver.setReduction(red_outer);
  pdesolver.setMinLinearReduction(min_lin_red);
  pdesolver.setMaxIterations(max_newton);
  pdesolver.setLineSearchMaxIterations(lineSearch);
  pdesolver.setAbsoluteLimit(abs_limit);
  pdesolver.setVerbosityLevel(verbose, print_proc); // RASPEN, classic has inly first argument
  pdesolver.setReduction_inner1(red_inner1);  // RASPEN only
  pdesolver.setReduction_inner2(red_inner2);  // RASPEN only
  pdesolver.setRestart(restart);              // RASPEN only
  // pdesolver.setComponents(); // two-level
  pdesolver.setParameters(ptree.sub("fullycoupled.LineSearch"));
  const bool useMaxNorm = ptree.get<bool>("fullycoupled.UseMaxNorm");
  pdesolver.setUseMaxNorm(useMaxNorm);

  // write-to-file part

          //print log file
          std::string name = "result from processor_";
          name += std::to_string(gfs.gridView().comm().rank());
          name += ".txt";
          static std::ofstream tfile;
          tfile.open (name);
  #else
  // typedef Dune::PDELab::ISTLBackend_OVLP_BCGS_SSORk<GFSS,CS> LSS;
  typedef Dune::PDELab::ISTLBackend_OVLP_BCGS_SSORk_MaxNorm<GFSS,CS> LSS;
  LSS lss(gfss,cs,30,5,verbose); // for overlapping BCGS_SSORk
  typedef Dune::PDELab::NewtonMethod<IGOS,LSS> PDESOLVER;
  PDESOLVER pdesolver(igos,lss); // classic
  Dune::ParameterTree newtonParam;
  newtonParam = ptree.sub("fullycoupled");
  newtonParam["VerbosityLevel"] = std::to_string(verbose);
  pdesolver.setParameters(newtonParam);
  if (verbose > 0)
    pdesolver.printParameters("Fully coupled system");
  #endif

  // -END-RASPEN-PART---------------------------------------------------------------------------------------------------------------------------

  // select and prepare time-stepping scheme
  Dune::PDELab::OneStepThetaParameter<RF> method1(1.0);
  Dune::PDELab::TimeSteppingParameterInterface<RF>* pmethod=&method1;
  #if RASPEN
  typedef Dune::PDELab::Raspen::OneStepMethod<GFSS,RF,IGOS,PDESOLVER,ZS,ZS> OSMS; // RASPEN counterpart has extra GFSS template argument
  OSMS  osms(*pmethod,igos,pdesolver,print_proc);
  #else
  typedef Dune::PDELab::OneStepMethod<RF,IGOS,PDESOLVER,ZS,ZS> OSMS; // classic
  OSMS  osms(*pmethod,igos,pdesolver);
  #endif

  // OneStepMethod always uses verbosity 0 on rank>0
  osms.setVerbosityLevel(ver);

  // prepare VTK writer and write first file
  int subsampling=ptree.get("output.subsampling",(int)0);
  using VTKWRITER = Dune::SubsamplingVTKWriter<GV>;
  VTKWRITER vtkwriter(gv,Dune::refinementIntervals(subsampling));
  std::string filename= ptree.get("output.filenamefullycoupled","fullycoupled");
  struct stat stf;
  if( stat( filename.c_str(), &stf ) != 0 )
  {
    int statf = 0;
    statf = mkdir(filename.c_str(),S_IRWXU|S_IRWXG|S_IRWXO);
    if( statf != 0 && statf != -1)
      std::cout << "Error: Cannot create directory "
                << filename << std::endl;
  }
  using VTKSEQUENCEWRITER = Dune::VTKSequenceWriter<GV>;
  VTKSEQUENCEWRITER vtkSequenceWriter(
    std::make_shared<VTKWRITER>(vtkwriter),filename,filename,"");
  // add data field for all components of the space to the VTK writer
  Dune::PDELab::addSolutionToVTKWriter(vtkSequenceWriter,gfss,zs);
  vtkSequenceWriter.write(0.0,Dune::VTK::appendedraw);
  std::vector<RF> vtktimes;

  // time loop --------------------------------------------------------------------
  RF T = ptree.get("problem.T",(RF)1.0);
  uint screenshots = ptree.get("vtk.screenshots",uint(30));
  screenshots /= 2;
  // half of screenshots is spread linearly -covers mostly end
  RF linq = T/RF(screenshots);
  // half of screenshots is spread geometrically -covers beginning, maybe too much
  RF geoq = std::pow(T,1./RF(screenshots));
  RF qq = geoq;
  for (uint i=1; i<screenshots; ++i)
  {
    vtktimes.push_back(linq*RF(i));
    vtktimes.push_back(qq);
    qq *= geoq;
  }
  vtktimes.push_back(T);
  std::sort(vtktimes.begin(),vtktimes.end(),std::greater<RF>());
  if (verbose >= 1)
  {
    for (auto v : vtktimes)
      std::cout << v << ", ";
    std::cout << std::endl;
  }
  RF nextvtk = vtktimes[vtktimes.size()-1];
  vtktimes.pop_back();
  unsigned int pocetiteracii{0};
  unsigned int maxiter{0};
  std::vector<unsigned int> uspesneiter(max_newton+1);
  problemd.setTime(time);
  problemd.setOtherTime(time);
  problemd.setTimeStep(ptree.get("fem.dt",(RF)0.1));
  // RF lastvtk = 0.; // the time of last created vtk file, used to avoid creating too many frames if timestep is small
  std::vector<double> timestephistory{};

  mojefunkcie::writer timestephistoryfile(filename+".txt");
  std::map<std::string,int> failstats;
  failstats.insert(std::make_pair("negativeConcentration",0));
  failstats.insert(std::make_pair("solverException",0));
  failstats.insert(std::make_pair("unknowntype",0));

  int iter_number{0};
  int should_throw{0};
  // calculate system, update zsold, and check for negative Ca, co2 concentrations
  auto applysystem = [&] (RF dt)
  {
    #if RASPEN
    osms.apply(time,dt,zs,gs,zsnew,v,part_unity,gfss); // RASPEN has extra parameters
    #else
    osms.apply(time,dt,zs,gs,zsnew); // classic
    #endif
    should_throw = mojefunkcie::check_blockvector_for_negatives(zsnew,tresholdCa,2,"Ca concentration");
    should_throw += mojefunkcie::check_blockvector_for_negatives(zsnew,tresholdco2,3,"co2 concentration");
    should_throw = gv.comm().max(should_throw);
    if (should_throw != 0){
      ++failstats["negativeConcentration"];
      DUNE_THROW(NegativeConcentrationError,"Ca or co2 concentration too negative");
    }
  };
  // updates maxiter for time step size and store iteration numbers for statistics
  auto store_iteration_number = [&] () // -> void
  {
    pocetiteracii=pdesolver.result().iterations;
    uspesneiter.at(pocetiteracii)+=1;
    maxiter = std::max(maxiter,pocetiteracii);
  };

  bool multirankdebug = false;
  while (time<T-1e-8)
  {
    try
    {
      // if (time>4.58e+4)
      if (multirankdebug)
      {
        multirankdebug = false;
        // set sleep cycle for debugger, REMOVE IF RUNNING OUTSIDE DEBUGGER
        int ifl = 0;
        char hostname[256];
        gethostname(hostname, sizeof(hostname));
        std::cout << "Approaching the critical time step! ";
        std::cout << "Using infinite sleep cycle for attaching debugger!" << std::endl;
        printf("PID %d on %s ready for attach\n", getpid(), hostname);
        fflush(stdout);
        while (0 == ifl)
              sleep(5);
      }
      maxiter = 0;
      zsold = zs;
      zsnew = zs;
      if (time+2.*problemd.getTimeStep() > nextvtk)
      {
        if (time+problemd.getTimeStep() > nextvtk)
          problemd.setTimeStep(nextvtk-time);
        else
          problemd.setTimeStep((nextvtk-time)/2.);
      }
      applysystem(problemd.getTimeStep());
      zs=zsnew;
      store_iteration_number();
      time+=problemd.getTimeStep();
      problemd.setOtherTime(time);

      // write solution at vtktimes
      // if (time>lastvtk*vtkgap)
      if (time > nextvtk-1e-3 && time < nextvtk+1e-3)
      {
        vtkSequenceWriter.write(time,Dune::VTK::appendedraw);
        if (!vtktimes.empty())
        {
          nextvtk = vtktimes[vtktimes.size()-1];
          vtktimes.pop_back();
        }
        else
          nextvtk = T*10.; // more than final time T
      }
      if (verbose >= 1)
      {
        timestephistoryfile.text << problemd.getTimeStep() << std::endl;
      }
      if ( (maxiter<4) & (iter_number<itNum-1) )
      {
        problemd.adjustTimeStep(dtup);
      }
      else
        if ((maxiter>(max_newton*7.)/10.) && problemd.getTimeStep()>1e-4)
        {
          problemd.adjustTimeStep(dtdown);
        }
    } // end try
    catch(const std::string& e)
    {          // NaN or Inf occured, reduce time step
      ++failstats["unknowntype"];
      if (verbose>=1)
      {
        std::cout << e << std::endl;
      }
      if (problemd.getTimeStep()<1e-4)
      {
        if (verbose>=1)
        {
          std::cout << "too little time step" << std::endl;
        }
        vtkSequenceWriter.write(time+T,Dune::VTK::appendedraw);
        throw;
      }
      else
      {
        zs = zsold;
        // vtkSequenceWriter.write(time+ problemd.getTimeStep(),Dune::VTK::appendedraw);
        problemd.adjustTimeStep(dtdown);
        if (verbose>=1)
        {
          std::cout << "Reducing TimeStep size to " << problemd.getTimeStep() << std::endl;
        }
      }
    }
    catch(const Dune::Exception& e)
    {
      ++failstats["solverException"];
      if (verbose>=1)
      {
        std::cout << e.what() << std::endl;
      }
      if (problemd.getTimeStep()<1e-4)
      {
        if (verbose>=1)
        {
          std::cout << "too little time step" << std::endl;
        }
        vtkSequenceWriter.write(time+T,Dune::VTK::appendedraw);
        throw;
      }
      else
      {
        // vtkSequenceWriter.write(time+ problemd.getTimeStep(),Dune::VTK::appendedraw);
        problemd.adjustTimeStep(dtdown);
        zs = zsold;
        if (verbose>=1)
        {
          std::cout << "Reducing TimeStep size to " << problemd.getTimeStep() << std::endl;
        }
      }
    }
    catch(const std::exception& e)
    {
      ++failstats["unknownType"];
      if (verbose>=1)
      {
        std::cout << e.what() << std::endl;
      }
      if (problemd.getTimeStep()<1e-4)
      {
        if (verbose>=1)
        {
          std::cout << "too little time step" << std::endl;
        }
        vtkSequenceWriter.write(time+T,Dune::VTK::appendedraw);
        throw;
      }
      else
      {
        // vtkSequenceWriter.write(time+ problemd.getTimeStep(),Dune::VTK::appendedraw);
        problemd.adjustTimeStep(dtdown);
        zs = zsold;
        if (verbose>=1)
        {
          std::cout << "Reducing TimeStep size to " << problemd.getTimeStep() << std::endl;
        }
      }
    }
    catch(...) // in case of unexpected exception terminate
    {
      ++failstats["unknowntype"];
      throw;
    }
  } // end while
  if (verbose>=1)
  {
    std::cout << "Successful timesteps needed ";
    for(auto i : uspesneiter)
    {
      std::cout << i << ", ";
    }
    std::cout << "iterations." << std::endl;
  }
  if (verbose>=1)
  {
    // failstats[solverException] contains solver exceptions, and negativeConcentration,
    // because all are derived from Dune::Exception
    int fsse = failstats["solverException"] - failstats["negativeConcentration"];
    std::cout << "Failures occured by" << std::endl;
    std::cout << "negative concentration: " << failstats["negativeConcentration"] << std::endl;
    std::cout << "solver exception      : " << fsse << std::endl;
    std::cout << "other                 : " << failstats["unknowntype"] << "." << std::endl;
  }
} // end driver
