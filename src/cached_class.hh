template<typename GFS, typename Data, typename RF>
class CachedParam
{
  // this class is only a minimalistic interface to automatically load data
  // theoretically we could omit the third template parameter RF and get it from LFS,
  // but that differs if we use PowerGFS or normal GFS

  typedef Dune::PDELab::LocalFunctionSpace<GFS> LFS;
  typedef Dune::PDELab::LFSIndexCache<LFS> LFSCache;
  std::shared_ptr<Data> data;
  std::shared_ptr<const GFS> pgfs;
  mutable LFS lfs;            // mutable is necessary, we use it inside alpha-vol (and similar)
  mutable LFSCache lfs_cache; // =in a const function -> methods inside must be const and then
                              // changing requires mutability
public:

  CachedParam(const GFS& gfs_, Data& data_)
  // stores data_, and must know its structure gfs_
  : data(stackobject_to_shared_ptr(data_)),
    pgfs(stackobject_to_shared_ptr(gfs_)),
    lfs(pgfs),
    lfs_cache(lfs)
  {}

  template<typename Entity>
  inline std::vector<RF> read(const Entity& ent) const
  // expects eg.entity() as input, needs to know which local data to read
  {
    typename Data::template LocalView<LFSCache> c_view(*data);
    lfs.bind(ent);
    lfs_cache.update();
    std::vector<RF> cl(lfs.size());
    c_view.bind(lfs_cache);
    c_view.read(cl);
    c_view.unbind();
    return cl;
  }

  template<typename Entity>
  std::size_t size(const Entity& ent) const
  {
    lfs.bind(ent);
    lfs_cache.update();
    return lfs.size();
  }

  template<typename Entity>
  inline void write(const Entity& ent, const std::vector<RF>& data_) const
  // expects eg.entity() as input, needs to know which local data to write
  {
    typename Data::template LocalView<LFSCache> c_view(*data);
    lfs.bind(ent);
    lfs_cache.update();
    c_view.bind(lfs_cache);
    c_view.write(data_);
    c_view.unbind();
  }

};