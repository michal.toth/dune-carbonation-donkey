      constexpr std::size_t lfsusize = 4; // this file is inside if(lfsusize==4)
                                         // overwriting enables using it as a template argument

      const auto& a0 = eg.geometry().corner(0);
      const auto& c3 = eg.geometry().corner(3);
      const DF dx = c3[0]-a0[0];
      const DF dy = c3[1]-a0[1];
      assert(dx>0 && dy>0); // assumption on the rectangle orientation
      constexpr std::array<int,lfsusize> pairx{1,0,3,2};
      constexpr std::array<int,lfsusize> pairy{2,3,0,1};

      std::array<RF,lfsusize> Kw, Ka, dKw_dphi, dKa_dphi; // better have vector of coefficients than re-compute pow()
      for (size_t i=0; i<lfsusize; ++i)
      {
        Kw[i] = param.K(phi[i]); // intrinsic permeability
        Ka[i] = Kw[i]*param.getKa_correction(); // air has different intrinsic perm. because water is chemically bound
        dKw_dphi[i] = param.dK_dphi(phi[i]);
        dKa_dphi[i] = dKw_dphi[i]*param.getKa_correction();

      }
      auto lvw = [&] (int j) {return param.kw(Sa[j])/param.getvisc_w();}; // relative permeability, and viscosity. Without density!
      auto lva = [&] (int j) {return param.ka(Sa[j])/param.visc_mix(co2[j]);};
      auto dlva_dco2 = [&] (int j) {
        RF denom = param.visc_mix(co2[j]);
        denom *= denom;
        return param.ka(Sa[j])*(-1.)/denom*param.dvisc_mix_dco2(co2[j]);
      };

      // advection part
      if (osType%2==1)
      {
        using mojefunkcie::dh2avg_d1;
        using mojefunkcie::dh2avg_d2;
        for (int i=0; i<int(lfsusize); ++i)
        {
          if (i<pairx[i])
          {
            { // Ca flow
              RF gradwx = (pw[pairx[i]]-pw[i])*mega/dx;
              RF Kwx = h2avg(Kw[i],Kw[pairx[i]]); // intrinsic permeability, harmonic averaged
              size_t uwx = gradwx > 0. ? pairx[i] : i; // index of upwind source
              RF kwx = lvw(uwx); // upwinded relative permeability, and viscosity
              RF dKwx_dphii = dh2avg_d1(Kw[i],Kw[pairx[i]])*dKw_dphi[i];
              RF dKwx_dphix = dh2avg_d2(Kw[i],Kw[pairx[i]])*dKw_dphi[pairx[i]];
              // RF advCa = -dy/2.*Kwx*kwx*gradwx**Ca[uwx]; // Ca advection, upwinded
              RF dadvCa_dCau = -dy/2.*Kwx*kwx*gradwx;
              RF dadvCa_dphii = -dy/2.*dKwx_dphii*kwx*gradwx*Ca[uwx];
              RF dadvCa_dphix = -dy/2.*dKwx_dphix*kwx*gradwx*Ca[uwx];
              mat.accumulate(lfsv_Ca,      i ,lfsv_Ca ,    uwx , dadvCa_dCau);
              mat.accumulate(lfsv_Ca,pairx[i],lfsv_Ca ,    uwx ,-dadvCa_dCau);
              mat.accumulate(lfsv_Ca,      i ,lfsu_phi,      i , dadvCa_dphii);
              mat.accumulate(lfsv_Ca,pairx[i],lfsu_phi,      i ,-dadvCa_dphii);
              mat.accumulate(lfsv_Ca,      i ,lfsu_phi,pairx[i], dadvCa_dphix);
              mat.accumulate(lfsv_Ca,pairx[i],lfsu_phi,pairx[i],-dadvCa_dphix);
            }
            { // CO2 flow
              RF gradax = (pa[pairx[i]]-pa[i])*mega/dx;
              RF Kax = h2avg(Ka[i],Ka[pairx[i]]);
              size_t uax = gradax > 0. ? pairx[i] : i;
              RF kax = lva(uax);
              // RF advCO2 = -dy/2.*Kax*kax*gradax*(pa[uax]+param.getpa0())*mega*co2[uax];
              RF dKax_dphii = dh2avg_d1(Ka[i],Ka[pairx[i]])*dKa_dphi[i];
              RF dKax_dphix = dh2avg_d2(Ka[i],Ka[pairx[i]])*dKa_dphi[pairx[i]];

              RF dadvCO2_dco2u = -dy/2.*Kax*kax*gradax*(pa[uax]+param.getpa0())*mega
                                 -dy/2.*Kax*dlva_dco2(uax)*gradax*(pa[uax]+param.getpa0())*mega*co2[uax];
              RF dadvCO2_dphii = -dy/2.*dKax_dphii*kax*gradax*(pa[uax]+param.getpa0())*mega*co2[uax];
              RF dadvCO2_dphix = -dy/2.*dKax_dphix*kax*gradax*(pa[uax]+param.getpa0())*mega*co2[uax];
              mat.accumulate(lfsv_co2,      i ,lfsu_co2,    uax , dadvCO2_dco2u);
              mat.accumulate(lfsv_co2,pairx[i],lfsu_co2,    uax ,-dadvCO2_dco2u);
              mat.accumulate(lfsv_co2,      i ,lfsu_phi,      i , dadvCO2_dphii);
              mat.accumulate(lfsv_co2,pairx[i],lfsu_phi,      i ,-dadvCO2_dphii);
              mat.accumulate(lfsv_co2,      i ,lfsu_phi,pairx[i], dadvCO2_dphix);
              mat.accumulate(lfsv_co2,pairx[i],lfsu_phi,pairx[i],-dadvCO2_dphix);
            }
          }
          if (i<pairy[i])
          {
            { // Ca flow
              RF gradwy = (pw[pairy[i]]-pw[i])*mega/dy+param.getgrav()*param.getrho_w(); // direction of gravity guaranteed from i<pairy[i]
              RF Kwy = h2avg(Kw[i],Kw[pairy[i]]); // intrinsic permeability, harmonic averaged
              size_t uwy = gradwy > 0. ? pairy[i] : i; // index of upwind source
              RF kwy = lvw(uwy); // upwinded relative permeability, and viscosity
              RF dKwy_dphii = dh2avg_d1(Kw[i],Kw[pairy[i]])*dKw_dphi[i];
              RF dKwy_dphiy = dh2avg_d2(Kw[i],Kw[pairy[i]])*dKw_dphi[pairy[i]];
              // RF advCa = -dy/2.*Kwy*kwy*gradwy*Ca[uwy]; // Ca advection, upwinded
              RF dadvCa_dCau  = -dx/2.*Kwy*kwy*gradwy;
              RF dadvCa_dphii = -dx/2.*dKwy_dphii*kwy*gradwy*Ca[uwy];
              RF dadvCa_dphiy = -dx/2.*dKwy_dphiy*kwy*gradwy*Ca[uwy];
              mat.accumulate(lfsv_Ca,      i ,lfsu_Ca ,    uwy , dadvCa_dCau);
              mat.accumulate(lfsv_Ca,pairy[i],lfsu_Ca ,    uwy ,-dadvCa_dCau);
              mat.accumulate(lfsv_Ca,      i ,lfsu_phi,      i , dadvCa_dphii);
              mat.accumulate(lfsv_Ca,pairy[i],lfsu_phi,      i ,-dadvCa_dphii);
              mat.accumulate(lfsv_Ca,      i ,lfsu_phi,pairy[i], dadvCa_dphiy);
              mat.accumulate(lfsv_Ca,pairy[i],lfsu_phi,pairy[i],-dadvCa_dphiy);
            }
            { // CO2 flow
              RF rho_ay = (p_h.getrho_a(pa[i],co2[i])+p_h.getrho_a(pa[pairy[i]],co2[pairy[i]]))/2.; // arithmetic average for density
              RF graday = (pa[pairy[i]]-pa[i])*mega/dy+param.getgrav()*rho_ay;
              RF Kay = h2avg(Ka[i],Ka[pairy[i]]);
              RF dKay_dphii = dh2avg_d1(Ka[i],Ka[pairy[i]])*dKa_dphi[i];
              RF dKay_dphiy = dh2avg_d2(Ka[i],Ka[pairy[i]])*dKa_dphi[pairy[i]];
              size_t uay = graday > 0. ? pairy[i] : i;
              RF kay = lva(uay);
              // RF advCO2 = -dx/2.*Kay*kay*graday*(pa[uay]+param.getpa0())*mega*co2[uay];
              RF dadvCO2_dco2i = -dx/2.*Kay*kay*param.getgrav()*param.drho_a_dco2(pa[i],co2[i])/2.              *(pa[uay]+param.getpa0())*mega*co2[uay];
              RF dadvCO2_dco2y = -dx/2.*Kay*kay*param.getgrav()*param.drho_a_dco2(pa[pairy[i]],co2[pairy[i]])/2.*(pa[uay]+param.getpa0())*mega*co2[uay];
              RF dadvCO2_dco2u = -dx/2.*Kay*kay*graday*           (pa[uay]+param.getpa0())*mega
                                 -dx/2.*Kay*dlva_dco2(uay)*graday*(pa[uay]+param.getpa0())*mega*co2[uay];
              RF dadvCO2_dphii = -dx/2.*dKay_dphii*kay    *graday*(pa[uay]+param.getpa0())*mega*co2[uay];
              RF dadvCO2_dphiy = -dx/2.*dKay_dphiy*kay    *graday*(pa[uay]+param.getpa0())*mega*co2[uay];
              mat.accumulate(lfsv_co2,      i ,lfsu_co2,      i , dadvCO2_dco2i);
              mat.accumulate(lfsv_co2,pairy[i],lfsu_co2,      i ,-dadvCO2_dco2i);
              mat.accumulate(lfsv_co2,      i ,lfsu_co2,pairy[i], dadvCO2_dco2y);
              mat.accumulate(lfsv_co2,pairy[i],lfsu_co2,pairy[i],-dadvCO2_dco2y);
              mat.accumulate(lfsv_co2,      i ,lfsu_co2,    uay , dadvCO2_dco2u);
              mat.accumulate(lfsv_co2,pairy[i],lfsu_co2,    uay ,-dadvCO2_dco2u);
              mat.accumulate(lfsv_co2,      i ,lfsu_phi,      i , dadvCO2_dphii);
              mat.accumulate(lfsv_co2,pairy[i],lfsu_phi,      i ,-dadvCO2_dphii);
              mat.accumulate(lfsv_co2,      i ,lfsu_phi,pairy[i], dadvCO2_dphiy);
              mat.accumulate(lfsv_co2,pairy[i],lfsu_phi,pairy[i],-dadvCO2_dphiy);
            }
          }
        } // end for i=1..lfsusize
      } // end if (osType%2==1)
      // Ca and CO_2 diffusion term -------------------------------------------------------------------------------------------------------------
      if((osType%4)/2==1)
      {
        // Ca and CO_2 diffusion
        using mojefunkcie::dh2avg_d1;
        using mojefunkcie::dh2avg_d2;
        auto Dw = [&] (int j) {return p_h.diffCa(phi[j],Sa[j]);};
        auto Da = [&] (int j) {return p_h.getdifco2(phi[j],Sa[j]);};
        auto pa_Ma = [&] (int j) {return (pa[j]+param.getpa0())*mega*p_h.getMa(co2[j])/p_h.getMco2();};
        // auto pa_Da_Ma = [&] (int j) {return p_h.getdifco2(phi[j],Sa[j])*(pa[j]+param.getpa0())*mega*p_h.getMa(co2[j])/p_h.getMco2();};
        auto dDw_dphi = [&] (int j) {return p_h.ddiffCa_dphi(phi[j],Sa[j]);};
        auto dDa_dphi = [&] (int j) {return p_h.ddifco2_dphi(phi[j],Sa[j]);};
        auto dpa_Ma_dco2 = [&] (int j) {return (pa[j]+param.getpa0())*mega*p_h.dMa_dco2(co2[j])/p_h.getMco2();};
        // auto dpa_Da_Ma_dphi = [&] (int j) {return p_h.ddifco2_dphi(phi[j],Sa[j])*(pa[j]+param.getpa0())*mega*p_h.getMa(co2[j])/p_h.getMco2();};
        // auto dpa_Da_Ma_dco2 = [&] (int j) {return p_h.getdifco2(phi[j],Sa[j])*(pa[j]+param.getpa0())*mega*p_h.dMa_dco2(co2[j])/p_h.getMco2();};

        for (int i=0; i<int(lfsusize); ++i)
        {
          // horizontal fluxes
          if(i<pairx[i])
          {
            { // Ca diffuson
              RF gradCax = (Ca[pairx[i]]-Ca[i])/dx; // gradient
              // int uCax = gradCax>0. ? pairx[i] : i; // index of upstream DoF
              // RF qCa = -dy/2.*DCax*gradCax; // flux, dy/2 is the face area
              RF DCax = h2avg(Dw(i),Dw(pairx[i]));
              RF dqCa_dCai = -dy/2.*DCax*(-1.)/dx;
              RF dqCa_dCax = -dy/2.*DCax*( 1.)/dx;
              RF dqCa_dphii = -dy/2.*dh2avg_d1(Dw(i),Dw(pairx[i]))*dDw_dphi(i)*gradCax;
              RF dqCa_dphix = -dy/2.*dh2avg_d2(Dw(i),Dw(pairx[i]))*dDw_dphi(pairx[i])*gradCax;
              mat.accumulate(lfsv_Ca,      i ,lfsu_Ca ,      i , dqCa_dCai);
              mat.accumulate(lfsv_Ca,pairx[i],lfsu_Ca ,      i ,-dqCa_dCai);
              mat.accumulate(lfsv_Ca,      i ,lfsu_Ca ,pairx[i], dqCa_dCax);
              mat.accumulate(lfsv_Ca,pairx[i],lfsu_Ca ,pairx[i],-dqCa_dCax);
              mat.accumulate(lfsv_Ca,      i ,lfsu_phi,      i , dqCa_dphii);
              mat.accumulate(lfsv_Ca,pairx[i],lfsu_phi,      i ,-dqCa_dphii);
              mat.accumulate(lfsv_Ca,      i ,lfsu_phi,pairx[i], dqCa_dphix);
              mat.accumulate(lfsv_Ca,pairx[i],lfsu_phi,pairx[i],-dqCa_dphix);
            }
            { // co2 diffusion
              RF gradco2x = (co2[pairx[i]]-co2[i])/dx;
              int uco2x = gradco2x>0. ? pairx[i] : i;
              RF Dco2x = h2avg(Da(i),Da(pairx[i]))*pa_Ma(uco2x);
              // RF qco2 = -dy/2.*Dco2x*gradco2x;
              RF dqco2_dco2i = -dy/2.*Dco2x*(-1.)/dx;
              RF dqco2_dco2x = -dy/2.*Dco2x*( 1.)/dx;
              RF dqco2_dco2u = -dy/2.*h2avg(Da(i),Da(pairx[i]))*dpa_Ma_dco2(uco2x)*gradco2x;
              RF dqco2_dphii = -dy/2.*dh2avg_d1(Da(i),Da(pairx[i]))*dDa_dphi(i)*pa_Ma(uco2x)*gradco2x;
              RF dqco2_dphix = -dy/2.*dh2avg_d2(Da(i),Da(pairx[i]))*dDa_dphi(pairx[i])*pa_Ma(uco2x)*gradco2x;

              mat.accumulate(lfsv_co2,      i ,lfsu_co2,      i , dqco2_dco2i);
              mat.accumulate(lfsv_co2,pairx[i],lfsu_co2,      i ,-dqco2_dco2i);
              mat.accumulate(lfsv_co2,      i ,lfsu_co2,pairx[i], dqco2_dco2x);
              mat.accumulate(lfsv_co2,pairx[i],lfsu_co2,pairx[i],-dqco2_dco2x);
              mat.accumulate(lfsv_co2,      i ,lfsu_co2,  uco2x , dqco2_dco2u);
              mat.accumulate(lfsv_co2,pairx[i],lfsu_co2,  uco2x ,-dqco2_dco2u);
              mat.accumulate(lfsv_co2,      i ,lfsu_phi,      i , dqco2_dphii);
              mat.accumulate(lfsv_co2,pairx[i],lfsu_phi,      i ,-dqco2_dphii);
              mat.accumulate(lfsv_co2,      i ,lfsu_phi,pairx[i], dqco2_dphix);
              mat.accumulate(lfsv_co2,pairx[i],lfsu_phi,pairx[i],-dqco2_dphix);
            }
          }
          // vertical fluxes
          if(i<pairy[i])
          {
            { // Ca diffuson
              RF gradCay = (Ca[pairy[i]]-Ca[i])/dy; // gradient
              RF DCay = h2avg(Dw(i),Dw(pairy[i]));
              // RF qCa = -dx/2.*DCay*gradCay; // flux, dy/2 is the face area
              RF dqCa_dCai = -dx/2.*DCay*(-1.)/dy;
              RF dqCa_dCay = -dx/2.*DCay*( 1.)/dy;
              RF dqCa_dphii = -dx/2.*dh2avg_d1(Dw(i),Dw(pairy[i]))*dDw_dphi(i)*gradCay;
              RF dqCa_dphiy = -dx/2.*dh2avg_d2(Dw(i),Dw(pairy[i]))*dDw_dphi(pairy[i])*gradCay;
              mat.accumulate(lfsv_Ca,      i ,lfsu_Ca ,      i , dqCa_dCai);
              mat.accumulate(lfsv_Ca,pairy[i],lfsu_Ca ,      i ,-dqCa_dCai);
              mat.accumulate(lfsv_Ca,      i ,lfsu_Ca ,pairy[i], dqCa_dCay);
              mat.accumulate(lfsv_Ca,pairy[i],lfsu_Ca ,pairy[i],-dqCa_dCay);
              mat.accumulate(lfsv_Ca,      i ,lfsu_phi,      i , dqCa_dphii);
              mat.accumulate(lfsv_Ca,pairx[i],lfsu_phi,      i ,-dqCa_dphii);
              mat.accumulate(lfsv_Ca,      i ,lfsu_phi,pairy[i], dqCa_dphiy);
              mat.accumulate(lfsv_Ca,pairx[i],lfsu_phi,pairy[i],-dqCa_dphiy);
            }
            { // co2 diffusion
              RF gradco2y = (co2[pairy[i]]-co2[i])/dy;
              int uco2y = gradco2y>0. ? pairy[i] : i;
              RF Dco2y  = h2avg(Da(i),Da(pairy[i]))*pa_Ma(uco2y);
              // RF qco2 = -dx/2.*Dco2y*gradco2y;
              RF dqco2_dco2i = -dx/2.*Dco2y*(-1.)/dy;
              RF dqco2_dco2y = -dx/2.*Dco2y*( 1.)/dy;
              RF dqco2_dco2u = -dx/2.*h2avg(Da(i),Da(pairy[i]))*dpa_Ma_dco2(uco2y)*gradco2y;
              RF dqco2_dphii = -dx/2.*dh2avg_d1(Da(i),Da(pairy[i]))*dDa_dphi(i)*pa_Ma(uco2y)*gradco2y;
              RF dqco2_dphiy = -dx/2.*dh2avg_d2(Da(i),Da(pairy[i]))*dDa_dphi(pairy[i])*pa_Ma(uco2y)*gradco2y;
              mat.accumulate(lfsv_co2,      i ,lfsu_co2,      i , dqco2_dco2i);
              mat.accumulate(lfsv_co2,pairy[i],lfsu_co2,      i ,-dqco2_dco2i);
              mat.accumulate(lfsv_co2,      i ,lfsu_co2,pairy[i], dqco2_dco2y);
              mat.accumulate(lfsv_co2,pairy[i],lfsu_co2,pairy[i],-dqco2_dco2y);
              mat.accumulate(lfsv_co2,      i ,lfsu_co2,  uco2y , dqco2_dco2u);
              mat.accumulate(lfsv_co2,pairy[i],lfsu_co2,  uco2y ,-dqco2_dco2u);
              mat.accumulate(lfsv_co2,      i ,lfsu_phi,      i , dqco2_dphii);
              mat.accumulate(lfsv_co2,pairy[i],lfsu_phi,      i ,-dqco2_dphii);
              mat.accumulate(lfsv_co2,      i ,lfsu_phi,pairy[i], dqco2_dphiy);
              mat.accumulate(lfsv_co2,pairy[i],lfsu_phi,pairy[i],-dqco2_dphiy);
            }
          }
        }

        // dispersion term
        const RF& a_T = p_h.getalfat();
        const RF& a_L = p_h.getalfal();

        // calculate dispersion coefficients from velocity and saturation (factor for unsaturated conditions)
        auto getDisp = [&] (const RF& vx, const RF& vy, const RF& Sw)
        {
          using std::sqrt;
          RF denom = std::max(1e-5, sqrt(vx*vx+vy*vy)*Sw*Sw);
          RF D11 = (a_L*vx*vx+a_T*vy*vy)/denom;
          RF D22 = (a_T*vx*vx+a_L*vy*vy)/denom;
          RF D12 = (a_L-a_T)*vx*vy/denom;
          return std::tuple<RF,RF,RF>{D11,D12,D22};
        };
        // derive dispersion coefficients by velocity entries
        auto dDisp = [&] (const RF& vx, const RF& vy, const RF& Sw)
        {
          using std::sqrt;
          RF denom = std::max(1e-5, sqrt(vx*vx+vy*vy)*Sw*Sw);
          RF dms = -1./(std::max(1e-5,vx*vx+vy*vy));
          RF dD11_dx = a_L*2.*vx/denom +(a_L*vx*vx+a_T*vy*vy)*dms*vx/denom;
          RF dD11_dy = a_T*2.*vy/denom +(a_L*vx*vx+a_T*vy*vy)*dms*vy/denom;
          RF dD22_dx = a_T*2.*vx/denom +(a_T*vx*vx+a_L*vy*vy)*dms*vx/denom;
          RF dD22_dy = a_L*2.*vy/denom +(a_T*vx*vx+a_L*vy*vy)*dms*vy/denom;
          RF dD12_dx = (a_L-a_T)*vy/denom +(a_L-a_T)*vx*vy*dms*vx/denom;
          RF dD12_dy = (a_L-a_T)*vx/denom +(a_L-a_T)*vx*vy*dms*vy/denom;
          return std::tuple<RF,RF,RF,RF,RF,RF>{dD11_dx,dD11_dy,dD12_dx,dD12_dy,dD22_dx,dD22_dy};
        };


        // fluxes for dispersion constant thoughout whole cell (not control volume!)
        {
          RF dCax = (Ca[3]-Ca[2]+Ca[1]-Ca[0])/2./dx;
          RF dCay = (Ca[3]-Ca[1]+Ca[2]-Ca[0])/2./dy;
          RF Sw = 1.-(Sa[0]+Sa[1]+Sa[2]+Sa[3])/4.; // consider upwinding Sw and phi
          RF aphi = (phi[0]+phi[1]+phi[2]+phi[3])/4.;
          RF vwx, vwy; // fluxes used in dispersion
          RF dvwx_dphi02, dvwx_dphi13, dvwy_dphi01, dvwy_dphi23;

          {
            RF gradwx = (pw[1]-pw[0]+pw[3]-pw[2])*mega/2./dx; // derivation in x
            RF gradwy = (pw[2]-pw[0]+pw[3]-pw[1])*mega/2./dy+param.getrho_w()*param.getgrav(); // in y
            RF Kwx = h2avg(param.K((phi[0]+phi[2])/2.),param.K((phi[1]+phi[3])/2.));
            RF Kwy = h2avg(param.K((phi[0]+phi[1])/2.),param.K((phi[2]+phi[3])/2.));
            RF kwx = param.kw(gradwx<0 ? (Sa[0]+Sa[2])/2. : (Sa[1]+Sa[3])/2.)/param.getvisc_w();
            RF kwy = param.kw(gradwy<0 ? (Sa[0]+Sa[1])/2. : (Sa[2]+Sa[3])/2.)/param.getvisc_w();
            RF fwx = -Kwx*kwx*gradwx;
            RF fwy = -Kwy*kwy*gradwy;
            vwx = fwx/std::max(1e-5,Sw*aphi);
            vwy = fwy/std::max(1e-5,Sw*aphi);

            RF dKwx_d02 = dh2avg_d1(param.K((phi[0]+phi[2])/2.),param.K((phi[1]+phi[3])/2.)) * param.dK_dphi((phi[0]+phi[2])/2.) / 2.;
            RF dKwx_d13 = dh2avg_d2(param.K((phi[0]+phi[2])/2.),param.K((phi[1]+phi[3])/2.)) * param.dK_dphi((phi[1]+phi[3])/2.) / 2.;
            RF dKwy_d01 = dh2avg_d1(param.K((phi[0]+phi[1])/2.),param.K((phi[2]+phi[3])/2.)) * param.dK_dphi((phi[0]+phi[1])/2.) / 2.;
            RF dKwy_d23 = dh2avg_d2(param.K((phi[0]+phi[1])/2.),param.K((phi[2]+phi[3])/2.)) * param.dK_dphi((phi[2]+phi[3])/2.) / 2.;
            dvwx_dphi02 = -dKwx_d02*kwx*gradwx/std::max(1e-5,Sw*aphi);
            dvwx_dphi13 = -dKwx_d13*kwx*gradwx/std::max(1e-5,Sw*aphi);
            dvwy_dphi01 = -dKwy_d01*kwy*gradwy/std::max(1e-5,Sw*aphi);
            dvwy_dphi23 = -dKwy_d23*kwy*gradwy/std::max(1e-5,Sw*aphi);
          }
          std::array<const RF, lfsusize> dvwx_dphi{dvwx_dphi02,dvwx_dphi13,dvwx_dphi02,dvwx_dphi13};
          std::array<const RF, lfsusize> dvwy_dphi{dvwy_dphi01,dvwy_dphi01,dvwy_dphi23,dvwy_dphi23};

          auto [Dw11,Dw12,Dw22] = getDisp(vwx,vwy,Sw);
          auto [dDw11_dvwx,dDw11_dvwy,dDw12_dvwx,dDw12_dvwy,dDw22_dvwx,dDw22_dvwy] = dDisp(vwx,vwy,Sw);

          // accumulate bottom and top face (from left to right, direction depends on flux calculation)
          {
            // r.accumulate(lfsv_Ca,0,-dy/2.*(Dw11*dCax+Dw12*dCay)); // bottom
            // r.accumulate(lfsv_Ca,1, dy/2.*(Dw11*dCax+Dw12*dCay)); // bottom
            // r.accumulate(lfsv_Ca,2,-dy/2.*(Dw11*dCax+Dw12*dCay)); // top
            // r.accumulate(lfsv_Ca,3, dy/2.*(Dw11*dCax+Dw12*dCay)); // top
            mat.accumulate(lfsv_Ca,0,lfsu_Ca,0,-dy/2.*(-Dw11/2./dx-Dw12/2./dy));
            mat.accumulate(lfsv_Ca,1,lfsu_Ca,0, dy/2.*(-Dw11/2./dx-Dw12/2./dy));
            mat.accumulate(lfsv_Ca,2,lfsu_Ca,0,-dy/2.*(-Dw11/2./dx-Dw12/2./dy));
            mat.accumulate(lfsv_Ca,3,lfsu_Ca,0, dy/2.*(-Dw11/2./dx-Dw12/2./dy));

            mat.accumulate(lfsv_Ca,0,lfsu_Ca,1,-dy/2.*( Dw11/2./dx-Dw12/2./dy));
            mat.accumulate(lfsv_Ca,1,lfsu_Ca,1, dy/2.*( Dw11/2./dx-Dw12/2./dy));
            mat.accumulate(lfsv_Ca,2,lfsu_Ca,1,-dy/2.*( Dw11/2./dx-Dw12/2./dy));
            mat.accumulate(lfsv_Ca,3,lfsu_Ca,1, dy/2.*( Dw11/2./dx-Dw12/2./dy));

            mat.accumulate(lfsv_Ca,0,lfsu_Ca,2,-dy/2.*(-Dw11/2./dx+Dw12/2./dy));
            mat.accumulate(lfsv_Ca,1,lfsu_Ca,2, dy/2.*(-Dw11/2./dx+Dw12/2./dy));
            mat.accumulate(lfsv_Ca,2,lfsu_Ca,2,-dy/2.*(-Dw11/2./dx+Dw12/2./dy));
            mat.accumulate(lfsv_Ca,3,lfsu_Ca,2, dy/2.*(-Dw11/2./dx+Dw12/2./dy));

            mat.accumulate(lfsv_Ca,0,lfsu_Ca,3,-dy/2.*( Dw11/2./dx+Dw12/2./dy));
            mat.accumulate(lfsv_Ca,1,lfsu_Ca,3, dy/2.*( Dw11/2./dx+Dw12/2./dy));
            mat.accumulate(lfsv_Ca,2,lfsu_Ca,3,-dy/2.*( Dw11/2./dx+Dw12/2./dy));
            mat.accumulate(lfsv_Ca,3,lfsu_Ca,3, dy/2.*( Dw11/2./dx+Dw12/2./dy));

          }
          // accumulate left and right face (from bottom to top)
          {
            // r.accumulate(lfsv_Ca,0,-dx/2.*(Dw22*dCay+Dw12*dCax)); // left
            // r.accumulate(lfsv_Ca,2, dx/2.*(Dw22*dCay+Dw12*dCax)); // left
            // r.accumulate(lfsv_Ca,1,-dx/2.*(Dw22*dCay+Dw12*dCax)); // right
            // r.accumulate(lfsv_Ca,3, dx/2.*(Dw22*dCay+Dw12*dCax)); // right
            mat.accumulate(lfsv_Ca,0,lfsu_Ca,0,-dx/2.*(-Dw22/2./dy-Dw12/2./dx));
            mat.accumulate(lfsv_Ca,2,lfsu_Ca,0, dx/2.*(-Dw22/2./dy-Dw12/2./dx));
            mat.accumulate(lfsv_Ca,1,lfsu_Ca,0,-dx/2.*(-Dw22/2./dy-Dw12/2./dx));
            mat.accumulate(lfsv_Ca,3,lfsu_Ca,0, dx/2.*(-Dw22/2./dy-Dw12/2./dx));

            mat.accumulate(lfsv_Ca,0,lfsu_Ca,1,-dx/2.*(-Dw22/2./dy+Dw12/2./dx));
            mat.accumulate(lfsv_Ca,2,lfsu_Ca,1, dx/2.*(-Dw22/2./dy+Dw12/2./dx));
            mat.accumulate(lfsv_Ca,1,lfsu_Ca,1,-dx/2.*(-Dw22/2./dy+Dw12/2./dx));
            mat.accumulate(lfsv_Ca,3,lfsu_Ca,1, dx/2.*(-Dw22/2./dy+Dw12/2./dx));

            mat.accumulate(lfsv_Ca,0,lfsu_Ca,2,-dx/2.*( Dw22/2./dy-Dw12/2./dx));
            mat.accumulate(lfsv_Ca,2,lfsu_Ca,2, dx/2.*( Dw22/2./dy-Dw12/2./dx));
            mat.accumulate(lfsv_Ca,1,lfsu_Ca,2,-dx/2.*( Dw22/2./dy-Dw12/2./dx));
            mat.accumulate(lfsv_Ca,3,lfsu_Ca,2, dx/2.*( Dw22/2./dy-Dw12/2./dx));

            mat.accumulate(lfsv_Ca,0,lfsu_Ca,3,-dx/2.*( Dw22/2./dy+Dw12/2./dx));
            mat.accumulate(lfsv_Ca,2,lfsu_Ca,3, dx/2.*( Dw22/2./dy+Dw12/2./dx));
            mat.accumulate(lfsv_Ca,1,lfsu_Ca,3,-dx/2.*( Dw22/2./dy+Dw12/2./dx));
            mat.accumulate(lfsv_Ca,3,lfsu_Ca,3, dx/2.*( Dw22/2./dy+Dw12/2./dx));
          }

          // derive by average of phi in denominator of vwx, vwy
          RF dDw11_denphi{0}, dDw12_denphi{0}, dDw22_denphi{0};
          if (Sw*aphi>1e-5) // regularization cut-out
          {
            RF d_phi = -1./aphi/4.;
            dDw11_denphi = dDw11_dvwx*vwx*d_phi + dDw11_dvwy*vwy*d_phi;
            dDw12_denphi = dDw12_dvwx*vwx*d_phi + dDw12_dvwy*vwy*d_phi;
            dDw22_denphi = dDw22_dvwx*vwx*d_phi + dDw22_dvwy*vwy*d_phi;
          }

          for (size_t i=0; i<lfsusize; ++i)
          {
            // and derive by phi in Kwx and Kwy (h2avg of pairs)
            RF dDw11_dphii = dDw11_denphi + dDw11_dvwx*dvwx_dphi[i] + dDw11_dvwy*dvwy_dphi[i];
            RF dDw12_dphii = dDw12_denphi + dDw12_dvwx*dvwx_dphi[i] + dDw12_dvwy*dvwy_dphi[i];
            RF dDw22_dphii = dDw22_denphi + dDw22_dvwx*dvwx_dphi[i] + dDw22_dvwy*dvwy_dphi[i];

            mat.accumulate(lfsv_Ca,0,lfsu_phi,i, -dy/2.*( dDw11_dphii*dCax + dDw12_dphii*dCay ) ); // bottom
            mat.accumulate(lfsv_Ca,1,lfsu_phi,i,  dy/2.*( dDw11_dphii*dCax + dDw12_dphii*dCay ) ); // bottom
            mat.accumulate(lfsv_Ca,2,lfsu_phi,i, -dy/2.*( dDw11_dphii*dCax + dDw12_dphii*dCay ) ); // top
            mat.accumulate(lfsv_Ca,3,lfsu_phi,i,  dy/2.*( dDw11_dphii*dCax + dDw12_dphii*dCay ) ); // top
            mat.accumulate(lfsv_Ca,0,lfsu_phi,i, -dx/2.*( dDw22_dphii*dCay + dDw12_dphii*dCax ) ); // left
            mat.accumulate(lfsv_Ca,2,lfsu_phi,i,  dx/2.*( dDw22_dphii*dCay + dDw12_dphii*dCax ) ); // left
            mat.accumulate(lfsv_Ca,1,lfsu_phi,i, -dx/2.*( dDw22_dphii*dCay + dDw12_dphii*dCax ) ); // right
            mat.accumulate(lfsv_Ca,3,lfsu_phi,i,  dx/2.*( dDw22_dphii*dCay + dDw12_dphii*dCax ) ); // right
          }

          const bool upwinded = param.dispUpwinded(); // upwinds dispersion coefficients
          if (upwinded)
          {
            RF lp = dy/2.*Dw11-dx/2.*Dw22; // longitudinal parts
            RF tp = (dy-dx)/2.*Dw12; // transversal parts
            if (std::abs(lp)>std::abs(tp)) // transversal dispersion does not affect signs (matrix signs are symmetric)
            {
              RF flux = std::abs(lp) + std::abs(tp);
              if (lp>0) // flux in x direction dominates
              {
                // r.accumulate(lfsv_Ca,0,-flux*(Ca_dif[2]-Ca_dif[0])/2./dy );
                // r.accumulate(lfsv_Ca,2, flux*(Ca_dif[2]-Ca_dif[0])/2./dy );
                // r.accumulate(lfsv_Ca,1,-flux*(Ca_dif[3]-Ca_dif[1])/2./dy );
                // r.accumulate(lfsv_Ca,3, flux*(Ca_dif[3]-Ca_dif[1])/2./dy );
                mat.accumulate(lfsv_Ca,0,lfsu_Ca,0, flux/2./dy );
                mat.accumulate(lfsv_Ca,0,lfsu_Ca,2,-flux/2./dy );
                mat.accumulate(lfsv_Ca,2,lfsu_Ca,0,-flux/2./dy );
                mat.accumulate(lfsv_Ca,2,lfsu_Ca,2, flux/2./dy );

                mat.accumulate(lfsv_Ca,1,lfsu_Ca,1, flux/2./dy );
                mat.accumulate(lfsv_Ca,1,lfsu_Ca,3,-flux/2./dy );
                mat.accumulate(lfsv_Ca,3,lfsu_Ca,1,-flux/2./dy );
                mat.accumulate(lfsv_Ca,3,lfsu_Ca,3, flux/2./dy );
              }
              else // flux in y direction dominates
              {
                // r.accumulate(lfsv_Ca,0,-flux*(Ca_dif[1]-Ca_dif[0])/2./dx );
                // r.accumulate(lfsv_Ca,1, flux*(Ca_dif[1]-Ca_dif[0])/2./dx );
                // r.accumulate(lfsv_Ca,2,-flux*(Ca_dif[3]-Ca_dif[2])/2./dx );
                // r.accumulate(lfsv_Ca,3, flux*(Ca_dif[3]-Ca_dif[2])/2./dx );
                mat.accumulate(lfsv_Ca,0,lfsu_Ca,0, flux/2./dx );
                mat.accumulate(lfsv_Ca,0,lfsu_Ca,1,-flux/2./dx );
                mat.accumulate(lfsv_Ca,1,lfsu_Ca,0,-flux/2./dx );
                mat.accumulate(lfsv_Ca,1,lfsu_Ca,1, flux/2./dx );

                mat.accumulate(lfsv_Ca,2,lfsu_Ca,2, flux/2./dx );
                mat.accumulate(lfsv_Ca,2,lfsu_Ca,3,-flux/2./dx );
                mat.accumulate(lfsv_Ca,3,lfsu_Ca,2,-flux/2./dx );
                mat.accumulate(lfsv_Ca,3,lfsu_Ca,3, flux/2./dx );
              }
            }
            else
            {
              // transversal dispersion decides signs
              RF flux = -lp+std::abs(tp); // -x+t+y-t
              // r.accumulate(lfsv_Ca,0,-flux*(Ca_dif[1]-Ca_dif[0])/2./dx );
              // r.accumulate(lfsv_Ca,1, flux*(Ca_dif[1]-Ca_dif[0])/2./dx );
              // r.accumulate(lfsv_Ca,2,-flux*(Ca_dif[3]-Ca_dif[2])/2./dx );
              // r.accumulate(lfsv_Ca,3, flux*(Ca_dif[3]-Ca_dif[2])/2./dx );
              mat.accumulate(lfsv_Ca,0,lfsu_Ca,0, flux/2./dx );
              mat.accumulate(lfsv_Ca,0,lfsu_Ca,1,-flux/2./dx );
              mat.accumulate(lfsv_Ca,1,lfsu_Ca,0,-flux/2./dx );
              mat.accumulate(lfsv_Ca,1,lfsu_Ca,1, flux/2./dx );

              mat.accumulate(lfsv_Ca,2,lfsu_Ca,2, flux/2./dx );
              mat.accumulate(lfsv_Ca,2,lfsu_Ca,3,-flux/2./dx );
              mat.accumulate(lfsv_Ca,3,lfsu_Ca,2,-flux/2./dx );
              mat.accumulate(lfsv_Ca,3,lfsu_Ca,3, flux/2./dx );
              flux = lp+std::abs(tp); // x+t-y-t
              // r.accumulate(lfsv_Ca,0,-flux*(Ca_dif[2]-Ca_dif[0])/2./dy );
              // r.accumulate(lfsv_Ca,2, flux*(Ca_dif[2]-Ca_dif[0])/2./dy );
              // r.accumulate(lfsv_Ca,1,-flux*(Ca_dif[3]-Ca_dif[1])/2./dy );
              // r.accumulate(lfsv_Ca,3, flux*(Ca_dif[3]-Ca_dif[1])/2./dy );
              mat.accumulate(lfsv_Ca,0,lfsu_Ca,0, flux/2./dy );
              mat.accumulate(lfsv_Ca,0,lfsu_Ca,2,-flux/2./dy );
              mat.accumulate(lfsv_Ca,2,lfsu_Ca,0,-flux/2./dy );
              mat.accumulate(lfsv_Ca,2,lfsu_Ca,2, flux/2./dy );

              mat.accumulate(lfsv_Ca,1,lfsu_Ca,1, flux/2./dy );
              mat.accumulate(lfsv_Ca,1,lfsu_Ca,3,-flux/2./dy );
              mat.accumulate(lfsv_Ca,3,lfsu_Ca,1,-flux/2./dy );
              mat.accumulate(lfsv_Ca,3,lfsu_Ca,3, flux/2./dy );
            }
            // derive by phi
            {
              RF lp = dy/2.*Dw11-dx/2.*Dw22; // longitudinal parts
              RF tp = (dy-dx)/2.*Dw12; // transversal parts
              std::array<RF,lfsusize> dlp_dphi, dtp_dphi;
              for (size_t i=0; i<lfsusize; ++i)
              {
                dlp_dphi[i] = dy/2.*(dDw11_denphi + dDw11_dvwx*dvwx_dphi[i] + dDw11_dvwy*dvwy_dphi[i])
                             -dx/2.*(dDw22_denphi + dDw22_dvwx*dvwx_dphi[i] + dDw22_dvwy*dvwy_dphi[i]);
                dtp_dphi[i] = (dy/dx)/2.*(dDw12_denphi + dDw12_dvwx*dvwx_dphi[i] + dDw12_dvwy*dvwy_dphi[i]);
              }
              RF signl = lp>0. ? 1. : -1.;
              RF signt = tp>0. ? 1. : -1.;

              if (std::abs(lp)>std::abs(tp)) // transversal dispersion does not affect signs (matrix signs are symmetric)
              {
                // RF flux = std::abs(lp) + std::abs(tp);

                if (lp>0) // flux in x direction dominates
                {
                  // r.accumulate(lfsv_Ca,0,-flux*(Ca_dif[2]-Ca_dif[0])/2./dy );
                  // r.accumulate(lfsv_Ca,2, flux*(Ca_dif[2]-Ca_dif[0])/2./dy );
                  // r.accumulate(lfsv_Ca,1,-flux*(Ca_dif[3]-Ca_dif[1])/2./dy );
                  // r.accumulate(lfsv_Ca,3, flux*(Ca_dif[3]-Ca_dif[1])/2./dy );
                  for (std::size_t i=0; i<lfsusize; ++i)
                  {
                    RF dflux_dphi = signl*(dlp_dphi[i]) + signt*dtp_dphi[i];
                    mat.accumulate(lfsv_Ca,0,lfsu_phi,i,-dflux_dphi*(Ca[2]-Ca[0])/2./dy );
                    mat.accumulate(lfsv_Ca,2,lfsu_phi,i, dflux_dphi*(Ca[2]-Ca[0])/2./dy );
                    mat.accumulate(lfsv_Ca,1,lfsu_phi,i,-dflux_dphi*(Ca[3]-Ca[1])/2./dy );
                    mat.accumulate(lfsv_Ca,3,lfsu_phi,i, dflux_dphi*(Ca[3]-Ca[1])/2./dy );
                  }
                }
                else // flux in y direction dominates
                {
                  // r.accumulate(lfsv_Ca,0,-flux*(Ca_dif[1]-Ca_dif[0])/2./dx );
                  // r.accumulate(lfsv_Ca,1, flux*(Ca_dif[1]-Ca_dif[0])/2./dx );
                  // r.accumulate(lfsv_Ca,2,-flux*(Ca_dif[3]-Ca_dif[2])/2./dx );
                  // r.accumulate(lfsv_Ca,3, flux*(Ca_dif[3]-Ca_dif[2])/2./dx );
                  for (std::size_t i=0; i<lfsusize; ++i)
                  {
                    RF dflux_dphi = signl*(dlp_dphi[i]) + signt*dtp_dphi[i];
                    mat.accumulate(lfsv_Ca,0,lfsu_phi,i,-dflux_dphi*(Ca[1]-Ca[0])/2./dx );
                    mat.accumulate(lfsv_Ca,1,lfsu_phi,i, dflux_dphi*(Ca[1]-Ca[0])/2./dx );
                    mat.accumulate(lfsv_Ca,2,lfsu_phi,i,-dflux_dphi*(Ca[3]-Ca[2])/2./dx );
                    mat.accumulate(lfsv_Ca,3,lfsu_phi,i, dflux_dphi*(Ca[3]-Ca[2])/2./dx );
                  }
                }
              }
              else // |tp|>|lp|, transversal dispersion dictates signs
              {
                // transversal dispersion decides signs
                // RF flux = -lp+|tp|;
                // r.accumulate(lfsv_Ca,0,-flux*(Ca_dif[1]-Ca_dif[0])/2./dx );
                // r.accumulate(lfsv_Ca,1, flux*(Ca_dif[1]-Ca_dif[0])/2./dx );
                // r.accumulate(lfsv_Ca,2,-flux*(Ca_dif[3]-Ca_dif[2])/2./dx );
                // r.accumulate(lfsv_Ca,3, flux*(Ca_dif[3]-Ca_dif[2])/2./dx );
                for (size_t i=0; i<lfsusize; ++i)
                {
                  RF dflux_dphi = -dlp_dphi[i] + signt*dtp_dphi[i];
                  mat.accumulate(lfsv_Ca,0,lfsu_phi,i,-dflux_dphi*(Ca[1]-Ca[0])/2./dx );
                  mat.accumulate(lfsv_Ca,1,lfsu_phi,i, dflux_dphi*(Ca[1]-Ca[0])/2./dx );
                  mat.accumulate(lfsv_Ca,2,lfsu_phi,i,-dflux_dphi*(Ca[3]-Ca[2])/2./dx );
                  mat.accumulate(lfsv_Ca,3,lfsu_phi,i, dflux_dphi*(Ca[3]-Ca[2])/2./dx );
                }

                // flux = lp+|tp|;
                // r.accumulate(lfsv_Ca,0,-flux*(Ca_dif[2]-Ca_dif[0])/2./dy );
                // r.accumulate(lfsv_Ca,2, flux*(Ca_dif[2]-Ca_dif[0])/2./dy );
                // r.accumulate(lfsv_Ca,1,-flux*(Ca_dif[3]-Ca_dif[1])/2./dy );
                // r.accumulate(lfsv_Ca,3, flux*(Ca_dif[3]-Ca_dif[1])/2./dy );
                for (size_t i=0; i<lfsusize; ++i)
                {
                  RF dflux_dphi = dlp_dphi[i] + signt*dtp_dphi[i];
                  mat.accumulate(lfsv_Ca,0,lfsu_phi,i,-dflux_dphi*(Ca[2]-Ca[0])/2./dy );
                  mat.accumulate(lfsv_Ca,2,lfsu_phi,i, dflux_dphi*(Ca[2]-Ca[0])/2./dy );
                  mat.accumulate(lfsv_Ca,1,lfsu_phi,i,-dflux_dphi*(Ca[3]-Ca[1])/2./dy );
                  mat.accumulate(lfsv_Ca,3,lfsu_phi,i, dflux_dphi*(Ca[3]-Ca[1])/2./dy );
                }
              }
            } // end phi block
          } // end upwinded block
        } // end block for dispersion
      } // end if((osType%4)/2==1)