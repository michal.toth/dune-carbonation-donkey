      constexpr std::size_t lfsusize = 4; // this file is inside if(lfsusize==4)
                                         // overwriting enables using it as a template argument

      const auto& a0 = eg.geometry().corner(0);
      const auto& c3 = eg.geometry().corner(3);
      const DF dx = c3[0]-a0[0];
      const DF dy = c3[1]-a0[1];
      assert(dx>0 && dy>0); // assumption on the rectangle orientation
      constexpr std::array<int,lfsusize> pairx{1,0,3,2};
      constexpr std::array<int,lfsusize> pairy{2,3,0,1};

      std::array<RF,lfsusize> Kw, Ka, kw, ka; // better have vector of coefficients than re-compute pow()
      std::array<RF,lfsusize> dkw_dSa, dka_dSa;
      for (size_t i=0; i<lfsusize; ++i)
      {
        Kw[i] = param.K(phi[i]); // intrinsic permeability
        Ka[i] = Kw[i]*param.getKa_correction(); // air has different intrinsic perm. because water is chemically bound
        kw[i] = param.kw(Sa[i]); // relative permeability water
        ka[i] = param.ka(Sa[i]); // relative permeability air
        dkw_dSa[i] = param.dkw_dSa(Sa[i]);
        dka_dSa[i] = param.dka_dSa(Sa[i]);
      }
      auto lkw = [&] (int j) {return kw[j]*param.getrho_w()/param.getvisc_w();}; // relative permeability, density and viscosity
      auto lka = [&] (int j) {return ka[j]*param.getrho_a(pa[j],co2[j])/param.visc_mix(co2[j]);};
      auto dlkw_dSa = [&] (int j) {return dkw_dSa[j]*param.getrho_w()/param.getvisc_w();}; // derive permeability
      auto dlka_dpw = [&] (int j) {return ka[j]*param.drho_a_dpa(pa[j],co2[j])/param.visc_mix(co2[j]);}; // derive density
      auto dlka_dSa = [&] (int j) {return dka_dSa[j]*param.getrho_a(pa[j],co2[j])/param.visc_mix(co2[j])+dlka_dpw(j)*dpc[j];}; // derive permeability and density

      for (int i=0; i<int(lfsusize); ++i)
      {
        if (i<pairx[i])
        {
          { // water flow
            RF gradwx = (pw[pairx[i]]-pw[i])*mega/dx;
            RF Kwx = h2avg(Kw[i],Kw[pairx[i]]); // intrinsic permeability, harmonic averaged
            size_t uwx = gradwx > 0. ? pairx[i] : i; // index of upwind source
            RF kwx = lkw(uwx); // upwinded relative permeability, density, and viscosity
            // RF dqwx_dphii = -dy/2.*kwx*gradwx* dh2avg_d1(Kw[i],Kw[pairx[i]])*dKw_dphi[i]; // _dphi is only in fully-coupled case
            // RF dqwx_dphix = -dy/2.*kwx*gradwx* dh2avg_d2(Kw[i],Kw[pairx[i]])*dKw_dphi[pairx[i]];
            RF dqwx_dSau = -dy/2.*Kwx*dlkw_dSa(uwx)*gradwx;
            RF dqwx_dpwi = -dy/2.*Kwx*kwx*(-1.)*mega/dx;
            RF dqwx_dpwx = -dy/2.*Kwx*kwx*( 1.)*mega/dx;
            mat.accumulate(lfsv_pw,      i ,lfsu_pw,      i , dqwx_dpwi);
            mat.accumulate(lfsv_pw,pairx[i],lfsu_pw,      i ,-dqwx_dpwi);
            mat.accumulate(lfsv_pw,      i ,lfsu_pw,pairx[i], dqwx_dpwx);
            mat.accumulate(lfsv_pw,pairx[i],lfsu_pw,pairx[i],-dqwx_dpwx);
            mat.accumulate(lfsv_pw,      i ,lfsu_Sa,    uwx , dqwx_dSau);
            mat.accumulate(lfsv_pw,pairx[i],lfsu_Sa,    uwx ,-dqwx_dSau);
          }
          { // air flow
            RF gradax = (pa[pairx[i]]-pa[i])*mega/dx;
            RF Kax = h2avg(Ka[i],Ka[pairx[i]]);
            size_t uax = gradax > 0. ? pairx[i] : i;
            RF kax = lka(uax);
            // RF dqax_dphii = -dy/2.*kax*gradax* dh2avg_d1(Ka[i],Ka[pairx[i]])*dKa_dphi[i];
            // RF dqax_dphix = -dy/2.*kax*gradax* dh2avg_d2(Ka[i],Ka[pairx[i]])*dKa_dphi[pairx[i]];
            RF dqax_dpwi = -dy/2.*Kax*kax*(-1.)*mega/dx;
            RF dqax_dpwx = -dy/2.*Kax*kax*( 1.)*mega/dx;
            RF dqax_dSai = -dy/2.*Kax*kax*(-1.*dpc[i])*mega/dx;
            RF dqax_dSax = -dy/2.*Kax*kax*( 1.*dpc[pairx[i]])*mega/dx;
            RF dqax_dSau = -dy/2.*Kax*dlka_dSa(uax)*gradax;
            RF dqax_dpwu = -dy/2.*Kax*dlka_dpw(uax)*gradax;
            mat.accumulate(lfsv_Sa,      i ,lfsu_pw,      i , dqax_dpwi);
            mat.accumulate(lfsv_Sa,pairx[i],lfsu_pw,      i ,-dqax_dpwi);
            mat.accumulate(lfsv_Sa,      i ,lfsu_pw,pairx[i], dqax_dpwx);
            mat.accumulate(lfsv_Sa,pairx[i],lfsu_pw,pairx[i],-dqax_dpwx);
            mat.accumulate(lfsv_Sa,      i ,lfsu_pw,    uax , dqax_dpwu);
            mat.accumulate(lfsv_Sa,pairx[i],lfsu_pw,    uax ,-dqax_dpwu);
            mat.accumulate(lfsv_Sa,      i ,lfsu_Sa,      i , dqax_dSai);
            mat.accumulate(lfsv_Sa,pairx[i],lfsu_Sa,      i ,-dqax_dSai);
            mat.accumulate(lfsv_Sa,      i ,lfsu_Sa,pairx[i], dqax_dSax);
            mat.accumulate(lfsv_Sa,pairx[i],lfsu_Sa,pairx[i],-dqax_dSax);
            mat.accumulate(lfsv_Sa,      i ,lfsu_Sa,    uax , dqax_dSau);
            mat.accumulate(lfsv_Sa,pairx[i],lfsu_Sa,    uax ,-dqax_dSau);
          }
          // fully coupled case is also missing _dco2 (_dphi is finished)
        }

        if (i<pairy[i])
        {
          { // water flow
            RF gradwy = ((pw[pairy[i]]-pw[i])*mega/dy+param.getgrav()*param.getrho_w()); // direction of gravity determined by i<pairy[i]
            RF Kwy = h2avg(Kw[i],Kw[pairy[i]]);
            size_t uwy = gradwy > 0. ? pairy[i] : i;
            RF kwy = lkw(uwy);

            // RF dqwy_dphii = -dx/2.*kwy*gradwy* dh2avg_d1(Kw[i],Kw[pairy[i]])*dKw_dphi[i];
            // RF dqwy_dphiy = -dx/2.*kwy*gradwy* dh2avg_d2(Kw[i],Kw[pairy[i]])*dKw_dphi[pairy[i]];
            RF dqwy_dSau = -dx/2.*Kwy*dlkw_dSa(uwy)*gradwy;
            RF dqwy_dpwi = -dx/2.*Kwy*kwy*(-1.)*mega/dy;
            RF dqwy_dpwy = -dx/2.*Kwy*kwy*( 1.)*mega/dy;
            mat.accumulate(lfsv_pw,      i ,lfsu_pw,      i , dqwy_dpwi);
            mat.accumulate(lfsv_pw,pairy[i],lfsu_pw,      i ,-dqwy_dpwi);
            mat.accumulate(lfsv_pw,      i ,lfsu_pw,pairy[i], dqwy_dpwy);
            mat.accumulate(lfsv_pw,pairy[i],lfsu_pw,pairy[i],-dqwy_dpwy);
            mat.accumulate(lfsv_pw,      i ,lfsu_Sa,    uwy , dqwy_dSau);
            mat.accumulate(lfsv_pw,pairy[i],lfsu_Sa,    uwy ,-dqwy_dSau);
          }
          { // air flow
            RF rho_ay = (p_h.getrho_a(pa[i],co2[i])+p_h.getrho_a(pa[pairy[i]],co2[pairy[i]]))/2.;
            RF graday = ((pa[pairy[i]]-pa[i])*mega/dy+param.getgrav()*rho_ay);
            RF Kay = h2avg(Ka[i],Ka[pairy[i]]);
            size_t uay = graday > 0. ? pairy[i] : i;
            RF kay = lka(uay);
            // RF dqay_dphii = -dy/2.*kay*graday* dh2avg_d1(Ka[i],Ka[pairy[i]])*dKa_dphi[i];
            // RF dqay_dphiy = -dy/2.*kay*graday* dh2avg_d2(Ka[i],Ka[pairy[i]])*dKa_dphi[pairy[i]];
            RF dqay_dpwi = -dx/2.*Kay*kay; // all except graday
            RF dqay_dpwy = dqay_dpwi;
            dqay_dpwi *= -1.*mega/dy+param.getgrav()*param.drho_a_dpa(pa[i],co2[i])/2.; // graday by pw[i]
            dqay_dpwy *=  1.*mega/dy+param.getgrav()*param.drho_a_dpa(pa[pairy[i]],co2[pairy[i]])/2.; // graday by pw[pairy[i]]
            RF dqay_dSai = dqay_dpwi*dpc[i]; // in qay, pw derives pa term, Sa derives that too and adds dpc_dSa (pa=pw+pc(Sa))
            RF dqay_dSay = dqay_dpwy*dpc[pairy[i]];
            RF dqay_dSau = -dx/2.*Kay*dlka_dSa(uay)*graday;
            RF dqay_dpwu = -dx/2.*Kay*dlka_dpw(uay)*graday;
            // RF dqay_dco2i += -dx/2.*Kay*kay*(-(i-pairy[i])/2.*param.getgrav()*param.drho_a_co2(pa[i],co2[i])/2.);
            // RF dqay_dco2y += -dx/2.*Kay*kay*(-(i-pairy[i])/2.*param.getgrav()*param.drho_a_co2(pa[pairy[i]],co2[pairy[i]])/2.);
            mat.accumulate(lfsv_Sa,      i ,lfsu_pw,      i , dqay_dpwi);
            mat.accumulate(lfsv_Sa,pairy[i],lfsu_pw,      i ,-dqay_dpwi);
            mat.accumulate(lfsv_Sa,      i ,lfsu_pw,pairy[i], dqay_dpwy);
            mat.accumulate(lfsv_Sa,pairy[i],lfsu_pw,pairy[i],-dqay_dpwy);
            mat.accumulate(lfsv_Sa,      i ,lfsu_pw,    uay , dqay_dpwu);
            mat.accumulate(lfsv_Sa,pairy[i],lfsu_pw,    uay ,-dqay_dpwu);
            mat.accumulate(lfsv_Sa,      i ,lfsu_Sa,      i , dqay_dSai);
            mat.accumulate(lfsv_Sa,pairy[i],lfsu_Sa,      i ,-dqay_dSai);
            mat.accumulate(lfsv_Sa,      i ,lfsu_Sa,pairy[i], dqay_dSay);
            mat.accumulate(lfsv_Sa,pairy[i],lfsu_Sa,pairy[i],-dqay_dSay);
            mat.accumulate(lfsv_Sa,      i ,lfsu_Sa,    uay , dqay_dSau);
            mat.accumulate(lfsv_Sa,pairy[i],lfsu_Sa,    uay ,-dqay_dSau);
          }
        }
      }