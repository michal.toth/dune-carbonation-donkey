      constexpr std::size_t lfsusize = 4; // this file is inside if(lfsusize==4)
                                     // overwriting enables using it as a template argument

      const auto& a0 = eg.geometry().corner(0);
      const auto& c3 = eg.geometry().corner(3);
      DF dx = c3[0]-a0[0];
      DF dy = c3[1]-a0[1];
      constexpr std::array<int,lfsusize> pairx{1,0,3,2};
      constexpr std::array<int,lfsusize> pairy{2,3,0,1};

      std::array<RF,lfsusize> Kwr, Kac, Karc; // better have vector of coefficients than re-compute pow()
      for (size_t i=0; i<lfsusize; ++i)
      {
        Kwr[i] = param.Kwr(Sa[i],phi[i]);
        Kac[i] = param.Kac(Sa[i],phi[i],co2[i]);
        Karc[i] = Kac[i]*param.getrho_a(pa[i],co2[i]);
      }

      for (int i=0; i<int(lfsusize); ++i)
      {
        // water and air diffusion
        RF rho_ay = p_h.getrho_a((pa[i]+pa[pairy[i]])/2.,(co2[i]+co2[pairy[i]])/2.);
        // RF rho_ay = (p_h.getrho_a(pa[i],co2[i])+p_h.getrho_a(pa[pairy[i]],co2[pairy[i]]))/2.; // TODO: check difference of inner and outer arit average
        RF qwx = -dy/2.*h2avg(Kwr[i],Kwr[pairx[i]],"Kwr")  * (pw[pairx[i]]-pw[i])/dx;
        RF qwy = -dx/2.*h2avg(Kwr[i],Kwr[pairy[i]],"Kwr")  *((pw[pairy[i]]-pw[i])/dy-(i-pairy[i])/2.*param.getgrav()*param.getrho_w());
        RF qax = -dy/2.*h2avg(Karc[i],Karc[pairx[i]],"Karc")* (pa[pairx[i]]-pa[i])/dx;
        RF qay = -dx/2.*h2avg(Karc[i],Karc[pairy[i]],"Karc")*((pa[pairy[i]]-pa[i])/dy-(i-pairy[i])/2.*param.getgrav()*rho_ay);
        // TODO? some models have also mixed, coupling terms for twophase flow
        // I access all vertices, so fluxes in the opposite way are accumulated there
        r.accumulate(lfsv_pw,i, qwx + qwy);
        r.accumulate(lfsv_Sa,i, qax + qay);

        // Ca and CO_2 convection
        qwy /= param.getrho_w();
        qwx /= param.getrho_w();
        qay *= param.getR()*param.getT();
        qax *= param.getR()*param.getT();

        // either I access the same edge twice and divide it by 2,
        // or accumulate only one edge -note that qwx, qwy have different sign then
        RF increment = qwx*(qwx>0 ? Ca[i] : Ca[pairx[i]]);
        r.accumulate(lfsv_Ca,      i, +increment );
           increment = qwy*(qwy>0 ? Ca[i] : Ca[pairy[i]]);
        r.accumulate(lfsv_Ca,      i, +increment );
        std::size_t branchco2 = qax>0 ? i : pairx[i];
           increment = qax/p_h.getMa(co2[branchco2])*co2[branchco2];
        r.accumulate(lfsv_co2,      i, +increment );
                    branchco2 = qay>0 ? i : pairy[i];
           increment = qay/p_h.getMa(co2[branchco2])*co2[branchco2];
        r.accumulate(lfsv_co2,      i, +increment );
      }

      // Ca and CO_2 diffusion
      std::array<RF,lfsusize> Dw, Ma, pa_Da_Ma;
      for (std::size_t i=0; i<lfsusize; ++i)
      {
        Dw[i] = p_h.getdifCa(phi[i],1.-Sa[i]);
        Ma[i] = p_h.getMa(co2[i]);
        pa_Da_Ma[i] = p_h.getdifco2(phi[i],Sa[i])*(pa[i]+param.getpa0())*Ma[i]/p_h.getMco2();
      }
      for (std::size_t i=0; i<lfsusize; ++i)
      {
        r.accumulate(lfsv_Ca,i, dy/2.*(Ca[i]-Ca[pairx[i]])/dx *h2avg(Dw[i],Dw[pairx[i]],"Dw alpha_volume")
                               +dx/2.*(Ca[i]-Ca[pairy[i]])/dy *h2avg(Dw[i],Dw[pairy[i]],"Dw alpha_volume") );
        r.accumulate(lfsv_co2,i, dy/2.*(co2[i]-co2[pairx[i]])/dx *h2avg(pa_Da_Ma[i],pa_Da_Ma[pairx[i]],"pa*Da*Ma alpha_volume")
                                +dx/2.*(co2[i]-co2[pairy[i]])/dy *h2avg(pa_Da_Ma[i],pa_Da_Ma[pairy[i]],"pa*Da*Ma alpha_volume"));
      }

      // dispersion term TODO pa_Da_Ma !
      const RF& a_T = p_h.getalfat();
      const RF& a_L = p_h.getalfal();
      RF avgKwr = mojefunkcie::hNavg(Kwr,"Kwr for dispersion in alpha_volume");
      RF vwx = -avgKwr* (pw[3]-pw[2]+pw[1]-pw[0])/2./dx/param.getrho_w();
      RF vwy = -avgKwr*((pw[3]-pw[1]+pw[2]-pw[0])/2./dy/param.getrho_w()-param.getgrav());
      RF vwsqrt = std::max(1e-5, sqrt(vwx*vwx+vwy*vwy));
      RF sat{0};
      for (const auto& v : Sa)
        sat+=v;
      sat/=static_cast<RF>(lfsusize);
      RF Dw11 = (a_L*vwx*vwx+a_T*vwy*vwy)/vwsqrt/((1.-sat)*(1-sat));
      RF Dw22 = (a_T*vwx*vwx+a_L*vwy*vwy)/vwsqrt/((1.-sat)*(1-sat));
      RF Dw12 = (a_L-a_T)*vwx*vwy/vwsqrt/((1.-sat)*(1-sat));
      const RF Caderx = (Ca[3]-Ca[2]+Ca[1]-Ca[0])/dx/2.;
      const RF Cadery = (Ca[3]-Ca[1]+Ca[2]-Ca[0])/dy/2.;

      for (int i=0; i<int(lfsusize); ++i)
        {
          // (i-pair[]) is used to determine signum of unit normal vector, i must be int!
          r.accumulate(lfsv_Ca,i,
                dy/2.*(Dw11*(Ca[i]-Ca[pairx[i]])/dx+Dw12*Cadery*(i-pairx[i])  )
               +dx/2.*(Dw22*(Ca[i]-Ca[pairy[i]])/dy+Dw12*Caderx*(i-pairy[i])/2) );
        }

      // RF avgKac = mojefunkcie::hNavg(Kac,"Karc for dispersion in alpha_volume");
      // std::array<RF,lfsusize> rhoa;
      // for (size_t j=0; j<lfsusize; ++j)
      //   rhoa[j] = param.getrho_a(pa[j],co2[j]);
      // RF arhoa = mojefunkcie::aritavg(rhoa);
      // RF vax = -avgKac* (pa[3]-pa[2]+pa[1]-pa[0])/2./dx;
      // RF vay = -avgKac*((pa[3]-pa[1]+pa[2]-pa[0])/2./dy-param.getgrav()*arhoa);
      // RF vasqrt = std::max(1e-5, sqrt(vax*vax+vay*vay));
      // RF Da11 = (a_L*vax*vax+a_T*vay*vay)/vasqrt*sat*sat;
      // RF Da22 = (a_T*vax*vax+a_L*vay*vay)/vasqrt*sat*sat;
      // RF Da12 = (a_L-a_T)*vax*vay/vasqrt*sat*sat;
      // RF avgpap = mojefunkcie::aritavg(pa)+param.getpa0();
      // RF avgMa = mojefunkcie::aritavg(Ma)/p_h.getMco2();
      // RF pM = avgpap*avgMa;
      // Da11 *= pM;
      // Da22 *= pM;
      // Da12 *= pM;

      // const RF co2derx = (co2[3]-co2[2]+co2[1]-co2[0])/dx/2.;
      // const RF co2dery = (co2[3]-co2[1]+co2[2]-co2[0])/dy/2.;

      // for (int i=0; i<int(lfsusize); ++i)
      //   {
      //     // (i-pair[]) is used to determine signum of unit normal vector, i must be int!
      //     r.accumulate(lfsv_co2,i,
      //           dy/2.*(Da11*(co2[i]-co2[pairx[i]])/dx+Da12*co2dery*(i-pairx[i])  )
      //          +dx/2.*(Da22*(co2[i]-co2[pairy[i]])/dy+Da12*co2derx*(i-pairy[i])/2) );
      //   }