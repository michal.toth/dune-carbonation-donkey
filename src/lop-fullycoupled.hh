/* A fully coupled system for solving two phase flow in porous media,
 * contaminant transport, chemical reactions, and changing porosity.
 * Primary variables for the twophase flow are water pressure and air
 * saturation. Van Genuchten-Mualem model is used for water retention
 * curve and hydraulic permeablity curve.
 * Contaminants used are Ca ions dissolved in water and CO_2 in air.
 * Ca in terms of molar concentration and CO_2 in terms of partial
 * pressure (using ideal gas law).
 * Chemical reactions considered are carbonation ad leaching.
 * Carbonation is a reaction of dissolved Ca ions with dissolved CO_2
 * (CO_3 ions) forming Calcite CaCO_3. Since reaction is instaneous,
 * concentration of CO_2 in water is not modelled. Carbonation takes
 * place only in region with both phases present. It reduces porosty.
 * Leaching is a dissolution of hydrated products inside concrete
 * (not Calcite). Depends on the amount of these minerals (CaStored)
 * and on water pH (Ca ions content). Leaching increases porosity and
 * the variable CaStored has same unit (dimensionless)= part of total
 * volume. Various types of minerals are leached at different
 * concentrations, so it gives different volumes of leached material,
 * different Ca concentrations and amount of water into the system.
 */

#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/geometry/type.hh>

#include<dune/geometry/referenceelements.hh>
#include<dune/pdelab/common/quadraturerules.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/finiteelement/localbasiscache.hh>

/** a local operator for solving contaminant transport and
 *    chemical reactions in porous medium using vertex-centered
 *    finite volume method. Elements are either triangles or
 *    axiparallel rectangles, or mixed (not tested).
 *  Variables are Calcium molar concentration in water Ca, and CO_2 concentration (molar fraction) in air p_{\bar{c}}.
 *  \phi is porosity, lower indices _w, _a stand for water and air, S_* is saturation,
 *  \vec{q_*} is flow, p_a pressure, \rho_* density, D_* dispersion+diffusion coeff. (flow-dep., no dispersion yet),
 *  \sigma(S_w) is speed of carbonation, k_{s,l} speed of leaching/decalcification, and
 *  C_{aS} is amount of dissolved Ca based on its concentration. Others are constants.
 *
 * \f{align*}{
 *   \partial_t(\phi S_w Ca) + \mathrm{div}[ \frac{\vec{q_w}}{\rho_w} Ca - D_w\nabla Ca ]
 *       &=& -\sigma(S_w)\phi S_w Ca p_{\bar{c}} + k_{s,l}\frac{\mathrm{d}C_{aS}(Ca)}{\mathrm{d}t} \in \Omega,\\
 *   \partial_t(\phi S_a \rho_{\bar{c}}) + \mathrm{div}[ \frac{\vec{q_a}}{\rho_a}\rho_{\bar{c}}
 *       -D_a\frac{M_{\bar{c}}}{RT}p_a\nabla\left(\frac{p_{\bar{c}}}{p_a}\right) ]
 *       &=& -\sigma(S_w)\phi S_w M_{\bar{c}} Ca p_{\bar{c}} \in \Omega,\\
 *                 \vec{q}_{Ca} \cdot \nu &=& 0             \ x=0     \\
 *                 \vec{q}_{co2}\cdot \nu &=& 0             \ x=0     \\
 *                       p_{co2}\cdot \nu &=& 0.5(pa+pa0)   \ x=MAX_x \\ %Dirichlet, accelerated conditions, high CO_2 pressure
 *                     \nabla Ca\cdot \nu &=& 0             \ x=Max_x \\ %free outflow, convection applies - can be flushed out
 // TODO: boundary conditions and initial conditions
 // for now I use temporary:
 *                 \vec{q}_{Ca} \cdot \nu &=& 0       \ y=0     \\
 *                 \vec{q}_{co2}\cdot \nu &=& 0       \ y=0     \\
 *                 \vec{q}_{Ca} \cdot \nu &=& 0       \ y=MAX_y \\
 *                 \vec{q}_{co2}\cdot \nu &=& 0       \ y=MAX_y \\
 *
 * \f}
 *
 */
template<typename Param_chem, typename GFS, typename UOLD>
// UOLD stores values from previous time step, only Ca is used
class LOP_spatial_fullycoupled
  : public Dune::PDELab::FullVolumePattern
  , public Dune::PDELab::FullSkeletonPattern
  , public Dune::PDELab::LocalOperatorDefaultFlags
  , public Dune::PDELab::NumericalJacobianVolume<LOP_spatial_fullycoupled<Param_chem,GFS,UOLD> >
  , public Dune::PDELab::NumericalJacobianBoundary<LOP_spatial_fullycoupled<Param_chem,GFS,UOLD> >
  , public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
  Param_chem& p_h;
  typename Param_chem::Param_wflow& param;
  // using RF = typename Param_chem::value_type;
  // using DF = RF;
  typedef Dune::PDELab::LocalFunctionSpace<GFS> LFS;
  typedef Dune::PDELab::LFSIndexCache<LFS> LFSCache;
  typedef typename LFS::template Child<0>::Type::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType RangeTypeC;
  typedef typename LFS::template Child<0>::Type::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
  typedef typename LFS::template Child<0>::Type::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeFieldType RF;
  CachedParam<GFS,UOLD,RF> data; // stored values from previous time step

public:
  // pattern assembly flags
  enum { doPatternVolume = true };
  //  enum { doPatternSkeleton = true }; // skeleton terms "faked" in alpha_volume

  // residual assembly flags
  //  enum { doLambdaBoundary = true };// side boundaries in alpha
  enum { doAlphaVolume = true };
  enum { doAlphaBoundary  = true };// boundaries

  //! constructor stores a copy of the parameter object
  LOP_spatial_fullycoupled (Param_chem&  param_chem_, const GFS& gfs_, UOLD& uold_)
    :p_h(param_chem_),
     param(param_chem_.param),
     data(gfs_,uold_)
  {}

  void setTime(double t)
  {
    param.setTime(t);
  }

  void setTimeStep(double dt)
  {
    param.setTimeStep(dt);
  }

  void adjustTimeStep(double coef)
  {
    param.adjustTimeStep(coef);
  }

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X,
           typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x,
                     const LFSV& lfsv, R& r) const
  {
    using mojefunkcie::h2avg;
    using mojefunkcie::hNavg;
    // get access to each vector space
    assert(LFSU::CHILDREN==6);
    using namespace Dune::TypeTree::Indices;
    const auto& lfsu_pw = lfsu.child(_0);
    const auto& lfsu_Sa = lfsu.child(_1);
    const auto& lfsu_Ca = lfsu.child(_2);
    const auto& lfsu_co2 = lfsu.child(_3);
    const auto& lfsu_phi = lfsu.child(_4);
    // const auto& lfsu_CaStored = lfsu.child(_5);
    const auto& lfsv_pw = lfsv.child(_0);
    const auto& lfsv_Sa = lfsv.child(_1);
    const auto& lfsv_Ca = lfsv.child(_2);
    const auto& lfsv_co2 = lfsv.child(_3);
    const auto& lfsv_phi = lfsv.child(_4);
    const auto& lfsv_CaStored = lfsv.child(_5);
    // typedef decltype(makeZeroBasisFieldValue(lfsu_pw)) RFU;
    const std::size_t& lfsusize = lfsv_pw.size();
    const auto& inside_cell = eg.entity();
    std::vector<RF> pw(lfsusize);
    std::vector<RF> Sa(lfsusize);
    std::vector<RF> pa(lfsusize);
    std::vector<RF> Ca(lfsusize);
    std::vector<RF> co2(lfsusize);
    std::vector<RF> phi(lfsusize);
    // std::vector<RF> CaStored(lfsusize);
    std::vector<RF> cl=data.read(inside_cell);
    std::vector<RF> Caold(lfsusize);
    for (std::size_t i=0; i<lfsusize; ++i)
    {
      pw[i] = x(lfsu_pw,i);
      Sa[i] = x(lfsu_Sa,i);
      Ca[i] = x(lfsu_Ca,i);
      co2[i] = x(lfsu_co2,i);
      phi[i] = x(lfsu_phi,i);
      pa[i] = pw[i] + param.pc(Sa[i]);
      Caold[i] = cl[lfsusize*2+i];
      // Caold[i] = std::max(Ca[i],cl[lfsusize*2+i]); // avoid using std::min(0., ...) in accumulate
    }
    const auto& volume = eg.geometry().volume();

    // // nonnegative variables for reaction part
    // std::vector<RF> Capos(lfsusize);
    // std::vector<RF> co2pos(lfsusize);
    // for(std::size_t i=0; i<lfsusize; ++i)
    // {
    //   Capos[i]    = std::max(0.,Ca[i] );
    //   co2pos[i]   = std::max(0.,co2[i]);
    // }

    // source terms, carbonation and leaching
    for (std::size_t i=0; i<lfsusize; ++i)
    {
      // RF carbonation = std::min(p_h.getcarbspeed(1.-Sa[i])*phi[i]*(1.-Sa[i])*Ca[i]*co2[i]*(pa[i]+param.getpa0()),std::min(co2[i],Ca[i]))*volume/RF(lfsusize);
      RF carbonation = p_h.getcarbspeed(1.-Sa[i])*phi[i]*(1.-Sa[i])*Ca[i]*co2[i]*(pa[i]+param.getpa0())*volume/RF(lfsusize);
      r.accumulate(lfsv_pw,i,carbonation*p_h.getMCaO() ); // a lot faster with this, even though change to water is negligible
      r.accumulate(lfsv_Sa,i,carbonation*p_h.getmolco2() );
      r.accumulate(lfsv_Ca,i,carbonation );
      r.accumulate(lfsv_co2,i,carbonation*param.getR()*param.getT() );
      r.accumulate(lfsv_phi,i,carbonation*p_h.getV_CaCO_3() );
      // Ca uses molar concentration, does not need any weights
      // CO_2 uses concentration, the equation was multiplied by RT/M_{\bar{c}}, (concentration*pressure yield partial CO2 pressure)
      // porosity uses volume fraction (dimensionless), needs Calcite volume

      // if (Ca[i] < Caold[i]) // leaching is one-way only, deposition not possible
      // {
        RF ksl = p_h.getleachspeed(1.-Sa[i],Ca[i])/param.getTimeStep()*volume/RF(lfsusize);  // currently describes k_{sl} and does not depend on Ca
        r.accumulate(lfsv_pw,i, ksl*(p_h.getwls(Ca[i])-p_h.getwls(Caold[i]))*p_h.getMh2o() ); // released water from hydrated products
        r.accumulate(lfsv_pw,i, ksl*(p_h.getCaC(Ca[i])-p_h.getCaC(Caold[i]))*p_h.getMCaO() ); // released CaO into water - a tad faster without it
        r.accumulate(lfsv_Ca,i, ksl*(p_h.getCaC(Ca[i])-p_h.getCaC(Caold[i])) );
        r.accumulate(lfsv_phi,i, ksl*(p_h.getVdt(Ca[i])-p_h.getVdt(Caold[i])) );
        r.accumulate(lfsv_CaStored,i,-ksl*(p_h.getVdt(Ca[i])-p_h.getVdt(Caold[i])) );
      // }
    }


    // skeleton terms, convection and diffusion (and later maybe dispersion) of water, air, Ca, and CO_2
    if (lfsusize==4)
    {
      #include "lop_fullycoupled_alphavolume_lfsusize4part.hh"
    } // end lfsusize==4
    else if (lfsusize == 3)
    {
      throw std::string("not finished alphavolume lfsusize==3");
    } //end lfsusize==3
    else
      throw std::string("wrong lfsusize");
  } // end alpha_volume

  template<typename EG, typename LFSU, typename X,
           typename LFSV, typename MAT>
  void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x,
                     const LFSV& lfsv, MAT& mat) const
  {
    using mojefunkcie::h2avg;
    using mojefunkcie::h2avg_deriv;
    using mojefunkcie::hNavg;
    // get access to each vector space
    assert(LFSU::CHILDREN==6);
    using namespace Dune::TypeTree::Indices;
    const auto& lfsu_pw = lfsu.child(_0);
    const auto& lfsu_Sa = lfsu.child(_1);
    const auto& lfsu_Ca = lfsu.child(_2);
    const auto& lfsu_co2 = lfsu.child(_3);
    const auto& lfsu_phi = lfsu.child(_4);
    // const auto& lfsu_CaStored = lfsu.child(_5);
    const auto& lfsv_pw = lfsv.child(_0);
    const auto& lfsv_Sa = lfsv.child(_1);
    const auto& lfsv_Ca = lfsv.child(_2);
    const auto& lfsv_co2 = lfsv.child(_3);
    const auto& lfsv_phi = lfsv.child(_4);
    const auto& lfsv_CaStored = lfsv.child(_5);
    // typedef decltype(makeZeroBasisFieldValue(lfsu_pw)) RFU;
    const std::size_t& lfsusize = lfsv_pw.size();
    // auto fillvector = [&](decltype(lfsu_pw) lfsuch){
    //   std::vector<RF> v;
    //   for (std::size_t i=0; i<lfsusize; ++i)
    //   {
    //     v[i] = x(lfsuch,i);
    //   }
    //   return v;};
    std::vector<RF> pw(lfsusize);
    std::vector<RF> Sa(lfsusize);
    std::vector<RF> pa(lfsusize);
    std::vector<RF> Ca(lfsusize);
    std::vector<RF> co2(lfsusize);
    std::vector<RF> phi(lfsusize);
    std::vector<RF> CaStored(lfsusize);
    std::vector<RF> dpc(lfsusize);
    const auto& inside_cell = eg.entity();
    std::vector<RF> cl=data.read(inside_cell);
    std::vector<RF> Caold(lfsusize);
    for (std::size_t i=0; i<lfsusize; ++i)
    {
      pw[i]       = x(lfsu_pw,i);
      Sa[i]       = x(lfsu_Sa,i);
      Ca[i]       = x(lfsu_Ca,i);
      co2[i]      = x(lfsu_co2,i);
      phi[i]      = x(lfsu_phi,i);
      pa[i]       = pw[i] + param.pc(Sa[i]);
      dpc[i]      = param.pc_deriv(Sa[i]);
      Caold[i]    = cl[lfsusize*2+i];
    }
    const auto& volume = eg.geometry().volume();
    // source terms, carbonation and leaching
    for (std::size_t i=0; i<lfsusize; ++i)
      {
        // RF carbonation = std::min(p_h.getcarbspeed(1.-Sa[i])*phi[i]*(1.-Sa[i])*Ca[i]*co2[i]*(pa[i]+param.getpa0())*volume/RF(lfsusize),std::min(co2[i],Ca[i]));
        // // RF carbonation = p_h.getcarbspeed(1.-Sa[i])*phi[i]*(1.-Sa[i])*Ca[i]*co2[i]*(pa[i]+param.getpa0())*volume/RF(lfsusize);
        // r.accumulate(lfsv_pw,i,carbonation*p_h.getMCaO() ); // a lot faster with this, even though change to water is negligible
        // r.accumulate(lfsv_Sa,i,carbonation*p_h.getmolco2() );
        // r.accumulate(lfsv_Ca,i,carbonation);
        // r.accumulate(lfsv_co2,i,carbonation*param.getR()*param.getT());
        // r.accumulate(lfsv_phi,i,carbonation*p_h.getV_CaCO_3());

        // if (p_h.getcarbspeed(1.-Sa[i])*phi[i]*(1.-Sa[i])*Ca[i]*co2[i]*(pa[i]+param.getpa0())<std::min(co2[i],Ca[i]))
        // if (p_h.getcarbspeed(1.-Sa[i])*phi[i]*(1.-Sa[i])*Ca[i]*co2[i]*(pa[i]+param.getpa0())<=std::min(co2[i],Ca[i]))
        if (true) // no "complementarity condition"
        {
          RF carbspeed = p_h.getcarbspeed(1.-Sa[i])*volume/RF(lfsusize);
          RF carbonation_dpw = carbspeed*phi[i]*(1.-Sa[i])*Ca[i]*co2[i];
          mat.accumulate(lfsv_pw,i,lfsu_pw,i,carbonation_dpw*p_h.getMCaO());
          mat.accumulate(lfsv_Sa,i,lfsu_pw,i,carbonation_dpw*p_h.getmolco2());
          mat.accumulate(lfsv_Ca,i,lfsu_pw,i,carbonation_dpw);
          mat.accumulate(lfsv_co2,i,lfsu_pw,i,carbonation_dpw*param.getR()*param.getT());
          mat.accumulate(lfsv_phi,i,lfsu_pw,i,carbonation_dpw*p_h.getV_CaCO_3());

          RF carbonation_dSa = ( p_h.getcarbspeed_deriv(1.-Sa[i])*(-1.)*(1.-Sa[i])*(pa[i]+param.getpa0())*volume/RF(lfsusize)
                                 +carbspeed                            *(  -1.   )*(pa[i]+param.getpa0())
                                 +carbspeed                            *(1.-Sa[i])* dpc[i]                )
                                *phi[i]*Ca[i]*co2[i];
          mat.accumulate(lfsv_pw,i,lfsu_Sa,i,carbonation_dSa*p_h.getMCaO());
          mat.accumulate(lfsv_Sa,i,lfsu_Sa,i,carbonation_dSa*p_h.getmolco2());
          mat.accumulate(lfsv_Ca,i,lfsu_Sa,i,carbonation_dSa);
          mat.accumulate(lfsv_co2,i,lfsu_Sa,i,carbonation_dSa*param.getR()*param.getT());
          mat.accumulate(lfsv_phi,i,lfsu_Sa,i,carbonation_dSa*p_h.getV_CaCO_3());

          carbspeed *= (1.-Sa[i])*(pa[i]+param.getpa0());
          RF carbonation_dCa = carbspeed*phi[i]*co2[i];
          mat.accumulate(lfsv_pw,i,lfsu_Ca,i,carbonation_dCa*p_h.getMCaO());
          mat.accumulate(lfsv_Sa,i,lfsu_Ca,i,carbonation_dCa*p_h.getmolco2());
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,i,carbonation_dCa);
          mat.accumulate(lfsv_co2,i,lfsu_Ca,i,carbonation_dCa*param.getR()*param.getT());
          mat.accumulate(lfsv_phi,i,lfsu_Ca,i,carbonation_dCa*p_h.getV_CaCO_3());

          carbspeed *= Ca[i];
          RF carbonation_dco2 = carbspeed*phi[i];
          mat.accumulate(lfsv_pw,i,lfsu_co2,i,carbonation_dco2*p_h.getMCaO());
          mat.accumulate(lfsv_Sa,i,lfsu_co2,i,carbonation_dco2*p_h.getmolco2());
          mat.accumulate(lfsv_Ca,i,lfsu_co2,i,carbonation_dco2);
          mat.accumulate(lfsv_co2,i,lfsu_co2,i,carbonation_dco2*param.getR()*param.getT());
          mat.accumulate(lfsv_phi,i,lfsu_co2,i,carbonation_dco2*p_h.getV_CaCO_3());

          carbspeed *= co2[i]; // ==carbonation_dphi
          mat.accumulate(lfsv_pw,i,lfsu_phi,i,carbspeed*p_h.getMCaO());
          mat.accumulate(lfsv_Sa,i,lfsu_phi,i,carbspeed*p_h.getmolco2());
          mat.accumulate(lfsv_Ca,i,lfsu_phi,i,carbspeed);
          mat.accumulate(lfsv_co2,i,lfsu_phi,i,carbspeed*param.getR()*param.getT());
          mat.accumulate(lfsv_phi,i,lfsu_phi,i,carbspeed*p_h.getV_CaCO_3());
          // it may be easier to just divide "carbonation" by deriver, but zero_division must be avoided
          ++(p_h.branchinfo[0]);
        }
        // else if (Ca[i]<co2[i]) // & Ca[i] < carbonation
        else if (Ca[i]<=co2[i]) // & Ca[i] < carbonation term
        {
          mat.accumulate(lfsv_pw,i,lfsu_Ca,i,volume/RF(lfsusize)*p_h.getMCaO());
          mat.accumulate(lfsv_Sa,i,lfsu_Ca,i,volume/RF(lfsusize)*p_h.getmolco2());
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,i,volume/RF(lfsusize) );
          mat.accumulate(lfsv_co2,i,lfsu_Ca,i,volume/RF(lfsusize)*param.getR()*param.getT());
          mat.accumulate(lfsv_phi,i,lfsu_Ca,i,volume/RF(lfsusize)*p_h.getV_CaCO_3());
          ++(p_h.branchinfo[1]);
        }
        else // if (co2[i]<Ca[i]) // & co2 partial pressure < carbonation term
        {
          // d_co2
          mat.accumulate(lfsv_pw,i,lfsu_co2,i,volume/RF(lfsusize)*p_h.getMCaO());
          mat.accumulate(lfsv_Sa,i,lfsu_co2,i,volume/RF(lfsusize)*p_h.getmolco2());
          mat.accumulate(lfsv_Ca,i,lfsu_co2,i,volume/RF(lfsusize) );
          mat.accumulate(lfsv_co2,i,lfsu_co2,i,volume/RF(lfsusize)*param.getR()*param.getT());
          mat.accumulate(lfsv_phi,i,lfsu_co2,i,volume/RF(lfsusize)*p_h.getV_CaCO_3());
          // d_pw
          // mat.accumulate(lfsv_pw,i,lfsu_pw,i,co2[i]*volume/RF(lfsusize)*p_h.getMCaO());
          // mat.accumulate(lfsv_Sa,i,lfsu_pw,i,co2[i]*volume/RF(lfsusize)*p_h.getmolco2());
          // mat.accumulate(lfsv_Ca,i,lfsu_pw,i,co2[i]*volume/RF(lfsusize) );
          // mat.accumulate(lfsv_co2,i,lfsu_pw,i,co2[i]*volume/RF(lfsusize)*param.getR()*param.getT());
          // mat.accumulate(lfsv_phi,i,lfsu_pw,i,co2[i]*volume/RF(lfsusize)*p_h.getV_CaCO_3());
          // d_Sa
          // mat.accumulate(lfsv_pw,i,lfsu_Sa,i,co2[i]*dpc[i]*volume/RF(lfsusize)*p_h.getMCaO());
          // mat.accumulate(lfsv_Sa,i,lfsu_Sa,i,co2[i]*dpc[i]*volume/RF(lfsusize)*p_h.getmolco2());
          // mat.accumulate(lfsv_Ca,i,lfsu_Sa,i,co2[i]*dpc[i]*volume/RF(lfsusize) );
          // mat.accumulate(lfsv_co2,i,lfsu_Sa,i,co2[i]*dpc[i]*volume/RF(lfsusize)*param.getR()*param.getT());
          // mat.accumulate(lfsv_phi,i,lfsu_Sa,i,co2[i]*dpc[i]*volume/RF(lfsusize)*p_h.getV_CaCO_3());
          ++(p_h.branchinfo[2]);
        }


        // if (Ca[i] < Caold[i]) // leaching is one-way only, deposition not possible
        // {
          // r.accumulate(lfsv_pw,i, ksl*(p_h.getwls(Capos[i])-p_h.getwls(Caold[i]))*p_h.getMh2o() );
          // r.accumulate(lfsv_pw,i, ksl*(p_h.getCaC(Capos[i])-p_h.getCaC(Caold[i]))*p_h.getMCaO() );
          // r.accumulate(lfsv_Ca,i, ksl*(p_h.getCaC(Capos[i])-p_h.getCaC(Caold[i])) );
          // r.accumulate(lfsv_phi,i, ksl*(p_h.getVdt(Capos[i])-p_h.getVdt(Caold[i])) );
          // r.accumulate(lfsv_CaStored,i,-ksl*(p_h.getVdt(Capos[i])-p_h.getVdt(Caold[i])) );
          RF ksl = p_h.getleachspeed(1.-Sa[i],Ca[i])/param.getTimeStep()*volume/RF(lfsusize);
          // RF ksl = p_h.getleachspeed(1.-Sa[i],Capos[i])/param.getTimeStep()*volume/RF(lfsusize);
          mat.accumulate(lfsv_pw,i,lfsu_Ca,i, ksl*p_h.dwls_dCa(Ca[i])*p_h.getMh2o() );
          mat.accumulate(lfsv_pw,i,lfsu_Ca,i, ksl*p_h.dCaC_dCa(Ca[i])*p_h.getMCaO() );
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,i, ksl*p_h.dCaC_dCa(Ca[i]) );
          mat.accumulate(lfsv_phi,i,lfsu_Ca,i, ksl*p_h.dVdt_dCa(Ca[i]) );
          mat.accumulate(lfsv_CaStored,i,lfsu_Ca,i,-ksl*p_h.dVdt_dCa(Ca[i]) );
          RF ksl_dSa = p_h.dleachspeed_dSw(1.-Sa[i],Ca[i])*(-1.)/param.getTimeStep()*volume/RF(lfsusize);
          // RF ksl_dSa = p_h.dleachspeed_dSw(1.-Sa[i],Capos[i])*(-1.)/param.getTimeStep()*volume/RF(lfsusize);
          mat.accumulate(lfsv_pw,i,lfsu_Sa,i, ksl_dSa*(p_h.getwls(Ca[i])-p_h.getwls(Caold[i]))*p_h.getMh2o() );
          mat.accumulate(lfsv_pw,i,lfsu_Sa,i, ksl_dSa*(p_h.getCaC(Ca[i])-p_h.getCaC(Caold[i]))*p_h.getMCaO() );
          mat.accumulate(lfsv_Ca,i,lfsu_Sa,i, ksl_dSa*(p_h.getCaC(Ca[i])-p_h.getCaC(Caold[i])) );
          mat.accumulate(lfsv_phi,i,lfsu_Sa,i, ksl_dSa*(p_h.getVdt(Ca[i])-p_h.getVdt(Caold[i])) );
          mat.accumulate(lfsv_CaStored,i,lfsu_Sa,i,-ksl_dSa*(p_h.getVdt(Ca[i])-p_h.getVdt(Caold[i])) );
          RF ksl_dCa = p_h.dleachspeed_dCa(1.-Sa[i],Ca[i])/param.getTimeStep()*volume/RF(lfsusize);
          // RF ksl_dCa = p_h.dleachspeed_dCa(1.-Sa[i],Capos[i])/param.getTimeStep()*volume/RF(lfsusize);
          mat.accumulate(lfsv_pw,i,lfsu_Ca,i, ksl_dCa*(p_h.getwls(Ca[i])-p_h.getwls(Caold[i]))*p_h.getMh2o() );
          mat.accumulate(lfsv_pw,i,lfsu_Ca,i, ksl_dCa*(p_h.getCaC(Ca[i])-p_h.getCaC(Caold[i]))*p_h.getMCaO() );
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,i, ksl_dCa*(p_h.getCaC(Ca[i])-p_h.getCaC(Caold[i])) );
          mat.accumulate(lfsv_phi,i,lfsu_Ca,i, ksl_dCa*(p_h.getVdt(Ca[i])-p_h.getVdt(Caold[i])) );
          mat.accumulate(lfsv_CaStored,i,lfsu_Ca,i,-ksl_dCa*(p_h.getVdt(Ca[i])-p_h.getVdt(Caold[i])) );
        // }
      }

    // skeleton terms, convection and diffusion (and later maybe dispersion) of water, air, Ca, and CO_2
    if (lfsusize==4)
    {
      #include "lop_fullycoupled_jacobianvolume_lfsusize4part.hh"
    } // end lfsusize==4
    else if (lfsusize == 3)
    {
      throw std::string("not finished jacobianvolume lfsusize==3");
    } //end lfsusize==3
    else
      throw std::string("wrong lfsusize");
  } //end jacobian_volume */


  template<typename IG, typename LFSU, typename X,
           typename LFSV, typename R>
  void alpha_boundary (const IG& ig,
                       const LFSU& lfsu_i, const X& x_i,
                       const LFSV& lfsv_i, R& r_i) const
  {
    using mojefunkcie::h2avg;
    //const auto& geo = ig.inside().geometry(); // inside Entity
    using namespace Dune::TypeTree::Indices;
    const auto& lfsu_pw = lfsu_i.child(_0);
    const auto& lfsu_Sa = lfsu_i.child(_1);
    const auto& lfsu_Ca = lfsu_i.child(_2);
    const auto& lfsu_co2 = lfsu_i.child(_3);
    const auto& lfsu_phi = lfsu_i.child(_4);
    // const auto& lfsu_CaStored = lfsu_i.child(_5);
    //assert(LFSV::CHILDREN==4);
    const auto& lfsv_pw = lfsv_i.child(_0);
    const auto& lfsv_Sa = lfsv_i.child(_1);
    const auto& lfsv_Ca = lfsv_i.child(_2);
    const auto& lfsv_co2 = lfsv_i.child(_3);
    // const auto& lfsv_phi = lfsv_i.child(_4);
    // const auto& lfsv_CaStored = lfsv_i.child(_5);
    const std::size_t& lfsusize = lfsv_pw.size();
    // do not forget to divide by 2, I integrate only on half of face (or less in higher dim)
    const auto& face_volume = ig.geometry().volume();

    const auto& global = ig.geometry().center();
    // auto fillvector = [&](decltype(lfsu_pw) lfsuch){
    //   std::vector<RF> v;
    //   for (std::size_t i=0; i<lfsusize; ++i)
    //   {
    //     v[i] = x_i(lfsuch,i);
    //   }
    //   return v;};
    std::vector<RF> pw(lfsusize);
    std::vector<RF> Sa(lfsusize);
    std::vector<RF> Ca(lfsusize);
    std::vector<RF> co2(lfsusize);
    std::vector<RF> pa(lfsusize);
    std::vector<RF> phi(lfsusize);
    for (std::size_t i=0; i<lfsusize; ++i)
    {
      pw[i] = x_i(lfsu_pw,i);
      Sa[i] = x_i(lfsu_Sa,i);
      Ca[i] = x_i(lfsu_Ca,i);
      co2[i] = x_i(lfsu_co2,i);
      phi[i] = x_i(lfsu_phi,i);
      pa[i] = pw[i]+param.pc(Sa[i]);
    }

    if (lfsusize==4)
    {
      //right boundary  fixed CO_2 pressure = 0.0004(pa+pa0)
      //                Ca fixed concentration 0
      //         consider    Ca can be flushed out with water, otherwise insulated
      if (global[0]>=param.getwidth()-1e-7)
      {
        // water and air flow have dirichlet coditions

        // RF idx = 1./(ig.inside().geometry().corner(1)[0]-ig.inside().geometry().corner(0)[0]);
        // const RF& dy = face_volume;
        // right boundary condition
        // dirichlet condition for CO_2, not specified here
        // free outflow Ca

        // // convection term
        // RF bfloa = -param.Kar(cl[1+lfsusize],pa[1],porosity[1])*(param.ga_nitsche(ig,x_i)-pa[0])*idx;
        // if (bfloa<0) // inflow, inflowing air has outside CO_2 partial_pressure/density
        //   {
        //     const RF& pco2 = p_h.getrhoco2(p_h.getairco2()*param.getpa0());
        //     r_i.accumulate(lfsv_Sa,1,face_volume/2*bfloa/param.getrho_a(pa[1])*pco2);
        //   }
        // else // outflow, outflowing air has boundary CO_2 concentration/mass
        //   {
        //     r_i.accumulate(lfsv_Sa,1,face_volume/2*bfloa/param.getrho_a(pa[1])*co2[1]);
        //   }
        // bfloa = -param.Kar(cl[3+lfsusize],pa[3],porosity[3])*(param.ga_nitsche(ig,x_i)-pa[3])*idx;
        // if (bfloa<0) // inflow, inflowing air has outside CO_2 partial_pressure/density
        //   {
        //     const RF& pco2 = p_h.getrhoco2(p_h.getairco2()*param.getpa0());
        //     r_i.accumulate(lfsv_Sa,3,face_volume/2*bfloa/param.getrho_a(pa[3])*pco2);
        //   }
        // else // outflow, outflowing air has boundary CO_2 concentration/mass
        //   {
        //     r_i.accumulate(lfsv_Sa,3,face_volume/2*bfloa/param.getrho_a(pa[3])*co2[3]);
        //   }

        // // Ca insulated, convection may flush it out
        // RF idx = 1./(ig.inside().geometry().corner(1)[0]-ig.inside().geometry().corner(0)[0]);
        // RF bflow1, bflow3; // consider not average permeabilities
        // bflow1 = -h2avg(param.Kwr(Sa[0],phi[0]),param.Kwr(Sa[1],phi[1]))*(pw[1]-pw[0])*idx;
        // bflow3 = -h2avg(param.Kwr(Sa[2],phi[2]),param.Kwr(Sa[3],phi[3]))*(pw[3]-pw[2])*idx;

        // if (bflow1>0) // outflow, outflowing water has boundary Ca concentration/mass
        // {
        //   r_i.accumulate(lfsv_Ca,1,face_volume/2.*bflow1/param.getrho_w()*Ca[1]);
        // }
        // if (bflow3>0) // outflow, outflowing water has boundary Ca concentration/mass
        // {
        //   r_i.accumulate(lfsv_Ca,3,face_volume/2.*bflow3/param.getrho_w()*Ca[3]);
        // }
      }
      // bottom boundary, symmetry:  \partial_y = 0  for Ca and CO_2
      // this practically removes diffusion term and accounts only for convection
      else if (global[1]<=1e-7)
      {
        // water, air \partial_y=0 zero derivation, gravity induced inflow
        r_i.accumulate(lfsv_pw,1,
               face_volume/2.*param.Kwr(Sa[1],phi[1])*(-param.getgrav()*param.getrho_w()) );
        r_i.accumulate(lfsv_pw,0,
               face_volume/2.*param.Kwr(Sa[0],phi[0])*(-param.getgrav()*param.getrho_w()) );
        // air \partial_y=0 zero derivation (only gravity induced inflow)
        r_i.accumulate(lfsv_Sa,1,
               face_volume/2.*param.Karc(Sa[1],pa[1],phi[1],co2[1])*(-param.getgrav()*p_h.getrho_a(pa[1],co2[1])) );
        r_i.accumulate(lfsv_Sa,0,
               face_volume/2.*param.Karc(Sa[0],pa[0],phi[0],co2[0])*(-param.getgrav()*p_h.getrho_a(pa[0],co2[0])) );
        // since \partial_y=0 for flow, flows are determined by gravity
        // canceled minus sign comes from unit outer normal vector
        RF bflow0 =  param.Kw(Sa[0],phi[0])*(-param.getrho_w()*param.getgrav());
        RF bflow1 =  param.Kw(Sa[1],phi[1])*(-param.getrho_w()*param.getgrav());
        // sign of bflow determines if it is outflow or inflow, \partial_y Ca=0 guarantees correctness of Ca[i] weight
        r_i.accumulate(lfsv_Ca,0,face_volume/2.*bflow0*Ca[0]);
        r_i.accumulate(lfsv_Ca,1,face_volume/2.*bflow1*Ca[1]);
        RF bfloa0 =  param.Karc(Sa[0],pa[0],phi[0],co2[0])*(-p_h.getrho_a(pa[0],co2[0])*param.getgrav())/param.rho_a_deriv();
        RF bfloa1 =  param.Karc(Sa[1],pa[1],phi[1],co2[1])*(-p_h.getrho_a(pa[1],co2[1])*param.getgrav())/param.rho_a_deriv();
        // sign of bfloa determines if it is outflow or inflow, \partial_y CO_2=0 guarantees correctness of co2[i] weight
        r_i.accumulate(lfsv_co2,0,face_volume/2.*bfloa0*co2[0]);
        r_i.accumulate(lfsv_co2,1,face_volume/2.*bfloa1*co2[1]);
      }
      // top boundary, symmetry:  \partial_y = 0  for Ca and CO_2
      else if (global[1]>=param.getheight()-1e-7)
      {
        // water, air \partial_y=0 zero derivation, gravity induced inflow
        r_i.accumulate(lfsv_pw,3,
               -face_volume/2.*param.Kwr(Sa[3],phi[3])*(-param.getgrav()*param.getrho_w()) );
        r_i.accumulate(lfsv_pw,2,
               -face_volume/2.*param.Kwr(Sa[2],phi[2])*(-param.getgrav()*param.getrho_w()) );
        // air \partial_y=0 zero derivation (only gravity induced inflow)
        r_i.accumulate(lfsv_Sa,3,
               -face_volume/2.*param.Karc(Sa[3],pa[3],phi[3],co2[3])*(-param.getgrav()*p_h.getrho_a(pa[3],co2[3])) );
        r_i.accumulate(lfsv_Sa,2,
               -face_volume/2.*param.Karc(Sa[2],pa[2],phi[2],co2[2])*(-param.getgrav()*p_h.getrho_a(pa[2],co2[2])) );

        // since \partial_y=0 for flow, flows are determined by gravity
        // canceled minus sign comes from unit outer normal vector
        RF bflow2 = -param.Kw(Sa[2],phi[2])*(-param.getrho_w()*param.getgrav());
        RF bflow3 = -param.Kw(Sa[3],phi[3])*(-param.getrho_w()*param.getgrav());
        // sign of bflow determines if it is outflow or inflow, \partial_y Ca=0 guarantees correctness of Ca[i] weight
        r_i.accumulate(lfsv_Ca,2,face_volume/2.*bflow2*Ca[2]);
        r_i.accumulate(lfsv_Ca,3,face_volume/2.*bflow3*Ca[3]);
        RF bfloa2 = -param.Karc(Sa[2],pa[2],phi[2],co2[2])*(-param.getrho_a(pa[2])*param.getgrav())/param.rho_a_deriv();
        RF bfloa3 = -param.Karc(Sa[3],pa[3],phi[3],co2[3])*(-param.getrho_a(pa[3])*param.getgrav())/param.rho_a_deriv();
        // sign of bfloa determines if it is outflow or inflow, \partial_y CO_2=0 guarantees correctness of co2[i] weight
        r_i.accumulate(lfsv_co2,2,face_volume/2.*bfloa2*co2[2]);
        r_i.accumulate(lfsv_co2,3,face_volume/2.*bfloa3*co2[3]);
      }
    }
    else // lfsusize==3
    {
      if (global[0]>=param.getwidth()-1e-7) // right boundary condition
      {
        // water, air have dirichlet conditions
        // Ca is insulated, but has free outflow with convection
        // CO_2 has dirichlet condition

        // find indices of boundary vertices
        const auto& inside_geo = ig.inside().geometry();
        // compute outflow
        std::array<Dune::FieldVector<RF,2>, 3> vertex;
        for (std::size_t i=0; i<lfsusize; ++i)
        {
          vertex[i] = inside_geo.corner(i);
        }
        auto lambda_notrightvertex = [&](std::size_t i) ->bool {return vertex[i][0]<param.getwidth()-1e-7;};
        std::array<std::size_t,2> active = mojefunkcie::setactive(lambda_notrightvertex,lfsusize);

        // dirichlet condition for CO_2, not specified here
        // free outflow Ca

        // std::array<RF,2> gradpa;
        // gradpa = param.ReconstructFlow(pa,vertex);
        // // gradpa[1]+= -param.getgrav()*param.getrho_a(apa); // I need only x direction
        // RF bfloa = -param.Kar(cl[active[0]+lfsusize],pa[active[0]],porosity[active[0]])*gradpa[1];
        // if (bfloa<0) // inflow, inflowing air has outside CO_2 partial_pressure/density
        //   {
        //     const RF& pco2 = p_h.getrhoco2(p_h.getairco2()*param.getpa0());
        //     r_i.accumulate(lfsv_Sa,active[0],face_volume/2*bfloa/param.getrho_a(pa[active[0]])*pco2);
        //   }
        // else // outflow, outflowing air has boundary CO_2 concentration/mass
        //   {
        //     r_i.accumulate(lfsv_Sa,active[0],face_volume/2*bfloa/param.getrho_a(pa[active[0]])*co2[active[0]]);
        //   }
        // bfloa = -param.Kar(cl[active[1]+lfsusize],pa[active[1]],porosity[active[1]])*gradpa[1];
        // if (bfloa<0) // inflow, inflowing air has outside CO_2 partial_pressure/density
        //   {
        //     const RF& pco2 = p_h.getrhoco2(p_h.getairco2()*param.getpa0());
        //     r_i.accumulate(lfsv_Sa,active[1],face_volume/2*bfloa/param.getrho_a(pa[active[1]])*pco2);
        //   }
        // else // outflow, outflowing air has boundary CO_2 concentration/mass
        //   {
        //     r_i.accumulate(lfsv_Sa,active[1],face_volume/2*bfloa/param.getrho_a(pa[active[1]])*co2[active[1]]);
        //   }

        // free outflow Ca, may be flushed out by convection
        std::array<RF,2> nablaw = mojefunkcie::ReconstructFlow(pw,vertex);
        std::array<RF,3> Kw;
        for (std::size_t i=0; i<lfsusize; ++i)
        {
          Kw[i] = param.Kw(Sa[i],phi[i]);
        }
        const RF avgKw = mojefunkcie::hNavg(Kw);
        const RF bflow = -avgKw*nablaw[0];
        if (bflow>0) // outflowing water contains Ca, inflowing does not -no term for bflow<0
        {
          r_i.accumulate(lfsv_Ca,active[0],face_volume/2.*bflow*Ca[active[0]]);
          r_i.accumulate(lfsv_Ca,active[1],face_volume/2.*bflow*Ca[active[1]]);
          // r_i.accumulate(lfsv_Ca,active[0],face_volume/2*(-Kw[active[0]]*nablaw[0])*Ca[active[0]]);
          // r_i.accumulate(lfsv_Ca,active[1],face_volume/2*(-Kw[active[1]]*nablaw[0])*Ca[active[1]]);
          // commented slightly improves performance, but outflow is too slow, Ca is accumulated at boundary
        }
      }
      // bottom
      else if (global[1]<=1e-7)
      {
        // find indices of boundary vertices
        const auto& inside_geo = ig.inside().geometry();
        auto lambda_notbottomvertex = [&](std::size_t i) ->bool {return inside_geo.corner(i)[1]>1e-7;};
        std::array<std::size_t,2> active = mojefunkcie::setactive(lambda_notbottomvertex,lfsusize);

        std::array<RF,2> viscmix {param.getvisc_a()/p_h.visc_mix(co2[active[0]]),param.getvisc_a()/p_h.visc_mix(co2[active[1]])};
        // water, air zero pressure derivation (gravity induced inflow)
        r_i.accumulate(lfsv_pw,active[0],
               face_volume/2*param.Kwr(Sa[active[0]],phi[active[0]])*(-param.getgrav()*param.getrho_w()) );
        r_i.accumulate(lfsv_pw,active[1],
               face_volume/2*param.Kwr(Sa[active[1]],phi[active[1]])*(-param.getgrav()*param.getrho_w()) );
        // r_i.accumulate(lfsv_Sa,active[0],
        //        face_volume/2*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*(-param.getgrav()*p_h.getrho_a(pa[active[0]],co2[active[0]])) );
        // r_i.accumulate(lfsv_Sa,active[1],
        //        face_volume/2*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*(-param.getgrav()*p_h.getrho_a(pa[active[1]],co2[active[1]])) );
        r_i.accumulate(lfsv_Sa,active[0],
               face_volume/2*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*viscmix[0]*(-param.getgrav()*p_h.getrho_a(pa[active[0]],co2[active[0]])) );
        r_i.accumulate(lfsv_Sa,active[1],
               face_volume/2*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*viscmix[1]*(-param.getgrav()*p_h.getrho_a(pa[active[1]],co2[active[1]])) );

        // since \partial_y=0 for flow, flows are determined by gravity
        // canceled minus sign comes from unit outer normal vector
        RF bflow0 =  param.Kw(Sa[active[0]],phi[active[0]])*(-param.getrho_w()*param.getgrav());
        RF bflow1 =  param.Kw(Sa[active[1]],phi[active[1]])*(-param.getrho_w()*param.getgrav());
        // sign of bflow determines if it is outflow or inflow, \partial_y Ca=0 guarantees correctness of Ca[i] weight
        r_i.accumulate(lfsv_Ca,active[0],face_volume/2.*bflow0*Ca[active[0]]);
        r_i.accumulate(lfsv_Ca,active[1],face_volume/2.*bflow1*Ca[active[1]]);
        RF bfloa0 =  param.Ka(Sa[active[0]],phi[active[0]])*viscmix[0]*(-p_h.getrho_a(pa[active[0]],co2[active[0]])*param.getgrav());
        RF bfloa1 =  param.Ka(Sa[active[1]],phi[active[1]])*viscmix[1]*(-p_h.getrho_a(pa[active[1]],co2[active[1]])*param.getgrav());
        // sign of bfloa determines if it is outflow or inflow, \partial_y CO_2=0 guarantees correctness of co2[i] weight
        r_i.accumulate(lfsv_co2,active[0],face_volume/2.*bfloa0*co2[active[0]]);
        r_i.accumulate(lfsv_co2,active[1],face_volume/2.*bfloa1*co2[active[1]]);
      }
      // top
      else if (global[1]>=param.getheight()-1e-7)
      {
        // find indices of boundary vertices
        const auto& inside_geo = ig.inside().geometry();
        auto lambda_nottopvertex = [&](std::size_t i) ->bool {return inside_geo.corner(i)[1]<param.getheight()-1e-7;};
        std::array<std::size_t,2> active = mojefunkcie::setactive(lambda_nottopvertex,lfsusize);
        std::array<RF,2> viscmix {param.getvisc_a()/p_h.visc_mix(co2[active[0]]),param.getvisc_a()/p_h.visc_mix(co2[active[1]])};

        // water, air have zero pressure derivation (gravity induced inflow)
        r_i.accumulate(lfsv_pw,active[0],
               -face_volume/2.*param.Kwr(Sa[active[0]],phi[active[0]])*(-param.getgrav()*param.getrho_w()) );
        r_i.accumulate(lfsv_pw,active[1],
               -face_volume/2.*param.Kwr(Sa[active[1]],phi[active[1]])*(-param.getgrav()*param.getrho_w()) );
        // r_i.accumulate(lfsv_Sa,active[0],
        //        -face_volume/2*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*(-param.getgrav()*p_h.getrho_a(pa[active[0]],co2[active[0]])) );
        // r_i.accumulate(lfsv_Sa,active[1],
        //        -face_volume/2*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*(-param.getgrav()*p_h.getrho_a(pa[active[1]],co2[active[1]])) );
        r_i.accumulate(lfsv_Sa,active[0],
               -face_volume/2.*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*viscmix[0]*(-param.getgrav()*p_h.getrho_a(pa[active[0]],co2[active[0]])) );
        r_i.accumulate(lfsv_Sa,active[1],
               -face_volume/2.*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*viscmix[1]*(-param.getgrav()*p_h.getrho_a(pa[active[1]],co2[active[1]])) );

        // since \partial_y=0 for flow, flows are determined by gravity
        // canceled minus sign comes from unit outer normal vector
        RF bflow0 =  param.Kw(Sa[active[0]],phi[active[0]])*(-param.getrho_w()*param.getgrav());
        RF bflow1 =  param.Kw(Sa[active[1]],phi[active[1]])*(-param.getrho_w()*param.getgrav());
        // sign of bflow determines if it is outflow or inflow, \partial_y Ca=0 guarantees correctness of Ca[i] weight
        r_i.accumulate(lfsv_Ca,active[0],face_volume/2.*bflow0*Ca[active[0]]);
        r_i.accumulate(lfsv_Ca,active[1],face_volume/2.*bflow1*Ca[active[1]]);
        RF bfloa0 =  param.Ka(Sa[active[0]],phi[active[0]])*viscmix[0]*(-p_h.getrho_a(pa[active[0]],co2[active[0]])*param.getgrav());
        RF bfloa1 =  param.Ka(Sa[active[1]],phi[active[1]])*viscmix[1]*(-p_h.getrho_a(pa[active[1]],co2[active[1]])*param.getgrav());
        // sign of bfloa determines if it is outflow or inflow, \partial_y CO_2=0 guarantees correctness of co2[i] weight
        r_i.accumulate(lfsv_co2,active[0],face_volume/2.*bfloa0*co2[active[0]]);
        r_i.accumulate(lfsv_co2,active[1],face_volume/2.*bfloa1*co2[active[1]]);
      }
    } // end lfsusize==3
  } // end alpha_boundary
/*
  template<typename IG, typename LFSU, typename X,
           typename LFSV, typename M>
  void jacobian_boundary (const IG& ig,
                          const LFSU& lfsu_i, const X& x_i,
                          const LFSV& lfsv_i, M& mat_ii) const
  {
    using mojefunkcie::h2avg;
    //const auto& geo = ig.inside().geometry(); // inside Entity
    using namespace Dune::TypeTree::Indices;
    const auto& lfsu_pw = lfsu_i.child(_0);
    const auto& lfsu_Sa = lfsu_i.child(_1);
    const auto& lfsu_Ca = lfsu_i.child(_2);
    const auto& lfsu_co2 = lfsu_i.child(_3);
    const auto& lfsu_phi = lfsu_i.child(_4);
    // const auto& lfsu_CaStored = lfsu_i.child(_5);
    //assert(LFSV::CHILDREN==4);
    const auto& lfsv_pw = lfsv_i.child(_0);
    const auto& lfsv_Sa = lfsv_i.child(_1);
    const auto& lfsv_Ca = lfsv_i.child(_2);
    const auto& lfsv_co2 = lfsv_i.child(_3);
    // const auto& lfsv_phi = lfsv_i.child(_4);
    // const auto& lfsv_CaStored = lfsv_i.child(_5);
    const std::size_t& lfsusize = lfsv_pw.size();
    // do not forget to divide by 2, I integrate only on half of face (or less in higher dim)
    const auto& face_volume = ig.geometry().volume();

    const auto& global = ig.geometry().center();
    auto fillvector = [&](decltype(lfsu_pw) lfsuch){
      std::vector<RF> v;
      for (std::size_t i=0; i<lfsusize; ++i)
      {
        v[i] = x_i(lfsuch,i);
      }

    std::vector<RF> pw{fillvector(lfsu_pw)};
    std::vector<RF> Sa{fillvector(lfsu_Sa)};
    std::vector<RF> Ca{fillvector(lfsu_Ca)};
    std::vector<RF> co2{fillvector(lfsu_co2)};
    std::vector<RF> pa(lfsusize);
    std::vector<RF> phi{fillvector(lfsu_phi)};
    for (std::size_t i=0; i<lfsusize; ++i)
    {
      pa[i] = pw[i]+param.pc(Sa[i]);
    }

    if (lfsusize==4)
    {
      throw "jacobian boundary for lfsusize==4 is not finished yet";
    }
    else // lfsusize==3
    {
      if (global[0]>=param.getwidth()-1e-7) // right boundary condition
      {
        // water, air have dirichlet conditions
        // Ca is insulated, but has free outflow with convection
        // CO_2 has dirichlet condition

        // find indices of boundary vertices
        const auto& inside_geo = ig.inside().geometry();
        // compute outflow
        std::array<Dune::FieldVector<RF,2>, 3> vertex;
        for (std::size_t i=0; i<lfsusize; ++i)
        {
          vertex[i] = inside_geo.corner(i);
        }
        auto lambda_notrightvertex = [&](std::size_t i) ->bool {return vertex[i][0]<param.getwidth()-1e-7;};
        std::array<std::size_t,2> active = mojefunkcie::setactive(lambda_notrightvertex,lfsusize);

        // free outflow Ca, may be flushed out by convection
        std::array<RF,2> nablaw = param.ReconstructFlow(pw,vertex);
        std::vector<RF> Kw(3);
        for (std::size_t i=0; i<lfsusize; ++i)
        {
          Kw[i] = param.Kw(Sa[i],phi[i]);
        }
        const RF avgKw = mojefunkcie::hNavg(Kw);
        const RF bflow = -avgKw*nablaw[0];
        if (bflow>0) // outflowing water contains Ca, inflowing does not -no term for bflow<0
        {
          // r_i.accumulate(lfsv_Ca,active[0],face_volume/2*bflow*Ca[active[0]]);
          // r_i.accumulate(lfsv_Ca,active[1],face_volume/2*bflow*Ca[active[1]]);
          mat_ii.accumulate(lfsv_Ca,active[0],lfsu_Ca,active[0],face_volume/2*bflow);
          mat_ii.accumulate(lfsv_Ca,active[1],lfsu_Ca,active[1],face_volume/2*bflow);
          std::array<std::array<RF,2>, 3> derw = param.ReconstructFlowDerived(vertex);
          for (std::size_t j=0; j<lfsusize; ++j)
          {
            // derive gradient pw
            mat_ii.accumulate(lfsv_Ca,active[0],lfsu_pw,j,face_volume/2*(-avgKw*derw[0][j])*Ca[active[0]]);
            mat_ii.accumulate(lfsv_Ca,active[1],lfsu_pw,j,face_volume/2*(-avgKw*derw[0][j])*Ca[active[1]]);
            // derive permeability by Sa
            RF davgKw_dj = avgKw*avgKw/(lfsusize*Kw[j]*Kw[j]);
            RF davgKw_dSaj = davgKw_dj*param.Kwr_deriv(Sa[j],phi[j])/param.getrho_w();
            mat_ii.accumulate(lfsv_Ca,active[0],lfsu_Sa,j,face_volume/2*(-davgKw_dSaj *nablaw[0])*Ca[active[0]]);
            mat_ii.accumulate(lfsv_Ca,active[1],lfsu_Sa,j,face_volume/2*(-davgKw_dSaj *nablaw[0])*Ca[active[1]]);
            // derive permeability by phi
            RF davgKw_dphij = davgKw_dj*param.dKwr_dphi(Sa[j],phi[j])/param.getrho_w();
            mat_ii.accumulate(lfsv_Ca,active[0],lfsu_phi,j,face_volume/2*(-davgKw_dphij*nablaw[0])*Ca[active[0]]);
            mat_ii.accumulate(lfsv_Ca,active[1],lfsu_phi,j,face_volume/2*(-davgKw_dphij*nablaw[0])*Ca[active[1]]);
          }
        }
      }
      // bottom
      else if (global[1]<=1e-7)
      {
        // find indices of boundary vertices
        const auto& inside_geo = ig.inside().geometry();
        auto lambda_notbottomvertex = [&](std::size_t i) ->bool {return inside_geo.corner(i)[1]>1e-7;};
        std::array<std::size_t,2> active = mojefunkcie::setactive(lambda_notbottomvertex,lfsusize);
        // water, air zero pressure derivation (gravity induced inflow)
        // r_i.accumulate(lfsv_pw,active[0],
        //       face_volume/2*param.Kwr(Sa[active[0]],phi[active[0]])*(-param.getgrav()*param.getrho_w()) );
        // r_i.accumulate(lfsv_pw,active[1],
        //       face_volume/2*param.Kwr(Sa[active[1]],phi[active[1]])*(-param.getgrav()*param.getrho_w()) );
        RF pom = face_volume/2*(-param.getgrav());
        mat_ii.accumulate(lfsv_pw,active[0],lfsu_Sa,active[0], pom*param.Kwr_deriv(Sa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_pw,active[1],lfsu_Sa,active[1], pom*param.Kwr_deriv(Sa[active[1]],phi[active[1]]) );
        mat_ii.accumulate(lfsv_pw,active[0],lfsu_phi,active[0], pom*param.dKwr_dphi(Sa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_pw,active[1],lfsu_phi,active[1], pom*param.dKwr_dphi(Sa[active[1]],phi[active[1]]) );
        // r_i.accumulate(lfsv_Sa,active[0],
        //        face_volume/2*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*(-param.getgrav()*p_h.getrho_a(pa[active[0]],co2[active[0]])) );
        // r_i.accumulate(lfsv_Sa,active[1],
        //        face_volume/2*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*(-param.getgrav()*p_h.getrho_a(pa[active[1]],co2[active[1]])) );
        // derive permeability by pw, Sa and phi
        mat_ii.accumulate(lfsv_Sa,active[0],lfsu_pw,active[0],pom*param.Ka(Sa[active[0]],phi[active[0]])*param.rho_a_deriv()* p_h.getrho_a(pa[active[0]],co2[active[0]]) );
        mat_ii.accumulate(lfsv_Sa,active[1],lfsu_pw,active[1],pom*param.Ka(Sa[active[1]],phi[active[1]])*param.rho_a_deriv()* p_h.getrho_a(pa[active[1]],co2[active[1]]) );
        mat_ii.accumulate(lfsv_Sa,active[0],lfsu_Sa,active[0],pom*param.Kar_deriv(Sa[active[0]],pa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_Sa,active[1],lfsu_Sa,active[1],pom*param.Kar_deriv(Sa[active[1]],pa[active[1]],phi[active[1]]) );
        mat_ii.accumulate(lfsv_Sa,active[0],lfsu_phi,active[0],pom*param.dKar_dphi(Sa[active[0]],pa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_Sa,active[1],lfsu_phi,active[1],pom*param.dKar_dphi(Sa[active[1]],pa[active[1]],phi[active[1]]) );
        // derive density from gravity term by pw and Sa
        mat_ii.accumulate(lfsv_Sa,active[0],lfsu_pw,active[0],pom*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*param.rho_a_deriv());
        mat_ii.accumulate(lfsv_Sa,active[1],lfsu_pw,active[1],pom*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*param.rho_a_deriv());
        mat_ii.accumulate(lfsv_Sa,active[0],lfsu_Sa,active[0],pom*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[0]]));
        mat_ii.accumulate(lfsv_Sa,active[1],lfsu_Sa,active[1],pom*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[1]]));

        // since \partial_y=0 for flow, flows are determined by gravity
        // canceled minus sign comes from unit outer normal vector
        RF bflow0 =  param.Kw(Sa[active[0]],phi[active[0]])*(-param.getrho_w()*param.getgrav());
        RF bflow1 =  param.Kw(Sa[active[1]],phi[active[1]])*(-param.getrho_w()*param.getgrav());
        // sign of bflow determines if it is outflow or inflow, \partial_y Ca=0 guarantees correctness of Ca[i] weight
        // r_i.accumulate(lfsv_Ca,active[0],face_volume/2*bflow0*Ca[active[0]]);
        // r_i.accumulate(lfsv_Ca,active[1],face_volume/2*bflow1*Ca[active[1]]);
        mat_ii.accumulate(lfsv_Ca,active[0],lfsu_Ca,active[0],face_volume/2*bflow0);
        mat_ii.accumulate(lfsv_Ca,active[1],lfsu_Ca,active[1],face_volume/2*bflow1);
        mat_ii.accumulate(lfsv_Ca,active[0],lfsu_pw,active[0],pom*param.Kwr_deriv(Sa[active[0]],phi[active[0]])*Ca[active[0]]);
        mat_ii.accumulate(lfsv_Ca,active[1],lfsu_pw,active[1],pom*param.Kwr_deriv(Sa[active[1]],phi[active[1]])*Ca[active[1]]);
        mat_ii.accumulate(lfsv_Ca,active[0],lfsu_pw,active[0],pom*param.dKwr_dphi(Sa[active[0]],phi[active[0]])*Ca[active[0]]);
        mat_ii.accumulate(lfsv_Ca,active[1],lfsu_pw,active[1],pom*param.dKwr_dphi(Sa[active[1]],phi[active[1]])*Ca[active[1]]);

        RF bfloa0 =  param.Ka(Sa[active[0]],phi[active[0]])*(-p_h.getrho_a(pa[active[0]],co2[active[0]])*param.getgrav());
        RF bfloa1 =  param.Ka(Sa[active[1]],phi[active[1]])*(-p_h.getrho_a(pa[active[1]],co2[active[1]])*param.getgrav());
        // sign of bfloa determines if it is outflow or inflow, \partial_y CO_2=0 guarantees correctness of co2[i] weight
        // r_i.accumulate(lfsv_co2,active[0],face_volume/2*bfloa0*co2[active[0]]);
        // r_i.accumulate(lfsv_co2,active[1],face_volume/2*bfloa1*co2[active[1]]);
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_co2,active[0],face_volume/2*bfloa0);
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_co2,active[1],face_volume/2*bfloa1);
        // derive permeability by Sa and phi
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_Sa,active[0],pom*param.Kar_deriv(Sa[active[0]],pa[active[0]],phi[active[0]])*co2[active[0]]);
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_Sa,active[1],pom*param.Kar_deriv(Sa[active[1]],pa[active[1]],phi[active[1]])*co2[active[1]]);
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_phi,active[0],pom*param.dKar_dphi(Sa[active[0]],pa[active[0]],phi[active[0]])*co2[active[0]]);
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_phi,active[1],pom*param.dKar_dphi(Sa[active[1]],pa[active[1]],phi[active[1]])*co2[active[1]]);
        // derive density from gravity term by pw and Sa
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_pw,active[0],pom*param.Ka(Sa[active[0]],phi[active[0]])*param.rho_a_deriv()*co2[active[0]]);
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_pw,active[1],pom*param.Ka(Sa[active[1]],phi[active[1]])*param.rho_a_deriv()*co2[active[1]]);
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_Sa,active[0],pom*param.Ka(Sa[active[0]],phi[active[0]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[0]])*co2[active[0]]);
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_Sa,active[1],pom*param.Ka(Sa[active[1]],phi[active[1]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[1]])*co2[active[1]]);
      }
      // top
      else if (global[1]>=param.getheight()-1e-7)
      {
        // find indices of boundary vertices
        const auto& inside_geo = ig.inside().geometry();
        auto lambda_nottopvertex = [&](std::size_t i) ->bool {return inside_geo.corner(i)[1]<param.getheight()-1e-7;};
        std::array<std::size_t,2> active = mojefunkcie::setactive(lambda_nottopvertex,lfsusize);
        // water, air zero pressure derivation (gravity induced inflow)
        // r_i.accumulate(lfsv_pw,active[0],
        //       face_volume/2*param.Kwr(Sa[active[0]],phi[active[0]])*(-param.getgrav()*param.getrho_w()) );
        // r_i.accumulate(lfsv_pw,active[1],
        //       face_volume/2*param.Kwr(Sa[active[1]],phi[active[1]])*(-param.getgrav()*param.getrho_w()) );
        RF pom = -face_volume/2*(-param.getgrav());
        mat_ii.accumulate(lfsv_pw,active[0],lfsu_Sa,active[0],pom*param.Kwr_deriv(Sa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_pw,active[1],lfsu_Sa,active[1],pom*param.Kwr_deriv(Sa[active[1]],phi[active[1]]) );
        mat_ii.accumulate(lfsv_pw,active[0],lfsu_phi,active[0],pom*param.dKwr_dphi(Sa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_pw,active[1],lfsu_phi,active[1],pom*param.dKwr_dphi(Sa[active[1]],phi[active[1]]) );
        // r_i.accumulate(lfsv_Sa,active[0],
        //        face_volume/2*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*(-param.getgrav()*p_h.getrho_a(pa[active[0]],cow[active[0]])) );
        // r_i.accumulate(lfsv_Sa,active[1],
        //        face_volume/2*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*(-param.getgrav()*p_h.getrho_a(pa[active[1]],cow[active[1]])) );
        // derive permeability by pw, Sa and phi
        mat_ii.accumulate(lfsv_Sa,active[0],lfsu_pw,active[0],pom*param.Ka(Sa[active[0]],phi[active[0]])*param.rho_a_deriv()* p_h.getrho_a(pa[active[0]],cow[active[0]]) );
        mat_ii.accumulate(lfsv_Sa,active[1],lfsu_pw,active[1],pom*param.Ka(Sa[active[1]],phi[active[1]])*param.rho_a_deriv()* p_h.getrho_a(pa[active[1]],cow[active[1]]) );
        mat_ii.accumulate(lfsv_Sa,active[0],lfsu_Sa,active[0],pom*param.Kar_deriv(Sa[active[0]],pa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_Sa,active[1],lfsu_Sa,active[1],pom*param.Kar_deriv(Sa[active[1]],pa[active[1]],phi[active[1]]) );
        mat_ii.accumulate(lfsv_Sa,active[0],lfsu_phi,active[0],pom*param.dKar_dphi(Sa[active[0]],pa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_Sa,active[1],lfsu_phi,active[1],pom*param.dKar_dphi(Sa[active[1]],pa[active[1]],phi[active[1]]) );
        // derive density from gravity term by pw and Sa
        mat_ii.accumulate(lfsv_Sa,active[0],lfsu_pw,active[0],pom*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*param.rho_a_deriv());
        mat_ii.accumulate(lfsv_Sa,active[1],lfsu_pw,active[1],pom*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*param.rho_a_deriv());
        mat_ii.accumulate(lfsv_Sa,active[0],lfsu_Sa,active[0],pom*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[0]]));
        mat_ii.accumulate(lfsv_Sa,active[1],lfsu_Sa,active[1],pom*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[1]]));

        pom*=-1.; // water and air flow have different sign on top and bottom, other not...
                  // should be checked when gravity!=0, TODO!

        // since \partial_y=0 for flow, flows are determined by gravity
        // canceled minus sign comes from unit outer normal vector
        RF bflow0 =  param.Kw(Sa[active[0]],phi[active[0]])*(-param.getrho_w()*param.getgrav());
        RF bflow1 =  param.Kw(Sa[active[1]],phi[active[1]])*(-param.getrho_w()*param.getgrav());
        // sign of bflow determines if it is outflow or inflow, \partial_y Ca=0 guarantees correctness of Ca[i] weight
        // r_i.accumulate(lfsv_Ca,active[0],face_volume/2*bflow0*Ca[active[0]]);
        // r_i.accumulate(lfsv_Ca,active[1],face_volume/2*bflow1*Ca[active[1]]);
        mat_ii.accumulate(lfsv_Ca,active[0],lfsu_Ca,active[0],face_volume/2*bflow0);
        mat_ii.accumulate(lfsv_Ca,active[1],lfsu_Ca,active[1],face_volume/2*bflow1);
        mat_ii.accumulate(lfsv_Ca,active[0],lfsu_pw,active[0],pom*param.Kwr_deriv(Sa[active[0]],phi[active[0]])*Ca[active[0]]);
        mat_ii.accumulate(lfsv_Ca,active[1],lfsu_pw,active[1],pom*param.Kwr_deriv(Sa[active[1]],phi[active[1]])*Ca[active[1]]);
        mat_ii.accumulate(lfsv_Ca,active[0],lfsu_pw,active[0],pom*param.dKwr_dphi(Sa[active[0]],phi[active[0]])*Ca[active[0]]);
        mat_ii.accumulate(lfsv_Ca,active[1],lfsu_pw,active[1],pom*param.dKwr_dphi(Sa[active[1]],phi[active[1]])*Ca[active[1]]);

        RF bfloa0 =  param.Ka(Sa[active[0]],phi[active[0]])*(-p_h.getrho_a(pa[active[0]],co2[active[0]])*param.getgrav());
        RF bfloa1 =  param.Ka(Sa[active[1]],phi[active[1]])*(-p_h.getrho_a(pa[active[1]],co2[active[1]])*param.getgrav());
        // sign of bfloa determines if it is outflow or inflow, \partial_y CO_2=0 guarantees correctness of co2[i] weight
        // r_i.accumulate(lfsv_co2,active[0],face_volume/2*bfloa0*co2[active[0]]);
        // r_i.accumulate(lfsv_co2,active[1],face_volume/2*bfloa1*co2[active[1]]);
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_co2,active[0],face_volume/2*bfloa0);
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_co2,active[1],face_volume/2*bfloa1);
        // derive permeability by Sa and phi
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_Sa,active[0],pom*param.Kar_deriv(Sa[active[0]],pa[active[0]],phi[active[0]])*co2[active[0]]);
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_Sa,active[1],pom*param.Kar_deriv(Sa[active[1]],pa[active[1]],phi[active[1]])*co2[active[1]]);
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_phi,active[0],pom*param.dKar_dphi(Sa[active[0]],pa[active[0]],phi[active[0]])*co2[active[0]]);
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_phi,active[1],pom*param.dKar_dphi(Sa[active[1]],pa[active[1]],phi[active[1]])*co2[active[1]]);
        // derive density from gravity term by pw and Sa
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_pw,active[0],pom*param.Ka(Sa[active[0]],phi[active[0]])*param.rho_a_deriv()*co2[active[0]]);
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_pw,active[1],pom*param.Ka(Sa[active[1]],phi[active[1]])*param.rho_a_deriv()*co2[active[1]]);
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_Sa,active[0],pom*param.Ka(Sa[active[0]],phi[active[0]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[0]])*co2[active[0]]);
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_Sa,active[1],pom*param.Ka(Sa[active[1]],phi[active[1]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[1]])*co2[active[1]]);
      }
    } // end lfsusize==3
  } // end jacobian_boundary */

}; // end LOP_spatial_fullycoupled

template<typename Param_chem>
class LOP_time_fullycoupled
  : public Dune::PDELab::FullVolumePattern
  , public Dune::PDELab::LocalOperatorDefaultFlags
  , public Dune::PDELab::NumericalJacobianVolume<LOP_time_fullycoupled<Param_chem> >
  , public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
  Param_chem& p_h;
  typename Param_chem::Param_wflow& param;
  using RF = typename Param_chem::value_type;
public:
  // pattern assembly flags
  enum { doPatternVolume = true };

  // residual assembly flags
  enum { doAlphaVolume = true };

  explicit LOP_time_fullycoupled(Param_chem& param_chem_)
    : p_h(param_chem_),
      param(p_h.param)
  {
  }

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X,
           typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x,
                     const LFSV& lfsv, R& r) const
  {
    // types & dimension
    //const int dim = EG::Entity::dimension;
    // get access to each vector space
    assert(LFSU::CHILDREN==6);
    using namespace Dune::TypeTree::Indices;
    const auto& lfsu_pw = lfsu.child(_0);
    const auto& lfsu_Sa = lfsu.child(_1);
    const auto& lfsu_Ca = lfsu.child(_2);
    const auto& lfsu_co2 = lfsu.child(_3);
    const auto& lfsu_phi = lfsu.child(_4);
    const auto& lfsu_CaStored = lfsu.child(_5);
    const auto& lfsv_pw = lfsv.child(_0);
    const auto& lfsv_Sa = lfsv.child(_1);
    const auto& lfsv_Ca = lfsv.child(_2);
    const auto& lfsv_co2 = lfsv.child(_3);
    const auto& lfsv_phi = lfsv.child(_4);
    const auto& lfsv_CaStored = lfsv.child(_5);
    const size_t & lfsusize = lfsu_pw.size();
    // typedef decltype(makeZeroBasisFieldValue(lfsu_pw)) RF;

    // std::vector<RF> pw(lfsusize);
    // std::vector<RF> Sa(lfsusize);
    // std::vector<RF> Ca(lfsusize);
    // std::vector<RF> co2(lfsusize);
    // std::vector<RF> por(lfsusize);
    // std::vector<RF> CaStored(lfsusize);
    // for (size_t i=0; i<lfsusize; ++i)
    // {
    //   pw[i]  = x(lfsu_pw,i);
    //   Sa[i]  = x(lfsu_Sa,i);
    //   Ca[i]  = x(lfsu_Ca,i);
    //   co2[i] = x(lfsu_co2,i);
    //   por[i] = x(lfsu_phi,i);
    //   CaStored[i] = x(lfsu_CaStored,i);
    // }

    const RF volume = eg.geometry().volume()/RF(lfsusize);
    for (std::size_t i=0; i<lfsusize; ++i)
      {
        const RF& pw  = x(lfsu_pw,i);
        const RF& Sa  = x(lfsu_Sa,i);
        const RF& Ca  = x(lfsu_Ca,i);
        const RF& co2 = x(lfsu_co2,i);
        const RF& phi = x(lfsu_phi,i);
        const RF& CaStored = x(lfsu_CaStored,i);
        const RF  pa = pw + param.pc(Sa);
        // water and air flow
        r.accumulate(lfsv_pw,i, phi*volume*param.getrho_w()* (1.-Sa) );
        r.accumulate(lfsv_Sa,i, phi*volume*p_h.getrho_a(pa,co2)*Sa  );
        r.accumulate(lfsv_Ca,i, phi*(1.-Sa)*Ca*volume);
        r.accumulate(lfsv_co2,i, phi*std::max(Sa,1e-16)*(pa+param.getpa0())*co2*volume); // regularization, else matrix is singular
        // in terms of partial pressure (not concentration) r.accumulate(lfsv_co2,i,phi*std::max(Sa,1e-16)*co2*volume); // regularization, else matrix is singular
        r.accumulate(lfsv_phi,i, phi     *volume);
        r.accumulate(lfsv_CaStored,i, CaStored*volume);

        // // leaching, now moved to spatial term to make backward "leaching" impossible,
        // // only negative Ca derivations in time should be enabled (positive won't generate leaching)
        // RF ksl = p_h.getleachspeed(1.-Sa,Ca);
        // r.accumulate(lfsv_pw,i, ksl*p_h.getMh2o()*p_h.getwls(Ca)*volume);
        // r.accumulate(lfsv_Ca,i, ksl*p_h.getCaC(Ca)*volume);
        // r.accumulate(lfsv_phi,i, ksl*p_h.getVdt(Ca)*volume); // strengthen binding? write phi+CaStored instead of por[i]-leaching*Ca?
        // r.accumulate(lfsv_CaStored,i,-ksl*p_h.getVdt(Ca)*volume);
        // // porosity describes the fraction of empty volume, CaStored is the volume of unreacted Ca compounds
        // // therefore leaching affects porosity and CaStored in exactly opposite way, the amount of lost CaStored is reflected in porosity change
      }
  }

  //! jacobian contribution of volume term
  template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
  void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
                        M& mat) const
  {
    // types & dimension
    //const int dim = EG::Entity::dimension;
    // get access to each vector space
    assert(LFSU::CHILDREN==6);
    using namespace Dune::TypeTree::Indices;
    const auto& lfsu_pw = lfsu.child(_0);
    const auto& lfsu_Sa = lfsu.child(_1);
    const auto& lfsu_Ca = lfsu.child(_2);
    const auto& lfsu_co2 = lfsu.child(_3);
    const auto& lfsu_phi = lfsu.child(_4);
    const auto& lfsu_CaStored = lfsu.child(_5);
    const auto& lfsv_pw = lfsv.child(_0);
    const auto& lfsv_Sa = lfsv.child(_1);
    const auto& lfsv_Ca = lfsv.child(_2);
    const auto& lfsv_co2 = lfsv.child(_3);
    const auto& lfsv_phi = lfsv.child(_4);
    const auto& lfsv_CaStored = lfsv.child(_5);
    const size_t & lfsusize = lfsu_pw.size();
    // typedef decltype(makeZeroBasisFieldValue(lfsu_pw)) RF;

    const RF volume = eg.geometry().volume()/RF(lfsusize);
    for (std::size_t i=0; i<lfsusize; ++i)
      {
        const RF& pw  = x(lfsu_pw,i);
        const RF& Sa  = x(lfsu_Sa,i);
        const RF& Ca  = x(lfsu_Ca,i);
        const RF& co2 = x(lfsu_co2,i);
        const RF& phi = x(lfsu_phi,i);
        // const RF& CaStored = x(lfsu_CaStored,i);
        const RF pa = pw + param.pc(Sa);
        // water and air flow
        // r.accumulate(lfsv_pw,i, phi*volume*param.getrho_w()* (1.-Sa) );
        mat.accumulate(lfsv_pw,i,lfsu_Sa,i, phi*volume*param.getrho_w()*(-1.) );
        mat.accumulate(lfsv_pw,i,lfsu_phi,i,     volume*param.getrho_w()*(1.-Sa) );
        // r.accumulate(lfsv_Sa,i, phi*volume*p_h.getrho_a(pa,co2)*Sa  );
        mat.accumulate(lfsv_Sa,i,lfsu_pw,i, phi*volume* param.drho_a_dpa(pa,co2)*Sa );
        mat.accumulate(lfsv_Sa,i,lfsu_Sa,i, phi*volume*(p_h.getrho_a(pa,co2)+param.drho_a_dpa(pa,co2)*param.pc_deriv(Sa)*Sa) );
        mat.accumulate(lfsv_Sa,i,lfsu_co2,i, phi*volume* param.drho_a_dco2(pa,co2)*Sa );
        mat.accumulate(lfsv_Sa,i,lfsu_phi,i,     volume* p_h.getrho_a(pa,co2)*Sa );

        // r.accumulate(lfsv_Ca,i, phi*(1.-Sa)*Ca*volume);
        mat.accumulate(lfsv_Ca,i,lfsu_Sa,i, phi*(  -1.)*Ca*volume);
        mat.accumulate(lfsv_Ca,i,lfsu_Ca,i, phi*(1.-Sa)   *volume);
        mat.accumulate(lfsv_Ca,i,lfsu_phi,i,     (1.-Sa)*Ca*volume );

        // transformed into CO_2 concentration
        // r.accumulate(lfsv_co2,i, phi*std::max(Sa,1e-16)*(pa+param.getpa0())*co2*volume); // regularization, else matrix is singular
        mat.accumulate(lfsv_co2,i,lfsu_pw,i, phi* std::max(Sa,1e-16)                    *co2*volume);
        mat.accumulate(lfsv_co2,i,lfsu_Sa,i, phi*                                        co2*volume
                                             *(std::max(Sa,1e-16)*param.pc_deriv(Sa)+(Sa>1e-16 ? 1. : 0.)*(pa+param.getpa0())) );
        mat.accumulate(lfsv_co2,i,lfsu_co2,i, phi* std::max(Sa,1e-16)*(pa+param.getpa0())*    volume);
        mat.accumulate(lfsv_co2,i,lfsu_phi,i,      std::max(Sa,1e-16)*(pa+param.getpa0())*co2*volume);

        // r.accumulate(lfsv_phi,i, phi     *volume);
        mat.accumulate(lfsv_phi,i,lfsu_phi,i, volume);
        // r.accumulate(lfsv_CaStored,i, CaStored*volume);
        mat.accumulate(lfsv_CaStored,i,lfsu_CaStored,i, volume);

        // // leaching -moved to spatial term
        // RF ksl = p_h.getleachspeed(1.-Sa,Ca);
        // RF dksl_dSa = p_h.dleachspeed_dSw(1.-Sa,Ca)*(-1.);
        // RF dksl_dCa = p_h.dleachspeed_dCa(1.-Sa,Ca);
        // // r.accumulate(lfsv_pw,i,                +ksl*p_h.getMh2o()*p_h.getwls(Ca)*volume);
        // mat.accumulate(lfsv_pw,i,lfsu_Sa,i,- dksl_dSa*p_h.getMh2o()*p_h.getwls(Ca)*volume);
        // mat.accumulate(lfsv_pw,i,lfsu_Ca,i,-(dksl_dCa*p_h.getwls(Ca)+ksl*p_h.dwls_dCa(Ca))*p_h.getMh2o()*volume);
        // // r.accumulate(lfsv_Ca,i, ksl*p_h.getCaC(Ca)*volume);
        // mat.accumulate(lfsv_Ca,i,lfsu_Sa,i, dksl_dSa*p_h.getCaC(Ca)*volume);
        // mat.accumulate(lfsv_Ca,i,lfsu_Ca,i, dksl_dCa*p_h.getCaC(Ca)*volume+ksl*p_h.dCaC_dCa(Ca)*volume );
        // // r.accumulate(lfsv_phi,i, phi     *volume+ksl*p_h.getVdt(Ca)*volume);
        // mat.accumulate(lfsv_phi,i,lfsu_Sa,i,  dksl_dSa*p_h.getVdt(Ca)*volume);
        // mat.accumulate(lfsv_phi,i,lfsu_Ca,i, (dksl_dCa*p_h.getVdt(Ca)+ksl*p_h.dVdt_dCa(Ca))*volume);
        // // r.accumulate(lfsv_CaStored,i, CaStored*volume-ksl*p_h.getVdt(Ca)*volume);
        // mat.accumulate(lfsv_CaStored,i,lfsu_Sa,i, - dksl_dSa*p_h.getVdt(Ca)*volume);
        // mat.accumulate(lfsv_CaStored,i,lfsu_Ca,i, -(dksl_dCa*p_h.getVdt(Ca)+ksl*p_h.dVdt_dCa(Ca))*volume);
        // // porosity describes the fraction of empty volume, CaStored is the volume of unreacted Ca compounds
        // // therefore leaching affects porosity and CaStored in exactly opposite way, the amount of lost CaStored is reflected in porosity change
      }
  } // end jac-vol */
}; // end LOP_time_fullycoupled
