      constexpr std::size_t lfsusize = 4; // this file is inside if(lfsusize==4)
                                     // overwriting enables using it as a template argument

      const auto& a0 = eg.geometry().corner(0);
      const auto& c3 = eg.geometry().corner(3);
      DF dx = c3[0]-a0[0]; // size of cell, use for face sizes and 2-point flux approx
      DF dy = c3[1]-a0[1];
      assert(dx>0 && dy>0); // assumption on the rectangle orientation
      constexpr std::array<int,lfsusize> pairx{1,0,3,2};
      constexpr std::array<int,lfsusize> pairy{2,3,0,1};

      std::array<RF,lfsusize> Kw, Ka; // better have vector of coefficients than re-compute pow()
      for (size_t i=0; i<lfsusize; ++i)
      {
        Kw[i] = param.K(phi[i]); // intrinsic permeability
        Ka[i] = Kw[i]*param.getKa_correction(); // air has different intrinsic perm. because water is chemically bound
      }
      auto lkw = [&] (int j) {return param.kw(Sa[j])*param.getrho_w()/param.getvisc_w();}; // relative permeability, density and viscosity
      auto lka = [&] (int j) {return param.ka(Sa[j])*param.getrho_a(pa[j],co2[j])/param.visc_mix(co2[j]);};

      // water and air diffusion
      for (int i=0; i<int(lfsusize); ++i)
      {
        // compute horizontal fluxes
        if (i<pairx[i]) // i==0, and i==2, thus pairx[i]=i+1
        {
          { // water flow
            RF gradwx = (pw[pairx[i]]-pw[i])*mega/dx;
            RF Kwx = h2avg(Kw[i],Kw[pairx[i]]); // intrinsic permeability, harmonic averaged
            size_t uwx = gradwx > 0. ? pairx[i] : i; // index of upwind source
            RF kwx = lkw(uwx); // upwinded relative permeability, density, and viscosity
            RF qwx = -dy/2.*Kwx*kwx*gradwx; // flux, dy/2 is the face area
            r.accumulate(lfsv_pw,      i , qwx);
            r.accumulate(lfsv_pw,pairx[i],-qwx);
          }
          { // air flow (in separate scope to avoid mixing in the variables from water flow)
            RF gradax = (pa[pairx[i]]-pa[i])*mega/dx;
            RF Kax = h2avg(Ka[i],Ka[pairx[i]]);
            size_t uax = gradax > 0. ? pairx[i] : i;
            RF kax = lka(uax);
            RF qax = -dy/2.*Kax*kax*gradax;
            r.accumulate(lfsv_Sa,      i , qax);
            r.accumulate(lfsv_Sa,pairx[i],-qax);
          }
        }
        // compute vertical fluxes, have gravity term
        if (i<pairy[i]) // i==0, and i==1, thus pairy[i]=i+2
        {
          { // water flow
            RF gradwy = (pw[pairy[i]]-pw[i])*mega/dy+param.getgrav()*param.getrho_w(); // direction of gravity guaranteed from i<pairy[i]
            RF Kwy = h2avg(Kw[i],Kw[pairy[i]]);
            size_t uwy = gradwy > 0. ? pairy[i] : i; // upwind direction takes gravity into account
            RF kwy = lkw(uwy);
            RF qwy = -dx/2.*Kwy*kwy*gradwy;
            r.accumulate(lfsv_pw,      i , qwy);
            r.accumulate(lfsv_pw,pairy[i],-qwy);
          }
          { // air flow
            RF rho_ay = (p_h.getrho_a(pa[i],co2[i])+p_h.getrho_a(pa[pairy[i]],co2[pairy[i]]))/2.; // arithmetic average for density
            RF graday = (pa[pairy[i]]-pa[i])*mega/dy+param.getgrav()*rho_ay;
            RF Kay = h2avg(Ka[i],Ka[pairy[i]]);
            size_t uay = graday > 0. ? pairy[i] : i;
            RF kay = lka(uay);
            RF qay = -dx/2.*Kay*kay*graday;
            r.accumulate(lfsv_Sa,      i , qay);
            r.accumulate(lfsv_Sa,pairy[i],-qay);
          }
        }
      }