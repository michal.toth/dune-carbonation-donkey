// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
/** \file
    \brief  Solve   Mutiphase Water-Air Flow in Porous Medium,
                    Contaminant Transport: Ca in Water, CO_2 in Air,
                    Chemical Reactions: Leaching and Carbonation.
*/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream> // console writing
#include <cmath> // e.g. usage of pow(e,x)
#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions
#include <dune/common/densematrix.hh> // for flow reconstruction
// C includes
#include<sys/stat.h>
// C++ includes
#include<math.h>
#include<iostream>
#include<stdexcept> // list of standard exceptions
#include<unistd.h> // used for parallel debugging - gethostname(), and getpid() functions
// dune-common includes
#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/parametertreeparser.hh>
#include<dune/common/timer.hh>
// dune-geometry includes
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/quadraturerules.hh>
// dune-grid includes
#include<dune/grid/yaspgrid.hh>
#include<dune/grid/utility/structuredgridfactory.hh>
#include<dune/grid/io/file/vtk.hh>
#include<dune/grid/io/file/gmshreader.hh>
#if HAVE_UG
#include<dune/grid/uggrid.hh> // we use structured grid
#endif
#if HAVE_DUNE_ALUGRID
#include<dune/alugrid/grid.hh>
#include<dune/alugrid/dgf.hh>
#include<dune/grid/io/file/dgfparser/dgfparser.hh>
#endif
#include<dune/typetree/treepath.hh> // not necessary
// dune-istl included by pdelab
// dune-pdelab includes
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/functionutilities.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/finiteelementmap/pkfem.hh>
#include<dune/pdelab/finiteelementmap/qkfem.hh>
#include<dune/pdelab/finiteelementmap/p0fem.hh>
#include<dune/pdelab/constraints/common/constraints.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include<dune/pdelab/function/callableadapter.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionadapter.hh>
#include<dune/pdelab/gridfunctionspace/subspace.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/gridfunctionspace/vtk.hh>
#include<dune/carbonation-donkey/instationary/implicitonestepfornewton.hh> // tracks line search time
#include<dune/pdelab/gridoperator/gridoperator.hh> // not raspen
#include<dune/pdelab/gridoperator/onestep.hh> // not raspen
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/variablefactories.hh>
#include<dune/pdelab/stationary/linearproblem.hh>
#include<dune/carbonation-donkey/gridoperator/renamed_gridoperator.hh> // raspen, compared to gridoperator from pdelab, this stores cu, cv
#include<dune/pdelab/instationary/onestep.hh> // not raspen
#include<dune/carbonation-donkey/backend/renamed_onestep_raspen.hh> // raspen
#include<dune/carbonation-donkey/gridoperator/renamed_onestep_raspen.hh> // raspen
#include<dune/carbonation-donkey/localoperator/renamed_pdelab_raspen.hh> // raspen
#include<dune/pdelab/solver/linesearchbounded.hh>
#include<dune/pdelab/solver/newton.hh>
#include<dune/carbonation-donkey/backend/renamed_raspen_for_miso_new_criterion_restrained_linesearch.hh> // raspen 1-level
// // #include<dune/carbonation-donkey/backend/two-level-raspen_multiplicative.hh> // raspen
// #include<dune/carbonation-donkey/backend/two-level-raspen_multiplicative_systems.hh> // raspen 2-level
#include<dune/pdelab/backend/istl.hh>
// #include<dune/istl/io.hh> // not necessary

// include other local files
#include <dune/carbonation-donkey/backend/maxnormlinsolver.hh>
#include "mojefunkcie.hh"
#include "cached_class.hh"
#include "lop-fullycoupled.hh"
#include "lop-operatorsplitting-flow-n.hh"
#include "lop-operatorsplitting-reaction-transport-n.hh"
// #include "lop_fullycoupled.hh"
#include "parametre.hh"
#include "parametre_chem.hh"
#include "driver-operatorsplitting-flow-contaminant-macro.hh"
// #include "driver-fullycoupled-macro.hh"


template <int dim>
class YaspPartitioning : public Dune::Yasp::Partitioning<dim>
{
public:
  using iTupel = typename Dune::Yasp::Partitioning<dim>::iTupel;
  virtual void partition(const iTupel& size, int P, iTupel& dims, int overlap) const override
  {
    for (int j=0; j<dim; ++j)
    {
      // find the biggest divisor of P such that n^dim<=P
      for (int i=ceil(pow(P,1./static_cast<double>(dim-j))); i>0; --i)
      {
        if (P%i == 0 && (i==1 || size[i]/i>=2*overlap || (i==2 && size[i]/i>=overlap)))
        {
          P=P/i;
          dims[j] = i;
          break;
        }
      }
      // if P is prime (or 1), fill first remaining rank with P and other with 1
      if (dims[j]==1)
      {
        dims[j]=P;
        for (int k=j+1; k<dim; ++k)
          dims[k]=1;
        std::sort(dims.begin(),dims.end(),[](auto a, auto b){return a>b ? true : false;});
        return;
      }
    }
    std::sort(dims.begin(),dims.end(),[](auto a, auto b){return a>b ? true : false;});
  }
};
template <> // template specialization for dim==2
class YaspPartitioning<2> : public Dune::Yasp::Partitioning<2>
{
public:
  using iTupel = typename Dune::Yasp::Partitioning<2>::iTupel;
  virtual void partition(const iTupel& size, int P, iTupel& dims, int overlap) const override
  {
    for (int i=sqrt(P); i>1; --i)
    {
      if (P%i == 0)
      {
        dims[0] = P/i;
        dims[1] = i;
        return;
      }
    }
    dims[0] = P;
    dims[1] = 1;
  }
};

//===============================================================
// Main program with grid setup
//===============================================================
int main(int argc, char** argv)
{
  try{
    // Maybe initialize Mpi
    Dune::MPIHelper&
      helper = Dune::MPIHelper::instance(argc, argv);
    if(Dune::MPIHelper::isFake)
      std::cout<< "This is a sequential program." << std::endl;
    else
      std::cout << "Parallel code run on "
                << helper.size() << " process(es)" << std::endl;
    // // set sleep cycle for debugger, REMOVE IF RUNNING OUTSIDE DEBUGGER
    // int ifl = 0;
    // char hostname[256];
    // gethostname(hostname, sizeof(hostname));
    // std::cout << "Using infinite sleep cycle for attaching debugger!" << std::endl;
    // printf("PID %d on %s ready for attach\n", getpid(), hostname);
    // fflush(stdout);
    // while (0 == ifl)
    //       sleep(5);

    // open ini file
    Dune::ParameterTree ptree;
    Dune::ParameterTreeParser ptreeparser;
    ptreeparser.readINITree("data.ini",ptree);
    ptreeparser.readOptions(argc,argv,ptree);

    // read ini file
    const int dim = ptree.get<int>("grid.dim");
    const int refinement = ptree.get<int>("grid.refinement");
    const int degree = ptree.get<int>("fem.degree");

    // YaspGrid section
    if (dim==2)
    {
      constexpr int dim = 2;
      // typedef Dune::YaspGrid<dim> Grid;
      typedef Dune::YaspGrid<dim,Dune::TensorProductCoordinates<double,dim>> Grid;
      typedef Grid::ctype DF;
      Dune::FieldVector<DF,dim> L;
      L[0] = ptree.get("grid.structured.LX",(DF)0.05);
      L[1] = ptree.get("grid.structured.LY",(DF)0.10);
      std::array<int,dim> N;
      N[0] = ptree.get("grid.structured.NX",(int)31);
      N[1] = ptree.get("grid.structured.NY",(int)31);
      std::array<std::vector<DF>, dim> coords;

      {
        // cell-width changes geometrically, coef=1 is uniform, coef>1 is denser at left
        DF width_ratio = ptree.get("grid.structured.width_ratio",(DF)1.);
        int meshcols = N[0]*std::pow(2,refinement); // number of cells
        DF coef = std::pow(width_ratio,1./static_cast<DF>(meshcols)); // check for accumulated numerical errors
        coords[0].resize(meshcols+1); // positions of vertices
        coords[0][0] = 0.;
        coords[0][1] = 1.;
        for (int i=2; i<meshcols+1; ++i)
        {
          auto newpoint = coords[0][i-1]+coef*(coords[0][i-1]-coords[0][i-2]);
          coords[0][i]=newpoint;
        }
        for (auto& v : coords[0])
          v *= L[0]/coords[0][coords[0].size()-1];
      }
      // // equidistant cell width
      // coords.at(0).resize(0);
      // for (int i=0; i<N[0]+1; ++i)
      // {
      //   coords.at(0).push_back(L[0]/N[0]*i);
      // }

      // int j=0;
      // // x-axis is spaced more densely at the right boundary
      // std::array<int,4> nrpoints{N[j]/4 ,N[j]*3 /8 , N[j]/ 8 ,N[j]-N[j]/4-N[j]*3/8-N[j]/8};
      // std::array<DF,4>  dx      {L[0]/2.,L[0]*3./8., L[0]/16.,L[0]/16.};
      // DF pos = 0;
      // for (int jj=0; jj<4; ++jj)
      // {
      //   for (int i=0; i<nrpoints[jj]; ++i)
      //   {
      //     coords.at(j).push_back(pos);
      //     pos+=dx[jj]/nrpoints[jj];
      //   }
      // }
      // coords.at(j).push_back(pos); // we have N[j] cells, +1 vertices

      {
        // cell-width changes geometrically, coef=1 is uniform, coef>1 is denser at bottom (height_ratio>1 is denser at top)
        DF height_ratio = ptree.get("grid.structured.height_ratio",(DF)1.);
        int meshrows = N[1]*std::pow(2,refinement); // number of cells
        DF coef = 1./std::pow(height_ratio,1./static_cast<DF>(meshrows)); // check for accumulated numerical errors
        coords[1].resize(meshrows+1); // positions of vertices
        coords[1][0] = 0.;
        coords[1][1] = 1.;
        for (int i=2; i<meshrows+1; ++i)
        {
          auto newpoint = coords[1][i-1]+coef*(coords[1][i-1]-coords[1][i-2]);
          coords[1][i]=newpoint;
        }
        for (auto& v : coords[1])
          v *= L[1]/coords[1][coords[1].size()-1];
      }


      YaspPartitioning<dim> yp;
      // std::shared_ptr<Grid> gridp = std::shared_ptr<Grid>(new Grid(L,N)); // sequential
      std::bitset<dim> periodic(false);
      int overlap=ptree.get("grid.overlap",(int)1);
      overlap *= std::pow(2,refinement); // refinement already included in mesh generation
      // std::shared_ptr<Grid> gridp = std::shared_ptr<Grid>(new Grid(L,N,periodic,overlap));//,Dune::MPIHelper::getCollectiveCommunication()));
      // std::shared_ptr<Grid> gridp = std::shared_ptr<Grid>(new Grid(coords,periodic,overlap,Dune::MPIHelper::getCollectiveCommunication()));
      std::shared_ptr<Grid> gridp = std::shared_ptr<Grid>(new Grid(coords,periodic,overlap,Dune::MPIHelper::getCommunication(),&yp));
      gridp->refineOptions(true); // keep overlap region width
      // gridp->globalRefine(refinement); // refinement included in mesh generation
      typedef Grid::LeafGridView GV;
      GV gv=gridp->leafGridView();
      if (degree==1)
      {
        typedef Dune::PDELab::QkLocalFiniteElementMap<GV,DF,double,1> FEM;
        FEM fem(gv);
        driver(gv,fem,ptree,gv);
      }
      else
        std::cout << "wrong degree" << std::endl;
    }
    else
    {
      std::cout << "wrong dimension" << std::endl;
    }
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (std::exception &e){
    std::cerr << "Standard exception thrown: " << e.what() << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }
}
