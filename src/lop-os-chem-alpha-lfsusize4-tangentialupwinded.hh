      constexpr std::size_t lfsusize = 4; // this file is inside if(lfsusize==4)
                                     // overwriting enables using it as a template argument
      // constexpr int verbosity{0};

      const auto& a0 = eg.geometry().corner(0);
      const auto& c3 = eg.geometry().corner(3);
      DF dx = c3[0]-a0[0];
      DF dy = c3[1]-a0[1];
      assert(dx>0 && dy>0); // assumption on the rectangle orientation
      constexpr std::array<int,lfsusize> pairx{1,0,3,2};
      constexpr std::array<int,lfsusize> pairy{2,3,0,1};

      std::array<RF,lfsusize> Kw, Ka; // better have vector of coefficients than re-compute pow()
      for (size_t i=0; i<lfsusize; ++i)
      {
        Kw[i] = param.K(phi[i]); // intrinsic permeability
        Ka[i] = Kw[i]*param.getKa_correction(); // air has different intrinsic perm. because water is chemically bound
      }
      auto lvw = [&] (int j) {return param.kw(Sa[j])/param.getvisc_w();}; // relative permeability, and viscosity. Without density!
      auto lva = [&] (int j) {return param.ka(Sa[j])/param.visc_mix(co2[j]);}; // not cached, I compute lva 4 times in total (no dispersion in air phase)

      // normal flux is two-point, tangential is two-point from upstream (relative to normal flux) pair of control volumes
      RF gradwxb = (pw[1]-pw[0])*mega/dx; // derivation in x on Bottom
      RF gradwxt = (pw[3]-pw[2])*mega/dx; // der. in x on Top
      RF gradwyl = (pw[2]-pw[0])*mega/dy+param.getrho_w()*param.getgrav(); // in y on Left
      RF gradwyr = (pw[3]-pw[1])*mega/dy+param.getrho_w()*param.getgrav(); // in y on Right
      RF fwxb, fwxt, fwyl, fwyr; // fluxes used in advection and dispersion
      auto setflux = [&] (RF& flux, const RF& grad, size_t out, size_t in)
      {
        size_t upw = grad > 0. ? out : in; // outer, inner
        RF hKw = h2avg(Kw[out],Kw[in]); // harmonic average for intrinsic permeability
        RF kw = lvw(upw); // full upwind for relative permeability
        flux = -hKw*kw*grad;
      };
      setflux(fwxb,gradwxb,1,0);
      setflux(fwxt,gradwxt,3,2);
      setflux(fwyl,gradwyl,2,0);
      setflux(fwyr,gradwyr,3,1);

      // Ca advection
      { // bottom
        size_t upw = gradwxb > 0. ? 1 : 0;
        r.accumulate(lfsv_Ca,0, dy/2.*fwxb*Ca_adv[upw]);
        r.accumulate(lfsv_Ca,1,-dy/2.*fwxb*Ca_adv[upw]);
      }
      { // top
        size_t upw = gradwxt > 0. ? 3 : 2;
        r.accumulate(lfsv_Ca,2, dy/2.*fwxt*Ca_adv[upw]);
        r.accumulate(lfsv_Ca,3,-dy/2.*fwxt*Ca_adv[upw]);
      }
      { // left
        size_t upw = gradwyl > 0. ? 2 : 0;
        r.accumulate(lfsv_Ca,0, dx/2.*fwyl*Ca_adv[upw]);
        r.accumulate(lfsv_Ca,2,-dx/2.*fwyl*Ca_adv[upw]);
      }
      { // right
        size_t upw = gradwyr > 0. ? 3 : 1;
        r.accumulate(lfsv_Ca,1, dx/2.*fwyr*Ca_adv[upw]);
        r.accumulate(lfsv_Ca,3,-dx/2.*fwyr*Ca_adv[upw]);
      }

      for (int i=0; i<int(lfsusize); ++i)
      {
        // CO2 advection
        if (i<pairx[i]) // i==0, and i==2, thus pairx[i]=i+1
        {
          // { // Ca flow, moved outside of for loop
          //   RF gradwx = (pw[pairx[i]]-pw[i])/dx;
          //   RF Kwx = h2avg(Kw[i],Kw[pairx[i]]); // intrinsic permeability, harmonic averaged
          //   size_t uwx = gradwx > 0. ? pairx[i] : i; // index of upwind source
          //   RF kwx = lvw(uwx); // upwinded relative permeability, and viscosity
          //   RF qwx = -dy/2.*Kwx*kwx*gradwx; // Darcy flux, and dy/2 is the face area
          //   RF advCa = qwx*Ca_adv[uwx]; // Ca advection, upwinded
          //   r.accumulate(lfsv_Ca,      i , advCa);
          //   r.accumulate(lfsv_Ca,pairx[i],-advCa);
          // }
          { // CO2 flow
            RF gradax = (pa[pairx[i]]-pa[i])*mega/dx;
            RF Kax = h2avg(Ka[i],Ka[pairx[i]]);
            size_t uax = gradax > 0. ? pairx[i] : i;
            RF kax = lva(uax);
            RF qax = -dy/2.*Kax*kax*gradax;
            RF advCO2 = qax*(pa[uax]+param.getpa0())*mega*co2_adv[uax];
            r.accumulate(lfsv_co2,      i , advCO2);
            r.accumulate(lfsv_co2,pairx[i],-advCO2);
          }
        }
        // compute vertical fluxes, have gravity term
        if (i<pairy[i]) // i==0, and i==1, thus pairy[i]=i+2
        {
          // { // Ca flow
          //   RF gradwy = (pw[pairy[i]]-pw[i])/dy+param.getgrav()*param.getrho_w(); // direction of gravity guaranteed from i<pairy[i]
          //   RF Kwy = h2avg(Kw[i],Kw[pairy[i]]);
          //   size_t uwy = gradwy > 0. ? pairy[i] : i; // upwind direction takes gravity into account
          //   RF kwy = lvw(uwy);
          //   RF qwy = -dx/2.*Kwy*kwy*gradwy;
          //   RF advCa = qwy*Ca_adv[uwy];
          //   r.accumulate(lfsv_Ca,      i , advCa);
          //   r.accumulate(lfsv_Ca,pairy[i],-advCa);
          // }
          { // CO2 flow
            RF rho_ay = (p_h.getrho_a(pa[i],co2[i])+p_h.getrho_a(pa[pairy[i]],co2[pairy[i]]))/2.; // arithmetic average for density
            RF graday = (pa[pairy[i]]-pa[i])*mega/dy+param.getgrav()*rho_ay;
            RF Kay = h2avg(Ka[i],Ka[pairy[i]]);
            size_t uay = graday > 0. ? pairy[i] : i;
            RF kay = lva(uay);
            RF qay = -dx/2.*Kay*kay*graday;
            RF advCO2 = qay*(pa[uay]+param.getpa0())*mega*co2_adv[uay];
            r.accumulate(lfsv_co2,      i , advCO2);
            r.accumulate(lfsv_co2,pairy[i],-advCO2);
          }
        }
      }

      // Ca and CO_2 diffusion
      auto Dw = [&] (int j) {return p_h.diffCa(phi[j],Sa[j]);};
      auto Da = [&] (int j) {return p_h.getdifco2(phi[j],Sa[j]);};
      auto pa_Ma = [&] (int j) {return (pa[j]+param.getpa0())*mega*p_h.getMa(co2_dif[j])/p_h.getMco2();};

      for (int i=0; i<int(lfsusize); ++i)
      {
        // horizontal fluxes
        if(i<pairx[i])
        {
          { // Ca diffuson
            RF gradCax = (Ca_dif[pairx[i]]-Ca_dif[i])/dx; // gradient
            // int uCax = gradCax>0. ? pairx[i] : i; // index of upstream DoF
            RF DCax = h2avg(Dw(i),Dw(pairx[i]));
            RF qCa = -dy/2.*DCax*gradCax; // flux, dy/2 is the face area
            r.accumulate(lfsv_Ca,      i , qCa);
            r.accumulate(lfsv_Ca,pairx[i],-qCa);
          }
          { // co2 diffusion
            RF gradco2x = (co2_dif[pairx[i]]-co2_dif[i])/dx;
            int uco2x = gradco2x>0. ? pairx[i] : i;
            RF Dco2x = h2avg(Da(i),Da(pairx[i]))*pa_Ma(uco2x);
            RF qco2 = -dy/2.*Dco2x*gradco2x;
            r.accumulate(lfsv_co2,      i , qco2);
            r.accumulate(lfsv_co2,pairx[i],-qco2);
          }
        }
        // vertical fluxes
        if(i<pairy[i])
        {
          { // Ca diffuson
            RF gradCay = (Ca_dif[pairy[i]]-Ca_dif[i])/dy;
            // int uCay = gradCay>0. ? pairy[i] : i;
            RF DCay = h2avg(Dw(i),Dw(pairy[i]));
            RF qCa = -dx/2.*DCay*gradCay;
            r.accumulate(lfsv_Ca,      i , qCa);
            r.accumulate(lfsv_Ca,pairy[i],-qCa);
          }
          { // co2 diffusion
            RF gradco2y = (co2_dif[pairy[i]]-co2_dif[i])/dy;
            int uco2y = gradco2y>0. ? pairy[i] : i;
            RF Dco2y  = h2avg(Da(i),Da(pairy[i]))*pa_Ma(uco2y);
            RF qco2 = -dx/2.*Dco2y*gradco2y;
            r.accumulate(lfsv_co2,      i , qco2);
            r.accumulate(lfsv_co2,pairy[i],-qco2);
          }
        }
      }

      // dispersion term
      const RF& a_T = p_h.getalfat();
      const RF& a_L = p_h.getalfal();
      auto getDisp = [&] (const RF& vx, const RF& vy, const RF& Sw)
      {
        using std::sqrt;
        RF denom = std::max(1e-5, sqrt(vx*vx+vy*vy)*Sw*Sw);
        RF D11 = (a_L*vx*vx+a_T*vy*vy)/denom;
        RF D22 = (a_T*vx*vx+a_L*vy*vy)/denom;
        RF D12 = (a_L-a_T)*vx*vy/denom;
        return std::tuple<RF,RF,RF>{D11,D12,D22};
      };
      // averaged, uses identical flux over the cell
      {
        RF dCax = (Ca_dif[3]-Ca_dif[2]+Ca_dif[1]-Ca_dif[0])/2./dx;
        RF dCay = (Ca_dif[3]-Ca_dif[1]+Ca_dif[2]-Ca_dif[0])/2./dy;
        RF Sw = 1.-(Sa[0]+Sa[1]+Sa[2]+Sa[3])/4.; // consider upwinding Sw and phi
        RF aphi = (phi[0]+phi[1]+phi[2]+phi[3])/4.;
        RF vwx, vwy; // fluxes used in dispersion

        {
          RF gradwx = (pw[1]-pw[0]+pw[3]-pw[2])*mega/2./dx; // derivation in x
          RF gradwy = (pw[2]-pw[0]+pw[3]-pw[1])*mega/2./dy+param.getrho_w()*param.getgrav(); // in y
          RF Kwx = h2avg(param.K((phi[0]+phi[2])/2.),param.K((phi[1]+phi[3])/2.));
          RF Kwy = h2avg(param.K((phi[0]+phi[1])/2.),param.K((phi[2]+phi[3])/2.));
          RF kwx = param.kw(gradwx<0 ? (Sa[0]+Sa[2])/2. : (Sa[1]+Sa[3])/2.)/param.getvisc_w();
          RF kwy = param.kw(gradwy<0 ? (Sa[0]+Sa[1])/2. : (Sa[2]+Sa[3])/2.)/param.getvisc_w();
          RF fwx = -Kwx*kwx*gradwx;
          RF fwy = -Kwy*kwy*gradwy;
          vwx = fwx/std::max(1e-5,Sw*aphi);
          vwy = fwy/std::max(1e-5,Sw*aphi);
        }

        auto [Dw11,Dw12,Dw22] = getDisp(vwx,vwy,Sw);

        // accumulate bottom and top face (from left to right, direction depends on flux calculation)
        {
          r.accumulate(lfsv_Ca,0,-dy/2.*(Dw11*dCax+Dw12*dCay)); // bottom
          r.accumulate(lfsv_Ca,1, dy/2.*(Dw11*dCax+Dw12*dCay)); // bottom
          r.accumulate(lfsv_Ca,2,-dy/2.*(Dw11*dCax+Dw12*dCay)); // top
          r.accumulate(lfsv_Ca,3, dy/2.*(Dw11*dCax+Dw12*dCay)); // top
        }
        // accumulate left and right face (from bottom to top)
        {
          r.accumulate(lfsv_Ca,0,-dx/2.*(Dw22*dCay+Dw12*dCax)); // left
          r.accumulate(lfsv_Ca,2, dx/2.*(Dw22*dCay+Dw12*dCax)); // left
          r.accumulate(lfsv_Ca,1,-dx/2.*(Dw22*dCay+Dw12*dCax)); // right
          r.accumulate(lfsv_Ca,3, dx/2.*(Dw22*dCay+Dw12*dCax)); // right
        }

        const bool upwinded = param.dispUpwinded(); // upwind dispersion coefficients
        if (upwinded) // add upwind flux to obtain M-matrix
        {
          RF lp = dy/2.*Dw11-dx/2.*Dw22; // longitudinal parts
          RF tp = (dy-dx)/2.*Dw12; // transversal parts
          if (std::abs(lp)>std::abs(tp)) // transversal dispersion does not affect signs
          {
            // matrix signs are symmetric, two positive off-diagonal entries are |lp|\pm tp
            // reducing them both by bigger value zeroes one and makes the other negative
            RF flux = std::abs(lp) + std::abs(tp);
            if (lp>0) // flux in x direction dominates
            {
              r.accumulate(lfsv_Ca,0,-flux*(Ca_dif[2]-Ca_dif[0])/2./dy );
              r.accumulate(lfsv_Ca,2, flux*(Ca_dif[2]-Ca_dif[0])/2./dy );
              r.accumulate(lfsv_Ca,1,-flux*(Ca_dif[3]-Ca_dif[1])/2./dy );
              r.accumulate(lfsv_Ca,3, flux*(Ca_dif[3]-Ca_dif[1])/2./dy );
            }
            else // flux in y direction dominates
            {
              r.accumulate(lfsv_Ca,0,-flux*(Ca_dif[1]-Ca_dif[0])/2./dx );
              r.accumulate(lfsv_Ca,1, flux*(Ca_dif[1]-Ca_dif[0])/2./dx );
              r.accumulate(lfsv_Ca,2,-flux*(Ca_dif[3]-Ca_dif[2])/2./dx );
              r.accumulate(lfsv_Ca,3, flux*(Ca_dif[3]-Ca_dif[2])/2./dx );
            }
          }
          else // |tp|>|lp|, transversal dispersion dictates signs
          {
            // positive off-diagonal entries are no longer symmetrically placed
            // we zero positive off-d and decrease its (negative) off-d pair
            RF flux = -lp+std::abs(tp);
            r.accumulate(lfsv_Ca,0,-flux*(Ca_dif[1]-Ca_dif[0])/2./dx );
            r.accumulate(lfsv_Ca,1, flux*(Ca_dif[1]-Ca_dif[0])/2./dx );
            r.accumulate(lfsv_Ca,2,-flux*(Ca_dif[3]-Ca_dif[2])/2./dx );
            r.accumulate(lfsv_Ca,3, flux*(Ca_dif[3]-Ca_dif[2])/2./dx );
            flux = lp+std::abs(tp);
            r.accumulate(lfsv_Ca,0,-flux*(Ca_dif[2]-Ca_dif[0])/2./dy );
            r.accumulate(lfsv_Ca,2, flux*(Ca_dif[2]-Ca_dif[0])/2./dy );
            r.accumulate(lfsv_Ca,1,-flux*(Ca_dif[3]-Ca_dif[1])/2./dy );
            r.accumulate(lfsv_Ca,3, flux*(Ca_dif[3]-Ca_dif[1])/2./dy );
          }
        } // end upwinded
      }

      // */